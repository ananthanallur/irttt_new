package com.rail.controller;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.client.methods.HttpGet;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.rail.lite.R;
import com.rail.model.DbDetails;
import com.rail.model.FinalDataReturned;
import com.rail.model.PNRData;
import com.rail.model.ScreenData;
import com.rail.model.TrainDetWithRoute;
import com.rail.network.DownloadManager;
import com.rail.network.NetworkDelegate;
import com.rail.network.ParseHandler;
import com.rail.network.UrlBuilder;
import com.rail.persistance.DatabaseHandler;
import com.rail.persistance.DatabaseUtils;
import com.rail.utilities.RuntimeData;

public class FrontController {
	private static Context sContext;
	private static FrontController sFrontController;
	public static DatabaseUtils mDatabaseUtils;

	private FrontController() {
	}

	public void initDatabase() {
		mDatabaseUtils = DatabaseUtils.getInstance(sContext);
	}

	public static FrontController getInstance(Context context) {
		sContext = context;
		if (sFrontController == null) {
			sFrontController = new FrontController();
		}
		return sFrontController;
	}

	public List<String> getStationNamesFromDB(String userInput) {
        return mDatabaseUtils.getMatchingStations(userInput);
    }

    public List<Object> getTrainsFromDB(String userInput) {
        return mDatabaseUtils.getTrainsFromDB(userInput);
    }


	public FinalDataReturned getTrainsMatchingForDay(ScreenData screenData) {
		return mDatabaseUtils.getTrainsMatchingForDay(screenData);
	}

	public TrainDetWithRoute getCompleteRoute(String trainNo,
			ScreenData screenData, boolean b) {
		return mDatabaseUtils.getCompleteRoute(trainNo, screenData, b);
	}

	public FinalDataReturned getTrainsMatchingForWeek(ScreenData screenData) {
		return mDatabaseUtils.getTrainsMatchingForWeek(screenData);
	}

	public FinalDataReturned getPassingTrainsForDay(ScreenData screenData) {
		return mDatabaseUtils.getPassingTrainsForDay(screenData);
	}

	public FinalDataReturned getPassingTrainsForWeek(ScreenData screenData) {
		return mDatabaseUtils.getPassingTrainsForWeek(screenData);
	}

	public FinalDataReturned getTrainsByName(ScreenData screenData) {
		return mDatabaseUtils.getTrainsByName(screenData);
	}

	public boolean isTrainByNumFound(String num) {
		return mDatabaseUtils.isTrainByNumFound(num);
	}

	public FinalDataReturned getTrainsByNum(ScreenData screenData) {
		return mDatabaseUtils.getTrainsByNum(screenData);
	}
	public List<String> getPassingTrainStationHistory() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		String passTrainHistory = pref.getString("passingTrainHistory", "");
		List<String> passingStations = new ArrayList<String>();
		if (passTrainHistory.length() > 0) {
			for (String station : passTrainHistory.split("\\^")) {
				passingStations.add(station);
			}
		}
		return passingStations;
	}

	public List<String> storePassingTrainStationHistory(String StationName) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> passingStations = new LinkedHashSet<String>();
		passingStations.add(StationName);
		passingStations.addAll(getPassingTrainStationHistory());
		StringBuilder builder = new StringBuilder();
		for (String station : passingStations) {
			builder.append(station).append("^");
		}
		String passingTrainStationHistory = builder.substring(0,
				builder.length() - 1).toString();
		pref.edit()
				.putString("passingTrainHistory", passingTrainStationHistory)
				.commit();
		return new ArrayList<String>(passingStations);
	}

	public List<String> getPNRHistory() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		String PNRHistory = pref.getString("PNRHistory", "");
		List<String> PNRs = new ArrayList<String>();
		if (PNRHistory.length() > 0) {
			for (String station : PNRHistory.split("\\^")) {
				PNRs.add(station);
			}
		}
		return PNRs;
	}

	public List<String> storePNRHistory(String PNR) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> PNRs = new LinkedHashSet<String>();
		PNRs.add(PNR);
		PNRs.addAll(getPNRHistory());
		StringBuilder builder = new StringBuilder();
		for (String PNR2 : PNRs) {
			builder.append(PNR2).append("^");
		}
		String PNRHistory = builder.substring(0, builder.length() - 1)
				.toString();
		pref.edit().putString("PNRHistory", PNRHistory).commit();
		return new ArrayList<String>(PNRs);
	}

	public List<String> getTrainsBtwStationHistory() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		String trainsBtwHistory = pref.getString("trainsBtw", "");
		List<String> trainsBtwStations = new ArrayList<String>();
		if (trainsBtwHistory.length() > 0) {
			for (String station : trainsBtwHistory.split("\\^")) {
				trainsBtwStations.add(station);
			}
		}
		return trainsBtwStations;
	}

	public List<String> getTrainNameOrNumberHistory() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		String trainNameNoStr = pref.getString("trainNameNoHistory", "");
		List<String> trainNameNos = new ArrayList<String>();
		if (trainNameNoStr.length() > 0) {
			for (String station : trainNameNoStr.split("\\^")) {
				trainNameNos.add(station);
			}
		}
		return trainNameNos;
	}

	public List<String> storeTrainNameOrNumHistory(String trainNameNo) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> trainNameNos = new LinkedHashSet<String>();
		trainNameNos.add(trainNameNo);
		trainNameNos.addAll(getTrainNameOrNumberHistory());
		StringBuilder builder = new StringBuilder();
		for (String train : trainNameNos) {
			builder.append(train).append("^");
		}
		String trainHistory = builder.substring(0, builder.length() - 1)
				.toString();
		pref.edit().putString("trainNameNoHistory", trainHistory).commit();
		return new ArrayList<String>(trainNameNos);
	}

	public List<String> storeTrainsBtwStationHistory(String StationName) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> trainsBtwStations = new LinkedHashSet<String>();
		trainsBtwStations.add(StationName);
		trainsBtwStations.addAll(getTrainsBtwStationHistory());
		StringBuilder builder = new StringBuilder();
		for (String station : trainsBtwStations) {
			builder.append(station).append("^");
		}
		String trainsBtwStationHistory = builder.substring(0,
				builder.length() - 1).toString();
		pref.edit().putString("trainsBtw", trainsBtwStationHistory).commit();
		return new ArrayList<String>(trainsBtwStations);
	}

	public List<String> delTrainsBtwStationHistory(List<String> StationNames) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> trainsBtwStations = new LinkedHashSet<String>();
		for (String station : getTrainsBtwStationHistory()) {
			if (!StationNames.contains(station)) {
				trainsBtwStations.add(station);
			}
		}
		StringBuilder builder = new StringBuilder();
		for (String station : trainsBtwStations) {
			builder.append(station).append("^");
		}
		if (builder.length() == 0) {
			builder.append("^");
		}
		String trainsBtwStationHistory = builder.substring(0,
				builder.length() - 1).toString();
		pref.edit().putString("trainsBtw", trainsBtwStationHistory).commit();
		return new ArrayList<String>(trainsBtwStations);
	}

	public void clearTrainsBtwStationHistory() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		pref.edit().putString("trainsBtw", "").commit();

	}

	public PNRData getPNRFromInternet(String pnr) {
		String urlForPNR = UrlBuilder.getURLFor(UrlBuilder.PNR_API,new String[]{pnr});
		String httpResponse = NetworkDelegate.getInstance(sContext)
				.getHttpResponse(urlForPNR, HttpGet.METHOD_NAME, null, null);
		return httpResponse != null ? ParseHandler.getInstance()
				.parsePNRResponse(httpResponse) : null;
	}

	public FinalDataReturned getTrainByNoForDay(ScreenData screenData) {
		return mDatabaseUtils.getTrainByNoForDay(screenData);
	}

	public FinalDataReturned getTrainWithNumberAndStation(ScreenData screenData) {
		return mDatabaseUtils.getTrainWithNumberAndStation(screenData);
	}

	public boolean isEulaAccepted() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		return pref.getBoolean("EULAACCEPTED", false);
	}

	public void setEulaAccepted() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Editor editor = pref.edit();
		editor.putBoolean("EULAACCEPTED", true);
		editor.commit();
	}

	public DbDetails checkAndGetRailDbDet() {

		DatabaseHandler tdb = DatabaseHandler.getInstance(sContext);
		if (tdb != null) {
			tdb.openDatabase(false);
			DbDetails dbDet = tdb.getRailDbDet();
			if (dbDet != null)
				Log.v("tag", "Db version " + dbDet.getDbver());
			return dbDet;
		}
		return null;
	}

	public void removeRecentPassingTrains(String remStation) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> passingStations = new LinkedHashSet<String>();
		for (String station : getPassingTrainStationHistory()) {
			if (!remStation.equalsIgnoreCase(station)) {
				passingStations.add(station);
			}
		}

		StringBuilder builder = new StringBuilder();
		for (String station : passingStations) {
			builder.append(station).append("^");
		}
		if (builder.length() == 0) {
			builder.append("^");
		}
		String passingTrainStationHistory = builder.substring(0,
				builder.length() - 1).toString();
		pref.edit()
				.putString("passingTrainHistory", passingTrainStationHistory)
				.commit();
	}

	public void clearRecentPassingTrains() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		pref.edit().putString("passingTrainHistory", "").commit();
	}

	public void clearRecentPNR() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		pref.edit().putString("PNRHistory", "").commit();
	}

	public void removeRecentPNRHistory(String rempnr) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> PNRs = new LinkedHashSet<String>();
		for (String pnr : getPNRHistory()) {
			if (!rempnr.equalsIgnoreCase(pnr)) {
				PNRs.add(pnr);
			}
		}

		StringBuilder builder = new StringBuilder();
		for (String PNR2 : PNRs) {
			builder.append(PNR2).append("^");
		}
		if (builder.length() == 0) {
			builder.append("^");
		}
		String PNRHistory = builder.substring(0, builder.length() - 1)
				.toString();
		pref.edit().putString("PNRHistory", PNRHistory).commit();
	}

	public void clearRecentTrainsByNameNum() {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		pref.edit().putString("trainNameNoHistory", "").commit();
	}

	private AdView getadView() {
		AdView adView = new AdView(sContext);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId("ca-app-pub-5168717312125894/3808681966");
		return adView;
	}

	public void showAdv(Context context, View rootView) {
		if (RailTimeConstants.SHOW_ADS) {
			AdView adView = getadView();
			LinearLayout layout = (LinearLayout) rootView
					.findViewById(R.id.linearLayoutmain_relative5);
			layout.removeAllViews();
			layout.addView(adView);
			adView.loadAd(new AdRequest.Builder().build());
			layout.setVisibility(View.VISIBLE);
			RuntimeData.setAdView(adView);
		}
	}

	public void removeParent(AdView adView) {
		if (adView.getParent() instanceof ListView) {
			((ListView) adView.getParent()).removeAllViews();
		} else if (adView.getParent() instanceof LinearLayout) {
			((LinearLayout) adView.getParent()).removeAllViews();
		}
	}

	public void showAdv(ListView listView) {

		if (RailTimeConstants.SHOW_ADS) {

			if (RuntimeData.getRequest() == null) {
				RuntimeData.setRequest(new AdRequest.Builder().build());
			}
			AdView adView = getadView();
			RuntimeData.setAdView(adView);
			listView.removeFooterView(RuntimeData.getAdView());
			if (RuntimeData.getAdView() != null) {
				listView.removeFooterView(RuntimeData.getAdView());
			}
			listView.addFooterView(RuntimeData.getAdView());
			adView.loadAd(RuntimeData.getRequest());
			RuntimeData.setAdView(adView);

		}
	}

	public void removeRecentTrainNames(String remTrain) {
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		Set<String> trainNameNos = new LinkedHashSet<String>();
		for (String trainName : getTrainNameOrNumberHistory()) {
			if (!remTrain.equalsIgnoreCase(trainName)) {
				trainNameNos.add(trainName);
			}
		}
		StringBuilder builder = new StringBuilder();
		for (String train : trainNameNos) {
			builder.append(train).append("^");
		}
		if (builder.length() == 0) {
			builder.append("^");
		}
		String trainHistory = builder.substring(0, builder.length() - 1)
				.toString();
		pref.edit().putString("trainNameNoHistory", trainHistory).commit();
	}
	
	public int getPreviousSeatMapselection(){
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		return pref.getInt("seatmapSelection", 7);
	}
	public void setPreviousSeatMapselection(int selection){
		SharedPreferences pref = sContext.getSharedPreferences("peferences",
				Context.MODE_PRIVATE);
		pref.edit().putInt("seatmapSelection", selection).commit();
	}
	
	public void downloadDb(Context context) {
		DownloadManager manager = new DownloadManager();
		RuntimeData.setDwnldMgr(manager);
		manager.handleDownloadYes(context);
	}
	
	
}
