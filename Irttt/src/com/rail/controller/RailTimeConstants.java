package com.rail.controller;





public class RailTimeConstants {
	
	public static final int DWNLD_APK = 4;
	public static final int DWNLD_FAILED = 5;
	public static final int DWNLD_COPY_FAILED = 6;
	public static final int DWNLD_INFO_NOT_FOUND = 7;
	public static final int DWNLD_CANCELED = 8;
	public static final int DWNLD_IN_PROGRESS = 9;
	public static final int DWNLD_SUCCESS = 10;
	
	
	public static final int DOWNLOAD_SUCCESS = 11;
	public static final int DOWNLOAD_FAILED = 12;
	public static final int NO_NET_CONNECTION = 14;
	public static final int NOT_ENOUGH_STORAGE = 15;
	
	public static final int APKDBCHECK_FAILED = 12;
	public static final int UPDATES_FOUND = 15;
	public static final int NO_UPDATES_FOUND = 16;
	
	public static final float MIN_STORAGE_FOR_DB = (float)4.5; // in MB.
	public static final boolean SHOW_ADS = true;
	public static String TEMP_APK_PATH = "tempapk.apk";
	public static String TEMP_DB_PATH = "db.zip";
	public static String DB_NAME = "trainDb";
	
	public static String TYPE_OF_DOWNLOAD = "TYPE";
	public static String TYPE_CONSTANT_DB = "DB";
	public static String TYPE_CONSTANT_FIRST_DB = "FIRSTDB";
	public static String DOWNLOAD_VER = "VER";
	public static String FILE_PATH = "PATH";
	public static String FILESIZE = "FILESIZE";
	
	public static String DB_DATE = "DATE";
	
	public static int APK_REQUEST_CODE = 11;
	
    public static int APK_DB_NOTIFIER_ID = 115432;
    public static int BG_DOWNLOAD_IN_PROGRESS = 12;
    
    public static String DOWNLOAD_DB_INTENT = "com.download.railtimedb";
    public static final String NOTIFICATION_DB_VER="notificationDBVer";
    public static final String NOTIFICATION_DB_COUNT ="dbNotificationCount";
    public static final String PLAY_APP_VER ="PLAY_APP_LATEST_VER";
    public static final String SERVER_DB_VER ="LATEST_DB_VER";
    public static final String TODAY ="today";
    public static final String DB_UPDATE_FOUND = "dbUpdatesFound";
    public static final String LATEST_DB_PATH ="LATEST_DB_PATH";
    public static final String LATEST_DB_SIZE ="LATEST_DB_SIZE";
    public static final String LATEST_DB_DATE = "LATEST_DB_DATE";
    public static final String PREFS_NAME = "mydata";
    public static final String DB_VER_DWNLD_CANCELLED = "DB_VER_CANCELLED";
    
    public static final int TRAIN_NOT_FOUND = 2345; // Initial value
    public static final int TRAIN_DOES_NOT_PASS_SRC = TRAIN_NOT_FOUND+1;
    public static final int TRAIN_DOES_NOT_PASS_DST = TRAIN_NOT_FOUND+2;
    public static final int TRAIN_PASSES_IN_OPPOSITE_DIR = TRAIN_NOT_FOUND+3;
    public static final int TRAIN_SERVICE_NO_FOR_DAY = TRAIN_NOT_FOUND+4; //done
    public static final int TRAIN_NO_TRANS_BW_SRC_N_DST = TRAIN_NOT_FOUND+5;//done
    public static final int TRAIN_NO_TRAINS_FOR_STN = TRAIN_NOT_FOUND+6; //done
    public static final int TRAIN_NO_TRAINS_WITH_NAME = TRAIN_NOT_FOUND+7;//done
    public static final int TRAIN_NO_TRAINS_WITH_NUM = TRAIN_NOT_FOUND+8;//done
    public static final int UNKNOWN_ERROR = TRAIN_NOT_FOUND+9;
    
    public static final int SCR1 = 1000;
    public static final int SCR2 = SCR1 + 1;
    public static final int SCR3 = SCR1 + 2;
    public static final int SCR4 = SCR1 + 3;
    public static final int SCR5 = SCR1 + 4;
    public static final int SCR6 = SCR1 + 5;
    public static final int SCR7 = SCR1 + 6;
    public static final int SCR8 = SCR1 + 7;
    public static final int SCR9 = SCR1 + 8;
    public static final int SCR10 = SCR1 + 9;
    public static final int SCR11 = SCR1 + 10;
    public static final int SCR12 = SCR1 + 11;
    public static final int SCR_UNKNOWN = SCR1 + 12;
    
    public static final int DOWNLOAD_EXT_KEEP_EXT = 0;
    public static final int DOWNLOAD_INT_KEEP_EXT = 1;
    public static final int DOWNLOAD_INT_KEEP_INT = 2;
    public static final int DOWNLOAD_SPACE_CONSTRAINT = 3;
    
    public static final String[] WEEKDAYS = {
	"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"
    };
    
    public static final String[] MONTHS_SHORT = {
    	"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
    };
    
    public static final String ENT_ST_NAME_OR_CODE = "Few chars name or code and search";
    //
}
