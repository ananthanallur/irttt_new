package com.rail.controller;

import java.util.ArrayList;

public class StationGroups {

	static String groupNames[] = { "Mumbai", "Bangalore", "Kolkata", "Chennai",
			"Hyderabad", "Tirupati", "Delhi", "Trivendrum", "Cochin", "Goa",
			"Lucknow", "Varanasi", "Patna", "Nagpur", "Chandrapur", "Bhopal",
			"Mangalore", "Pathankot", "Katni", "Durgh", "Waranghal", "Wardha", 
			"Amaravati" };

	static String[][] stationCodeOfGroup = {
			{ "CSTM", "BCT", "LTT", "KYN", "PNVL", "DR", "ADH", "BVI", "BDTS",
					"BSR" }, { "SBC", "YPR", "KJM" },
			{ "HWH", "KOAA", "SDAH", "SHM", "SRC" },
			{ "MAS", "MS", "TBM", "AJJ" }, { "HYB", "SC" , "KCG" }, { "TPTY", "RU" },
			{ "DLI", "NDLS", "NZM", "DSJ", "DEE", "GZB", "ANVT" },
			{ "KCVL", "TVC" }, { "ERS", "ERN" }, { "MAO", "VSG" },
			{ "LKO", "LJN" }, { "BSB", "MGS", "MUV" },
			{ "PNBE", "RJNR", "DNR" }, { "NGP", "ITR", "AJNI" },
			{ "CD", "BPQ", "CAF" }, { "BPL", "HBJ" }, { "MAQ", "MAJN" }, 
			{ "PTK","CHKB" },
			{ "KTE","KMZ" },
			{ "DURG","BIA","BPHB" },
			{ "WL","KZJ" },
			{ "WR","SEGM" },
			{ "AMI","BD" },
			};

	public static int getGroupStationsId(String stCode) {

		for (int i = 0; i < stationCodeOfGroup.length; i++) {
			for (int j = 0; j < stationCodeOfGroup[i].length; j++) {
				if (stCode.toUpperCase().equals(stationCodeOfGroup[i][j])) {
					return i;
				}
			}
		}
		return -1;
	}

	public static ArrayList<String> getGroupStations(int gId) {
		if (gId >= 0 && gId < stationCodeOfGroup.length) {
			ArrayList<String> group = new ArrayList<String>();
			for (int k = 0; k < stationCodeOfGroup[gId].length; k++)
				group.add(stationCodeOfGroup[gId][k]);
			return group;
		}
		return null;
	}
	
	public static String getGroupName(int gId)
	{
		return groupNames[gId];
	}
}