package com.rail.model;




public class DbDetails {
	private int dbver;
	private String dbdate;
	public int getDbver() {
		return dbver;
	}
	public void setDbver(int dbver) {
		this.dbver = dbver;
	}
	public String getDbdate() {
		return dbdate;
	}
	public void setDbdate(String dbdate) {
		this.dbdate = dbdate;
	}
	
}
