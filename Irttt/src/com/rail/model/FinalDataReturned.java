package com.rail.model;

import java.util.ArrayList;
import java.util.List;

public class FinalDataReturned {
	private int scrId;
	private int errCode;
	private String heading;
	private List<RowOf7Strings> rows= new ArrayList<RowOf7Strings>();
	public int getScrId() {
		return scrId;
	}
	public void setScrId(int scrId) {
		this.scrId = scrId;
	}
	public int getErrCode() {
		return errCode;
	}
	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}
	public String getHeading() {
		return heading;
	}
	public void setHeading(String heading) {
		this.heading = heading;
	}
	public List<RowOf7Strings> getRows() {
		return rows;
	}
	public void setRows(List<RowOf7Strings> rows) {
		this.rows = rows;
	}
	
	
}

