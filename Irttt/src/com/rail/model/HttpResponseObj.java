package com.rail.model;

import java.io.Serializable;

public class HttpResponseObj implements Serializable {

	private static final long serialVersionUID = 1L;
	private String data;

	public HttpResponseObj() {
		super();
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
