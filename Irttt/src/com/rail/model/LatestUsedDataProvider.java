package com.rail.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.rail.controller.RailTimeConstants;

public class LatestUsedDataProvider implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final int MAX_STORED_TRAINS = 15;
	private static final int MAX_STORED_STATIONS = 25;

	private transient SharedPreferences myData;
	private static final String LAST_SOURCE_NAME = "SOURCE";
	private static final String LAST_DEST_NAME = "DEST";
	private static final String LAST_TRAINNO = "LASTTRAIN";
	private static final String TRAIN_NUM = "TRAIN";
	private static final String STATIONS = "STATION";
	private transient Editor editor;
	private Station source;
	private Station dest;
	private String lastTrainNo;
	private ArrayList<String> trainNos;
	private ArrayList<String> stations;
	private static LatestUsedDataProvider usedData;

	public static LatestUsedDataProvider getInstance(Context context) {
		if (usedData == null) {
			usedData = new LatestUsedDataProvider(context);

		}
		return usedData;
	}

	
	public SharedPreferences getMyData() {
		return myData;
	}


	public void setMyData(SharedPreferences myData) {
		this.myData = myData;
	}


	public Editor getEditor() {
		return editor;
	}


	public void setEditor(Editor editor) {
		this.editor = editor;
	}


	private LatestUsedDataProvider(Context context) {
		super();
		myData = context.getSharedPreferences(RailTimeConstants.PREFS_NAME, 0);
		editor = myData.edit();
		source = new Station("", "", 0);
		dest = new Station("", "", 0);
		trainNos = new ArrayList<String>();
		stations = new ArrayList<String>();
		lastTrainNo = null;

		String srcname = myData.getString(LAST_SOURCE_NAME, "");
		if (!srcname.equals("")) {
			String[] names = srcname.split("-");
			if (names.length != 2) {
				// MyLog.v("Some issue with stored code");
			} else {
				source.setStationCode(names[1]);
				source.setStationName(names[0]);
			}
		}

		srcname = myData.getString(LAST_DEST_NAME, "");
		if (!srcname.equals("")) {
			String[] names = srcname.split("-");
			if (names.length != 2) {
				// MyLog.v("Some issue with stored code");
			} else {
				dest.setStationCode(names[1]);
				dest.setStationName(names[0]);
			}
		}

		srcname = myData.getString(LAST_TRAINNO, "");
		if (!srcname.equals("")) {
			lastTrainNo = srcname;
		}

		// railTimeDbVer = myData.getInt(RAIL_TIME_DBVER, 0);

		for (int i = 0; i < MAX_STORED_TRAINS; i++) {
			srcname = myData.getString(TRAIN_NUM + i, "");
			if (srcname.equals(""))
				break;
			trainNos.add(srcname);
		}

		for (int i = 0; i < MAX_STORED_STATIONS; i++) {
			srcname = myData.getString(STATIONS + i, "");
			if (srcname.equals(""))
				break;
			stations.add(srcname);
		}
	}

	// public int getRailTimeDbVer()
	// {
	// return railTimeDbVer;
	// }

	// public void saveRailTimeDbVer(int ver)
	// {
	// editor.putInt(RAIL_TIME_DBVER,ver);
	// editor.commit();
	// railTimeDbVer = ver;
	// }

	public void saveSourceName(String name) {
		if (name != null) {
			String[] names = name.split("-");
			if (names.length != 2) {
				// MyLog.v("Some issue with stored code");
			} else {
				editor.putString(LAST_SOURCE_NAME, name);
				editor.commit();
				source.setStationCode(names[1]);
				source.setStationName(names[0]);
				saveStation(name);
			}
		} else {
			// MyLog.v("Failed to store source station");
		}
	}

	private void saveStation(String name) {
		for (String station : stations) {
			if (station.equals(name))
				return;
		}
		if (stations.size() >= MAX_STORED_STATIONS) {
			final Random myRandom = new Random();
			int pos = myRandom.nextInt() % MAX_STORED_STATIONS;
			stations.remove(pos);
			stations.add(pos, name);
			editor.putString(STATIONS + pos, name);
			editor.commit();
		} else {
			stations.add(name);
			editor.putString(STATIONS + stations.size(), name);
			editor.commit();
		}
	}

	public void removeSrcStName() {
		editor.remove(LAST_SOURCE_NAME);
		editor.commit();
		source.setStationCode("");
		source.setStationName("");
	}

	public Station getSource() {
		if (source.getStationCode().length() == 0)
			return null;
		return source;
	}

	public void saveDestName(String name) {
		if (name != null) {
			String[] names = name.split("-");
			if (names.length != 2) {
				// MyLog.v("Some issue with stored code");
			} else {
				editor.putString(LAST_DEST_NAME, name);
				editor.commit();
				dest.setStationCode(names[1]);
				dest.setStationName(names[0]);
				saveStation(name);
			}
		} else {
			// MyLog.v("Failed to store dest station");
		}
	}

	public void removeDestName() {
		editor.remove(LAST_DEST_NAME);
		editor.commit();
		dest.setStationCode("");
		dest.setStationName("");
	}

	public Station getDestination() {
		if (dest.getStationCode().length() == 0)
			return null;
		return dest;
	}

	public String getLastTrain() {
		return lastTrainNo;
	}

	public void saveLastTrain(String name) {
		editor.putString(LAST_TRAINNO, name);
		editor.commit();
		lastTrainNo = name;
		saveTrain(name);
	}

	private void saveTrain(String name) {
		for (String train : trainNos) {
			if (train.equals(name))
				return;
		}
		if (trainNos.size() >= MAX_STORED_TRAINS) {
			final Random myRandom = new Random();
			int pos = myRandom.nextInt() % MAX_STORED_TRAINS;
			trainNos.remove(pos);
			trainNos.add(pos, name);
			editor.putString(TRAIN_NUM + pos, name);
			editor.commit();
		} else {
			trainNos.add(name);
			editor.putString(TRAIN_NUM + trainNos.size(), name);
			editor.commit();
		}
	}

	public void removeLastTrain() {
		editor.remove(LAST_TRAINNO);
		editor.commit();
		lastTrainNo = null;
	}

	public ArrayList<String> getTrains() {
		return trainNos;
	}

	public ArrayList<String> getStations() {
		return stations;
	}
}
