package com.rail.model;

import java.util.List;

public class PNRData {
	
	private String trainNumber;
	private String trainName;
	private String fromNTo;
	private String dateOfJourney;
	private String pnrNumber;
	private String chartPrepared;
	private String reservationFrom;
	private String reservationTo;
	private String numberOfPassengers;
	private String typeOfClass;
	private List<Passenger> passengers;
	private String error;
	
	
	
	@Override
	public String toString() {
		return "PNRData [trainNumber=" + trainNumber + ", trainName="
				+ trainName + ", fromNTo=" + fromNTo + ", dateOfJourney="
				+ dateOfJourney + ", pnrNumber=" + pnrNumber
				+ ", chartPrepared=" + chartPrepared + ", reservationFrom="
				+ reservationFrom + ", reservationTo=" + reservationTo
				+ ", numberOfPassengers=" + numberOfPassengers
				+ ", typeOfClass=" + typeOfClass + ", passengers=" + passengers
				+ ", error=" + error + "]";
	}


	public String getError() {
		return error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getTrainNumber() {
		return trainNumber;
	}


	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}


	public String getTrainName() {
		return trainName;
	}


	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}


	public String getFromNTo() {
		return fromNTo;
	}


	public void setFromNTo(String fromNTo) {
		this.fromNTo = fromNTo;
	}


	public String getDateOfJourney() {
		return dateOfJourney;
	}


	public void setDateOfJourney(String dateOfJourney) {
		this.dateOfJourney = dateOfJourney;
	}


	public String getPnrNumber() {
		return pnrNumber;
	}


	public void setPnrNumber(String pnrNumber) {
		this.pnrNumber = pnrNumber;
	}


	public String getChartPrepared() {
		return chartPrepared;
	}


	public void setChartPrepared(String chartPrepared) {
		this.chartPrepared = chartPrepared;
	}


	public String getReservationFrom() {
		return reservationFrom;
	}


	public void setReservationFrom(String reservationFrom) {
		this.reservationFrom = reservationFrom;
	}


	public String getReservationTo() {
		return reservationTo;
	}


	public void setReservationTo(String reservationTo) {
		this.reservationTo = reservationTo;
	}


	public String getNumberOfPassengers() {
		return numberOfPassengers;
	}


	public void setNumberOfPassengers(String numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}


	public String getTypeOfClass() {
		return typeOfClass;
	}


	public void setTypeOfClass(String typeOfClass) {
		this.typeOfClass = typeOfClass;
	}


	public List<Passenger> getPassengers() {
		return passengers;
	}


	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}


	public class Passenger{
		private String passengerNum;
		private String bookingStatus;
		private String currentStatus;
		public String getPassengerNum() {
			return passengerNum;
		}
		public void setPassengerNum(String passengerNum) {
			this.passengerNum = passengerNum;
		}
		public String getBookingStatus() {
			return bookingStatus;
		}
		public void setBookingStatus(String bookingStatus) {
			this.bookingStatus = bookingStatus;
		}
		public String getCurrentStatus() {
			return currentStatus;
		}
		public void setCurrentStatus(String currentStatus) {
			this.currentStatus = currentStatus;
		}
		@Override
		public String toString() {
			return "Passenger [passengerNum=" + passengerNum
					+ ", bookingStatus=" + bookingStatus + ", currentStatus="
					+ currentStatus + "]";
		}
		
		
		
	}
}
