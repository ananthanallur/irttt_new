package com.rail.model;

import java.io.Serializable;
import java.util.Calendar;

import android.content.Context;
import android.content.Intent;

public class ScreenData implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private String trainName; // Partial name for searching by train names.
	// If name is present then all other data is not considered.
	
	private Station source;
	private Station dest;
	private Calendar date;
	private String trainNo;
	private int btnId;
	private boolean considerNearbyTrains;
	private LatestUsedDataProvider usedData;
	
	
	
	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public Station getSource() {
		return source;
	}

	public void setSource(Station source) {
		this.source = source;
	}

	public Station getDest() {
		return dest;
	}

	public void setDest(Station dest) {
		this.dest = dest;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(String trainNo) {
		this.trainNo = trainNo;
	}

	public int getBtnId() {
		return btnId;
	}

	public void setBtnId(int btnId) {
		this.btnId = btnId;
	}

	public boolean isConsiderNearbyTrains() {
		return considerNearbyTrains;
	}

	public void setConsiderNearbyTrains(boolean considerNearbyTrains) {
		this.considerNearbyTrains = considerNearbyTrains;
	}

	public LatestUsedDataProvider getUsedData() {
		return usedData;
	}

	public void setUsedData(LatestUsedDataProvider usedData) {
		this.usedData = usedData;
	}

	

	public ScreenData(Context context) {
		usedData = LatestUsedDataProvider.getInstance(context);
		trainName = null;
	}

	public void fillExtrasToIntent(Intent i) {
		if( trainName != null && !trainName.equals("")){
			i.putExtra("TR_NAME_FOR_SEARCH", trainName);
			return;
		}

		if (source != null) {
			i.putExtra("SRC_ST_CODE", source.getStationCode());
			i.putExtra("SRC_ST_NAEM", source.getStationName());
			i.putExtra("SRC_INDEX", source.getIndex());
		}

		if (dest != null) {
			i.putExtra("DST_ST_CODE", dest.getStationCode());
			i.putExtra("DST_ST_NAEM", dest.getStationName());
			i.putExtra("DST_INDEX", dest.getIndex());
		}

		i.putExtra("DATE_MILLIS", date.getTimeInMillis());
		i.putExtra("TRAIN_NO", trainNo);
		i.putExtra("BTN_ID", btnId);
		i.putExtra("CONSIDER_NEARBY_STATIONS", considerNearbyTrains);

	}

	public void getExtrasFromIntent(Intent i) {
		
		trainName = null;
		String trName = i.getStringExtra("TR_NAME_FOR_SEARCH");
		if (trName != null && !trName.equals("")) {
           trainName = trName;
           return;
		}
		
		source = null;
		String stCode = i.getStringExtra("SRC_ST_CODE");
		if (stCode != null) {
			String stName = i.getStringExtra("SRC_ST_NAEM");
			if (stName != null) {
				int index = i.getIntExtra("SRC_INDEX", -1);
				Station src = new Station(stCode, stName, index);
				source = src;
			}
		}

		stCode = i.getStringExtra("DST_ST_CODE");
		if (stCode != null) {
			String stName = i.getStringExtra("DST_ST_NAEM");
			if (stName != null) {
				int index = i.getIntExtra("DST_INDEX", -1);
				Station s = new Station(stCode, stName, index);
				dest = s;
			}
		}

		date = Calendar.getInstance();
		date.setTimeInMillis(i.getLongExtra("DATE_MILLIS", -1));

		trainNo = i.getStringExtra("TRAIN_NO");
		btnId = i.getIntExtra("BTN_ID", -1);
		considerNearbyTrains = i.getBooleanExtra("CONSIDER_NEARBY_STATIONS", false);
	}

	public void saveToLatestValidData() {
		if( trainName != null && !trainName.equals("")){
			// We are not saving search for train name.
			return;
		}
		
		if (source != null)
			usedData.saveSourceName(source.getStationName() + "-" + source.getStationCode());
		else
			usedData.removeSrcStName();
		if (dest != null)
			usedData.saveDestName(dest.getStationName() + "-" + dest.getStationCode());
		else
			usedData.removeDestName();
		if( trainNo == null || trainNo.equals(""))
			usedData.removeLastTrain();
		else
			usedData.saveLastTrain(trainNo);
	}


}