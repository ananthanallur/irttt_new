package com.rail.model;

import java.io.Serializable;

public class Station implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String stationCode;
	private String stationName;
	private int index;// This is with respect to the prefereces.

	public Station(String code, String name, int index) {
		this.stationCode = code;
		this.stationName = name;
		this.index = index;
	}

	public String getStationCode() {
		return stationCode;
	}

	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}