package com.rail.model;




public class TdForSrcToDst {
	private TrainDetails td;
	private String srcArr;
	private String dstArr;
	private int srcDatePlus;
	private int dstDatePlus;

	public TrainDetails getTd() {
		return td;
	}

	public void setTd(TrainDetails td) {
		this.td = td;
	}

	public String getSrcArr() {
		return srcArr;
	}

	public void setSrcArr(String srcArr) {
		this.srcArr = srcArr;
	}

	public String getDstArr() {
		return dstArr;
	}

	public void setDstArr(String dstArr) {
		this.dstArr = dstArr;
	}

	public int getSrcDatePlus() {
		return srcDatePlus;
	}

	public void setSrcDatePlus(int srcDatePlus) {
		this.srcDatePlus = srcDatePlus;
	}

	public int getDstDatePlus() {
		return dstDatePlus;
	}

	public void setDstDatePlus(int dstDatePlus) {
		this.dstDatePlus = dstDatePlus;
	}

}
