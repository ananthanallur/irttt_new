package com.rail.model;

import java.util.List;

public class TdForSrcToDstForStnGrp {
	private List<TdForSrcToDst> tdForSrcToDstarray;
	private int srcStationGroupId;
	private int destStationGroupId;
	private int combinedStationGroupId;
	private int errorCode;
	
	
	
	public List<TdForSrcToDst> getTdForSrcToDstarray() {
		return tdForSrcToDstarray;
	}
	public void setTdForSrcToDstarray(List<TdForSrcToDst> tdForSrcToDstarray) {
		this.tdForSrcToDstarray = tdForSrcToDstarray;
	}
	public int getSrcStationGroupId() {
		return srcStationGroupId;
	}
	public void setSrcStationGroupId(int srcStationGroupId) {
		this.srcStationGroupId = srcStationGroupId;
	}
	public int getDestStationGroupId() {
		return destStationGroupId;
	}
	public void setDestStationGroupId(int destStationGroupId) {
		this.destStationGroupId = destStationGroupId;
	}
	public int getCombinedStationGroupId() {
		return combinedStationGroupId;
	}
	public void setCombinedStationGroupId(int combinedStationGroupId) {
		this.combinedStationGroupId = combinedStationGroupId;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}
