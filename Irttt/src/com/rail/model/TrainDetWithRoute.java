package com.rail.model;

import java.util.ArrayList;

public class TrainDetWithRoute {
	private TrainDetails td;
	private ArrayList<TrainRoute> rt;
	private int errorCode;
    private boolean otherGroupSrcStation;
   private  boolean otherGroupDstStation;
    private int srcRtPos;
    private int dstRtPos;
	
	public TrainDetWithRoute(TrainDetails td, ArrayList<TrainRoute> rt) {
		super();
		this.td = td;
		this.rt = rt;
		errorCode = 0;
	}

	public TrainDetails getTd() {
		return td;
	}

	public void setTd(TrainDetails td) {
		this.td = td;
	}

	public ArrayList<TrainRoute> getRt() {
		return rt;
	}

	public void setRt(ArrayList<TrainRoute> rt) {
		this.rt = rt;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public boolean isOtherGroupSrcStation() {
		return otherGroupSrcStation;
	}

	public void setOtherGroupSrcStation(boolean otherGroupSrcStation) {
		this.otherGroupSrcStation = otherGroupSrcStation;
	}

	public boolean isOtherGroupDstStation() {
		return otherGroupDstStation;
	}

	public void setOtherGroupDstStation(boolean otherGroupDstStation) {
		this.otherGroupDstStation = otherGroupDstStation;
	}

	public int getSrcRtPos() {
		return srcRtPos;
	}

	public void setSrcRtPos(int srcRtPos) {
		this.srcRtPos = srcRtPos;
	}

	public int getDstRtPos() {
		return dstRtPos;
	}

	public void setDstRtPos(int dstRtPos) {
		this.dstRtPos = dstRtPos;
	}
	
	
}
