package com.rail.model;




public class TrainDetails {
	private int tid;
	private String trainName;
	private boolean [] scheduleFromSunday ;
	private String trainNo;
	
	
	
	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getTrainName() {
		return trainName;
	}

	public void setTrainName(String trainName) {
		this.trainName = trainName;
	}

	public boolean[] getScheduleFromSunday() {
		return scheduleFromSunday;
	}

	public void setTrainNo (String no)
	{
		trainNo = no;
	}
	
	public String getTrainNo()
	{
		return trainNo;
	}

	// The values have to be provided in a boolean array 
		// starting from Sunday
	public void setScheduleFromSunday(boolean[] scheduleFromSunday) {
		this.scheduleFromSunday = scheduleFromSunday;
	}
	
	
	
}
