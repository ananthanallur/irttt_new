package com.rail.model;




public class TrainRoute {
	//String trainNo; // There will be multiple copies of this. Only used for storage.
	private int rtid;
	private int tid;
	private String stationCode;
	private String stationName;
	private String arriaval;
	private int datePlus; // This specifies when the train reaches
	              // 0 means the same day. 1 Next day 2 next next day.
	private int halt;
	private String dist;
	public int getRtid() {
		return rtid;
	}
	public void setRtid(int rtid) {
		this.rtid = rtid;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public String getStationCode() {
		return stationCode;
	}
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	public String getArriaval() {
		return arriaval;
	}
	public void setArriaval(String arriaval) {
		this.arriaval = arriaval;
	}
	public int getDatePlus() {
		return datePlus;
	}
	public void setDatePlus(int datePlus) {
		this.datePlus = datePlus;
	}
	public int getHalt() {
		return halt;
	}
	public void setHalt(int halt) {
		this.halt = halt;
	}
	public String getDist() {
		return dist;
	}
	public void setDist(String dist) {
		this.dist = dist;
	}
	
	
	
}