package com.rail.model;

//package com.example.parser;

public class UpdateDetails {

	private String dbpath;
	private int dbVer; 
	private String dbDate;
	private int dbSize;
	private int appVer;
	private boolean isUpdateAvailable;
	
	
	
	public int getAppVer() {
		return appVer;
	}
	public void setAppVer(int appVer) {
		this.appVer = appVer;
	}
	public boolean isUpdateAvailable() {
		return isUpdateAvailable;
	}
	public void setUpdateAvailable(boolean isUpdateAvailable) {
		this.isUpdateAvailable = isUpdateAvailable;
	}
	public String getDbpath() {
		return dbpath;
	}
	public void setDbpath(String dbpath) {
		this.dbpath = dbpath;
	}
	public int getDbVer() {
		return dbVer;
	}
	public void setDbVer(int dbVer) {
		this.dbVer = dbVer;
	}
	public String getDbDate() {
		return dbDate;
	}
	public void setDbDate(String dbDate) {
		this.dbDate = dbDate;
	}
	public int getDbSize() {
		return dbSize;
	}
	public void setDbSize(int dbSize) {
		this.dbSize = dbSize;
	} 

	

}
