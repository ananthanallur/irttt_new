package com.rail.network;

import java.io.File;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;

import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.model.DbDetails;
import com.rail.model.UpdateDetails;
import com.rail.persistance.DatabaseHandler;
import com.rail.utilities.IrtttAlertDialog;
import com.rail.utilities.OrientationUtils;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class DownloadManager implements UpdateFinderResultListener{
	private DownloadTask bg;
	int storageOption;
	Context context;

	public DownloadTask getBg() {
		return bg;
	}

	public void setBg(DownloadTask bg) {
		this.bg = bg;
	}

	public void handleDownloadYes(Context context) {
		this.context = context;
		if (!Utils.isNetworkAvailable(context)) {
			IrtttAlertDialog.showAlert(context,
					RailTimeConstants.NO_NET_CONNECTION);
			return;
		}

		storageOption = Utils.getStorageOption(context);
		if (storageOption == RailTimeConstants.DOWNLOAD_SPACE_CONSTRAINT) {

			IrtttAlertDialog.showAlert(context,
					RailTimeConstants.NOT_ENOUGH_STORAGE);
			return;
		}
		downloadWithoutDownloadMgr();
	}

	private void downloadWithoutDownloadMgr() {
		final UpdateFinder checker = new UpdateFinder(context);
		checker.findDBUpdate(this);		
	}

	public void showDownloadProgress(Context context) {
		ProgressDialog dialog;
		if (Build.VERSION.SDK_INT > 11) {
			dialog = new ProgressDialog(context, 5);
		} else {
			dialog = new ProgressDialog(context);
		}

		dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		dialog.setMessage("Downloading please wait..");
		dialog.setCancelable(false);
		dialog.setProgress(0);
		dialog.setMax(100);
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel\ndownload",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						bg.cancel();
						dialog.dismiss();
						RuntimeData.setProgress_dialog(null);
						OrientationUtils.unlockOrientation();
					}
				});
		dialog.setButton(DialogInterface.BUTTON_NEUTRAL,
				"Push to\n Background", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();
						OrientationUtils.unlockOrientation();
					}
				});
		RuntimeData.setProgress_dialog(dialog);
		OrientationUtils.lockOrientation((Activity) context);
		RuntimeData.getProgress_dialog().show();
	}

	class DownloadTask extends AsyncTask<Context, Integer, Integer>
			implements ProgressUpdateCallBack {

		private int storageOption = 0;

		private Context context;
		private FileDownloader downloader;
		boolean canceled = false;
		UpdateDetails dbUpdateDetails;

		public void cancel() {
			if (downloader != null)
				downloader.setCanceled(true);
			canceled = true;
			RuntimeData.setBgDwnldInProgress(false);
		}

		public DownloadTask(int storateOption_in, UpdateDetails dbUpdateDetails) {
			storageOption = storateOption_in;
			this.dbUpdateDetails = dbUpdateDetails;
		}

		@Override
		protected Integer doInBackground(Context... params) {
			context = params[0];
			return downloadnow(storageOption);
		}

		private int downloadnow(int storageOption) {
			int retVal;
			File root = Environment.getExternalStorageDirectory();
			File dbdir = new File(root, context.getPackageName());
			if (downloader != null)
				return RailTimeConstants.DWNLD_IN_PROGRESS;
			if (canceled)
				return RailTimeConstants.DWNLD_CANCELED;
			downloader = new FileDownloader(context);

			if (storageOption != RailTimeConstants.DOWNLOAD_EXT_KEEP_EXT) // sufficient
																			// space
																			// not
																			// available
																			// on
																			// SD.
			{
				retVal = downloader.download(RailTimeConstants.TEMP_DB_PATH,
						dbUpdateDetails.getDbpath(), dbUpdateDetails.getDbSize(), false, this);
			} else {
				dbdir.mkdirs();
				retVal = downloader.download(RailTimeConstants.TEMP_DB_PATH,
						dbUpdateDetails.getDbpath(), dbUpdateDetails.getDbSize(), true, this);
			}
			if (retVal == FileDownloader.FD_SUCCESS) {
				boolean copied = false;
				File filesAt;

				if (storageOption != RailTimeConstants.DOWNLOAD_EXT_KEEP_EXT)
					filesAt = context.getFilesDir();
				else
					filesAt = dbdir;
				// MyLog.v("Download success");
				if (storageOption == 0 || storageOption == 1) {
					dbdir.mkdirs();
					copied = Utils.copyFile(filesAt,
							RailTimeConstants.TEMP_DB_PATH,
							dbdir.getAbsolutePath(), RailTimeConstants.DB_NAME);
				} else {
					copied = Utils.copyFile(context.getFilesDir(),
							RailTimeConstants.TEMP_DB_PATH, context
									.getFilesDir().getAbsolutePath(),
							RailTimeConstants.DB_NAME);
				}

				if (!copied) {
					// MyLog.v("Failed to copy");
					return RailTimeConstants.DWNLD_COPY_FAILED;
				}
				// Delete the temp file now
				// LatestUsedDataProvider prefData =
				// LatestUsedDataProvider.getInstance(a);
				// prefData.saveRailTimeDbVer(a.ver);
				File f = new File(filesAt, RailTimeConstants.TEMP_DB_PATH);
				f.delete();

				// Delete the file in external storage if downloaded to internal
				if (storageOption == RailTimeConstants.DOWNLOAD_INT_KEEP_INT) {
					File f1 = new File(dbdir, RailTimeConstants.DB_NAME);
					f1.delete();
				}

				// MyLog.v("Downloaded and copied to place");
				return RailTimeConstants.DWNLD_SUCCESS;

			}
			if (retVal == FileDownloader.FD_CANCELED)
				return RailTimeConstants.DWNLD_CANCELED;
			return RailTimeConstants.DWNLD_FAILED;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			for (Integer value : values) {
				if (RuntimeData.getProgress_dialog() != null)
					RuntimeData.getProgress_dialog().setProgress(value);
			}
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if (RuntimeData.getProgress_dialog() != null) {
				if (RuntimeData.getProgress_dialog().isShowing()) {
					RuntimeData.getProgress_dialog().dismiss();
				}

			}
			RuntimeData.setBgDwnldInProgress(false);
			RuntimeData.setProgress_dialog(null);
			OrientationUtils.unlockOrientation((Activity) context);
			if (result == RailTimeConstants.DWNLD_APK) {
				File root = Environment.getExternalStorageDirectory();
				File file = new File(root, RailTimeConstants.TEMP_APK_PATH);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.fromFile(file),
						"application/vnd.android.package-archive");
				context.startActivity(intent);
			} else if (result == RailTimeConstants.DWNLD_SUCCESS) {
				FrontController.getInstance(context).initDatabase();
				SharedPreferences sf = context.getSharedPreferences(
						RailTimeConstants.PREFS_NAME, 0);
				int serverDbVer = sf.getInt(
						RailTimeConstants.SERVER_DB_VER, -1);
				DbDetails dbDt = FrontController.getInstance(context)
						.checkAndGetRailDbDet();

				if ((dbDt == null || serverDbVer > dbDt.getDbver())) {
					sf.edit().putBoolean(RailTimeConstants.DB_UPDATE_FOUND, true).commit();
				} else {
					sf.edit().putBoolean(RailTimeConstants.DB_UPDATE_FOUND, false).commit();
				}

				int latestAppVer = sf.getInt(
						RailTimeConstants.PLAY_APP_VER, -1);
				if (Utils.getMyAppVerCode(context) < latestAppVer) {
					sf.edit().putBoolean("apkUpdatesFound", true).commit();
				} else {
					sf.edit().putBoolean("apkUpdatesFound", false).commit();
				}

				context.sendBroadcast(new Intent("DOWNLOAD_SUCCESS"));
				if (RuntimeData.isRunning())
					IrtttAlertDialog.showAlert(context,
							RailTimeConstants.DOWNLOAD_SUCCESS);
			} else if (result == RailTimeConstants.DWNLD_FAILED
					|| result == RailTimeConstants.DWNLD_COPY_FAILED
					|| result == RailTimeConstants.DWNLD_IN_PROGRESS
					) {
				if (DatabaseHandler.getInstance(context).isDBPresent()) {
					FrontController.getInstance(context).initDatabase();
				}
				if (RuntimeData.isRunning())
					IrtttAlertDialog.showAlert(context,
							RailTimeConstants.DOWNLOAD_FAILED);
			} else {
				if (DatabaseHandler.getInstance(context).isDBPresent()) {
					FrontController.getInstance(context).initDatabase();
				}
			}

			NotificationManager mNotificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.cancel(RailTimeConstants.APK_DB_NOTIFIER_ID);
			// a.finish();

		}

		public void updateMyProgress(int value) {
			publishProgress(value);
		}
	}

	@Override
	public void onDBUpdateFinderFinished(UpdateDetails dbUpdateDetails) {
		if (dbUpdateDetails != null && dbUpdateDetails.isUpdateAvailable()) {
			bg = new DownloadTask(storageOption,dbUpdateDetails);
			bg.execute(context);
			RuntimeData.setBgDwnldInProgress(true);
			showDownloadProgress(context);
		}else{
			if (DatabaseHandler.getInstance(context).isDBPresent()) {
				FrontController.getInstance(context).initDatabase();
			}
			if (RuntimeData.isRunning())
				IrtttAlertDialog.showAlert(context,
						RailTimeConstants.DOWNLOAD_FAILED);
		}
	}

	@Override
	public void onAppUpdateFinderFinished(UpdateDetails updateDetails) {
		// TODO Auto-generated method stub
		
	}

}
