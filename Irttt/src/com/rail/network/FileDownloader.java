package com.rail.network;

//import java.io.BufferedInputStream; 3 months
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream; //import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL; //import java.net.URLConnection;
//import java.util.ArrayList;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

//import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Environment;

import com.rail.controller.RailTimeConstants;

public class FileDownloader {
	private Context context;
	private volatile boolean canceled;
	public static final int FD_SUCCESS = 0;
	public static final int FD_FAILED = 1;
	public static final int FD_CANCELED = 2;

	// BroadcastReceiver r;

	public FileDownloader(Context c) {
		context = c;
	}

	public void cancel() {
		setCanceled(true);
	}

	// size will be the expected size of the file. If size specified is zero
	// then size check will not be done. If size specified is more than zero
	// then check will be done to confirm that those many bytes are
	// downloaded.
	public synchronized int download(String local, String url_in, int size,
			boolean ext_storage, ProgressUpdateCallBack cbk) {

		setCanceled(false);

		InputStream in = null;
		FileOutputStream f = null;
		HttpURLConnection c = null;
		long totalRead = 0;
		int lastUpdated = 0;
		int count = 50; // retries
		while (count >= 0 && !isCanceled()) {
			try {
				System.out.println("---------------"+url_in);
				URL u = new URL(url_in);
				c = (HttpURLConnection) u.openConnection();
				c.setConnectTimeout(60000);
				c.setReadTimeout(30000);
				c.setRequestMethod("GET");
				//c.setDoOutput(true);

				if (totalRead > 0) {
					c.addRequestProperty("Range", "bytes=" + totalRead + "-");
				}
				c.connect();

				int responseCode = c.getResponseCode();
				switch (responseCode) {
				case HttpURLConnection.HTTP_OK:
					totalRead = 0;
					break;
				case HttpURLConnection.HTTP_PARTIAL:
					break; // retaining value of totalRead;
				default:
//					MyLog.v("Not expected.. respCode= " + responseCode);
					totalRead = 0;
				}
				if (ext_storage) {
					File root = Environment.getExternalStorageDirectory();
					File dbdir = new File(root, context.getPackageName());
					if (totalRead == 0)
						f = new FileOutputStream(new File(dbdir, local));
					else
						f = new FileOutputStream(new File(dbdir, local), true);
				} else {
					if (totalRead != 0) {
						f = new FileOutputStream(new File(
								context.getFilesDir(), local), true);
					} else {
						f = new FileOutputStream(new File(
								context.getFilesDir(), local));
					}
				}
				in = c.getInputStream();
				byte[] buffer = new byte[1024];
				int len1 = 0;
				while (!isCanceled() && ((len1 = in.read(buffer)) != -1)) {
					totalRead = totalRead + len1;
//					MyLog.v("Read bytes " + len1);
					f.write(buffer, 0, len1);
//					MyLog.v("Going to read ");

					if (size > 0 && cbk != null) {
						// Updating progress
						double currentprogress = ((double) totalRead / size) * 100;
						if (lastUpdated < currentprogress) {
							lastUpdated = (int) currentprogress;
							cbk.updateMyProgress(lastUpdated);
						}
					}
				}

				if (isCanceled()) {
					in.close();
					f.close();
					c.disconnect();
					break;
				}

//				MyLog.v("Total bytes read " + totalRead);
				if (size > 0 && totalRead != size) {
//					MyLog.v("download failed to download complete file");
				} else {
					in.close();
					f.close();
					c.disconnect();
					return FD_SUCCESS;
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (ProtocolException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
//				MyLog.v("IQException to read from file");
				count--;
			} finally {
				try {
					count--;
					in.close();
					f.close();
					c.disconnect();
				} catch (Exception e) {
				}
			}
		}

		if (isCanceled()) {
			File baseLoc;
			if (ext_storage) {
				File root = Environment.getExternalStorageDirectory();
				baseLoc = new File(root, local);
			} else
				baseLoc = new File(context.getFilesDir(),
						RailTimeConstants.TEMP_DB_PATH);
			baseLoc.delete();
			return FD_CANCELED;
		}
		return FD_FAILED;
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
