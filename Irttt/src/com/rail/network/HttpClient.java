package com.rail.network;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.rail.model.HttpResponseObj;

public class HttpClient {
	private DefaultHttpClient httpClient;
	public static final String HTTP_DATA_ERROR = "ERROR";

	public HttpClient() {
		HttpParams httpParameters = new BasicHttpParams();
		httpClient = new DefaultHttpClient();
		int timeoutConnection = 45000;
		int timeoutSocket = 45000;
		HttpConnectionParams.setConnectionTimeout(httpParameters,
				timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		httpClient.setParams(httpParameters);
	}

	/**
	 * Method which establishes the connection with the server and fetches data.
	 * 
	 * @param url
	 *            - Server address.
	 * @return response - Response object.
	 */
	public synchronized HttpResponseObj executeRequest(
			HttpUriRequest httpRequest) {
		HttpResponseObj data = new HttpResponseObj();
		try {

			HttpResponse httpResponse = httpClient.execute(httpRequest);
			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				data.setData(EntityUtils.toString(httpEntity));
			} else {
				data.setData(HTTP_DATA_ERROR);
			}

		} catch (UnsupportedEncodingException e) {
			data.setData(HTTP_DATA_ERROR);
		} catch (MalformedURLException e) {
			data.setData(HTTP_DATA_ERROR);
		} catch (IOException e) {
			data.setData(HTTP_DATA_ERROR);
		}
		return data;
	}

	public HttpUriRequest createHttpRequest(String url, String requestType,
			Map<String, String> params, Map<String, String> headers)
			throws UnsupportedEncodingException {
		HttpUriRequest request = null;
		// Handle the Null check for request by changing, default as POST
		// request.
		if (handleStrNull(requestType).length() > 0) {
			if (HttpGet.METHOD_NAME.equalsIgnoreCase(requestType)) {
				HttpGet httpGet = new HttpGet(url);
				request = httpGet;
			} else if (HttpPut.METHOD_NAME.equalsIgnoreCase(requestType)) {
				HttpPut httpPut = new HttpPut(url);
				if (params != null && params.size() > 0) {
					List<NameValuePair> postParams = new ArrayList<NameValuePair>();
					for (Map.Entry<String, String> param : params.entrySet()) {
						postParams.add(new BasicNameValuePair(param.getKey(),
								param.getValue()));

					}
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
							postParams, HTTP.UTF_8);
					httpPut.setEntity(urlEncodedFormEntity);
				}
				request = httpPut;

			} else {
				HttpPost httpPost = new HttpPost(url);
				if (params != null && params.size() > 0) {
					List<NameValuePair> postParams = new ArrayList<NameValuePair>();
					for (Map.Entry<String, String> param : params.entrySet()) {
						postParams.add(new BasicNameValuePair(param.getKey(),
								param.getValue()));

					}
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
							postParams, HTTP.UTF_8);
					httpPost.setEntity(urlEncodedFormEntity);
				}
				request = httpPost;
			}

			if (headers != null && headers.size() > 0) {
				for (Map.Entry<String, String> header : headers.entrySet()) {
					request.addHeader(header.getKey(),
							URLEncoder.encode(header.getValue(), "utf-8"));
				}
			}
		}

		return request;

	}

	public String handleStrNull(String resultStr) {
		if (null != resultStr && resultStr.length() > 0
				&& !resultStr.equalsIgnoreCase("null"))
			return resultStr;
		else
			return "";
	}

	
	
}