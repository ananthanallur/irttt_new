package com.rail.network;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.client.methods.HttpUriRequest;

import android.content.Context;
import android.util.Log;

import com.rail.model.HttpResponseObj;
import com.rail.utilities.Utils;

public class NetworkDelegate {
	
	private static NetworkDelegate networkDelegate;
	private static Context sContext;
	
	private NetworkDelegate(Context context){
		
	}
	
	public static NetworkDelegate getInstance(Context context){
		sContext = context;
		if(networkDelegate==null){
			networkDelegate= new NetworkDelegate(context);
		}
		
		return networkDelegate;
	}
	
	public String getHttpResponse(String url,String requestType,Map<String,String>params,Map<String,String>headers) {
		HttpClient railHttpClient=	new HttpClient();
		HttpResponseObj httpResponseObj = null;
		if (Utils.isNetworkAvailable(sContext)) {
			try {
				HttpUriRequest httpRequest = railHttpClient.createHttpRequest(url, requestType, params, headers);
				httpResponseObj = railHttpClient.executeRequest(httpRequest);
			} catch (UnsupportedEncodingException e) {
				Log.e("Error",e.getMessage());
			}
			if (httpResponseObj != null) {
				return httpResponseObj.getData();
			}
		}
		return null;
	}
	
	
}
