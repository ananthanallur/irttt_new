package com.rail.network;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.rail.model.PNRData;
import com.rail.model.PNRData.Passenger;

public class ParseHandler {
	
	private static ParseHandler parseHandler;
	
	private ParseHandler(){
		
	}
	
	public static ParseHandler getInstance(){
		if(parseHandler==null){
			parseHandler= new ParseHandler();
		}
		return parseHandler;
	}
	
	public PNRData parsePNRResponse(String data) {
		PNRData pnrData = new PNRData();
		try {
			JSONObject obj = new JSONObject(data);
			if("200".equalsIgnoreCase(obj.getString("response_code"))){
				pnrData.setPnrNumber(obj.getString("pnr"));
				pnrData.setTrainNumber(obj.getString("train_num"));
				pnrData.setTrainName(obj.getString("train_name"));
				pnrData.setDateOfJourney(obj.getString("doj"));
				pnrData.setTypeOfClass(obj.getString("class"));
				pnrData.setNumberOfPassengers(obj.getString("no_of_passengers"));
				pnrData.setChartPrepared(obj.getString("chart_prepared"));
				JSONObject obj2=null;
				String fromTo="";
				if((obj2=obj.getJSONObject("from_station"))!=null){
					String code=obj2.getString("code");
					String name =obj2.getString("name");
					if(name!=null && code!=null){
						fromTo += name+"-"+code;
					}
				}
				if((obj2=obj.getJSONObject("to_station"))!=null){
					String code=obj2.getString("code");
					String name =obj2.getString("name");
					if(name!=null && code!=null){
						fromTo +=" - "+ name+"-"+code;
					}
				}
				pnrData.setFromNTo(fromTo);
				if((obj2=obj.getJSONObject("boarding_point"))!=null){
					String code=obj2.getString("code");
					String name =obj2.getString("name");
					if(name!=null && code!=null){
						pnrData.setReservationFrom(name+"-"+code);
					}
				}
				if((obj2=obj.getJSONObject("reservation_upto"))!=null){
					String code=obj2.getString("code");
					String name =obj2.getString("name");
					if(name!=null && code!=null){
						pnrData.setReservationTo(name+"-"+code);
					}
				}
				
				List<Passenger>passgrs = new ArrayList<PNRData.Passenger>();
				if(obj.get("passengers") instanceof JSONArray){
					JSONArray passengers = obj.getJSONArray("passengers");
					for (int i = 0; i < passengers.length(); i++) {
						Passenger passenger = pnrData.new Passenger();
						JSONObject passObj =  passengers.getJSONObject(i);
						passenger.setPassengerNum(passObj.getString("sr"));
						passenger.setBookingStatus(passObj.getString("booking_status"));
						passenger.setCurrentStatus(passObj.getString("current_status"));
						passgrs.add(passenger);
						
					}
				}else{
					JSONObject passObj =  obj.getJSONObject("passengers");
					Passenger passenger = pnrData.new Passenger();
					passenger.setPassengerNum(passObj.getString("sr"));
					passenger.setBookingStatus(passObj.getString("booking_status"));
					passenger.setCurrentStatus(passObj.getString("curent_status"));
					passgrs.add(passenger);
				}
				pnrData.setPassengers(passgrs);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return pnrData;
	}
	
}
