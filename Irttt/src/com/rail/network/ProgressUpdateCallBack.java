package com.rail.network;

public interface ProgressUpdateCallBack {
	public void updateMyProgress(int value);
}
