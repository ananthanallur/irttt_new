package com.rail.network;

import org.apache.http.client.methods.HttpGet;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;

import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.model.DbDetails;
import com.rail.model.UpdateDetails;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class UpdateFinder {
	private static Context mContext;
	public static final int APKDBCHECK_FAILED = 12;
	public static final int NO_NET_CONNECTION = 14;
	public static final int UPDATES_FOUND = 15;
	public static final int NO_UPDATES_FOUND = 16;
	public static final int APKDBCHECK_SUCCESS = 7;
	private int count = 0;
	private AsyncTask<Void, Integer, UpdateDetails> execute;

	public UpdateFinder(Context context) {
		mContext = context;
	}

	public void findDBUpdate(UpdateFinderResultListener listener) {
		if (!Utils.isNetworkAvailable(mContext)) {
			return;
		}
		execute = new DBUpdateFinderTask(listener).execute();
	}

	public void findAppUpdate(UpdateFinderResultListener listener) {
		if (!Utils.isNetworkAvailable(mContext)) {
			return;
		}
		execute = new AppUpdateFinderTask(listener).execute();
	}

	class DBUpdateFinderTask extends AsyncTask<Void, Integer, UpdateDetails> {
		UpdateFinderResultListener listener;
		FileDownloader downloader;
		boolean canceled = false;

		public void cancel() {
			if (downloader != null)
				downloader.setCanceled(true);
			canceled = true;
		}

		public DBUpdateFinderTask(UpdateFinderResultListener listener) {
			this.listener = listener;
		}

		@Override
		protected UpdateDetails doInBackground(Void... params) {
			return checkDbUpdateInServer(mContext);

		}

		@Override
		protected void onPostExecute(UpdateDetails dbUpdateDetails) {
			super.onPostExecute(dbUpdateDetails);

			SharedPreferences sf = mContext.getSharedPreferences(
					RailTimeConstants.PREFS_NAME, 0);
			Editor ed = sf.edit();
			if (dbUpdateDetails != null ) {
				if(dbUpdateDetails.isUpdateAvailable()){
					ed.putInt(RailTimeConstants.SERVER_DB_VER,
							dbUpdateDetails.getDbVer());
					ed.putString(RailTimeConstants.LATEST_DB_PATH,
							dbUpdateDetails.getDbpath());
					ed.putInt(RailTimeConstants.LATEST_DB_SIZE,
							dbUpdateDetails.getDbSize());
					ed.putString(RailTimeConstants.LATEST_DB_DATE,
							dbUpdateDetails.getDbDate());
					ed.putBoolean(RailTimeConstants.DB_UPDATE_FOUND, true);
					ed.commit();
				}
				listener.onDBUpdateFinderFinished(dbUpdateDetails);
			} else if (count++ < 5) {
				Log.v("tag", "Download failed: Trying count=" + count);
				new DBUpdateFinderTask(listener).execute();
				sf.edit().putBoolean("apkUpdatesFound", false).commit();
				sf.edit().putBoolean(RailTimeConstants.DB_UPDATE_FOUND, false)
						.commit();
			}else{
				listener.onDBUpdateFinderFinished(dbUpdateDetails);
			}

			
		}

		/*
		 * public void updateMyProgress(int value) { publishProgress(value); }
		 */

		@Override
		protected void onProgressUpdate(Integer... values) {
			for (Integer value : values) {
				if (RuntimeData.getProgress_dialog() != null)
					RuntimeData.getProgress_dialog().setProgress(value);
			}
			super.onProgressUpdate(values);
		}

	}

	class AppUpdateFinderTask extends AsyncTask<Void, Integer, UpdateDetails> {
		UpdateFinderResultListener listener;

		public AppUpdateFinderTask(UpdateFinderResultListener listener) {
			this.listener = listener;
		}

		@Override
		protected UpdateDetails doInBackground(Void... params) {
			return checkAppUpdateInServer(mContext);

		}

		@Override
		protected void onPostExecute(UpdateDetails dbUpdateDetails) {
			super.onPostExecute(dbUpdateDetails);
			listener.onAppUpdateFinderFinished(dbUpdateDetails);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			for (Integer value : values) {
				if (RuntimeData.getProgress_dialog() != null)
					RuntimeData.getProgress_dialog().setProgress(value);
			}
			super.onProgressUpdate(values);
		}

	}

	private UpdateDetails checkDbUpdateInServer(Context context) {
		mContext = context;
		String url = UrlBuilder.getURLFor(UrlBuilder.DB_CHECK_API,
				new String[] { Utils.getAppVersion(context), getDBVersion() });
		System.out.println(url);
		NetworkDelegate networkDelegate = NetworkDelegate.getInstance(context);
		String httpResponse = networkDelegate.getHttpResponse(url,
				HttpGet.METHOD_NAME, null, null);
		try {
			return parseDBResponse(httpResponse);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private UpdateDetails checkAppUpdateInServer(Context context) {
		mContext = context;
		String url = UrlBuilder.getURLFor(UrlBuilder.APP_UPDATE_CHECK_API,
				new String[] { Utils.getAppVersion(context) });
		System.out.println(url);
		NetworkDelegate networkDelegate = NetworkDelegate.getInstance(context);
		String httpResponse = networkDelegate.getHttpResponse(url,
				HttpGet.METHOD_NAME, null, null);
		try {
			return parseAppResponse(httpResponse);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String getDBVersion() {
		DbDetails dbDt = FrontController.getInstance(mContext)
				.checkAndGetRailDbDet();
		if ((dbDt != null)) {
			return dbDt.getDbver() + "";
		} else {
			return "0";
		}
	}

	

	private static UpdateDetails parseDBResponse(String httpResponse)
			throws JSONException {
		if (httpResponse != null && httpResponse.length() > 0) {
			JSONObject jObject = new JSONObject(httpResponse);
			if (jObject.has("response_code")) {
				Integer responseCode = Integer.parseInt((String) jObject
						.get("response_code"));
				if (responseCode == 0) {
					UpdateDetails dbUpdateDetails = new UpdateDetails();
					dbUpdateDetails.setUpdateAvailable(false);
					return dbUpdateDetails;
				} else if (responseCode == 3) {
					UpdateDetails dbUpdateDetails = new UpdateDetails();
					dbUpdateDetails.setUpdateAvailable(true);
					String dbPath = (String) jObject.get("newdbpath");
					dbUpdateDetails.setDbpath(dbPath.replace("\\", ""));
					String dbDate = (String) jObject.get("newdbdate");
					dbUpdateDetails.setDbDate(dbDate);
					String dbVer = (String) jObject.get("newdbver");
					dbUpdateDetails.setDbVer(Integer.parseInt(dbVer));
					String dbsize = (String) jObject.get("newdbsize");
					dbUpdateDetails.setDbSize(Integer.parseInt(dbsize));
					return dbUpdateDetails;
				} else {
					Log.i("Error", "DB check failed with error" + responseCode);
				}
			}
		}
		return null;
	}

	private static UpdateDetails parseAppResponse(String httpResponse)
			throws JSONException {
		if (httpResponse != null && httpResponse.length() > 0) {
			JSONObject jObject = new JSONObject(httpResponse);
			if (jObject.has("response_code")) {
				Integer responseCode = Integer.parseInt((String) jObject
						.get("response_code"));
				if (responseCode == 0) {
					UpdateDetails dbUpdateDetails = new UpdateDetails();
					dbUpdateDetails.setUpdateAvailable(false);
					return dbUpdateDetails;
				} else if (responseCode == 3) {
					UpdateDetails dbUpdateDetails = new UpdateDetails();
					dbUpdateDetails.setUpdateAvailable(true);
					String appVer = (String) jObject.get("newappver");
					dbUpdateDetails.setAppVer(Integer.parseInt(appVer));
					return dbUpdateDetails;
				} else {
					Log.i("Error", "DB check failed with error" + responseCode);
				}
			}
		}
		return null;
	}

	public void cancel() {
		execute.cancel(true);
	}

}
