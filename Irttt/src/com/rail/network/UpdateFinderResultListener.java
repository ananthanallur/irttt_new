package com.rail.network;

import com.rail.model.UpdateDetails;

public interface UpdateFinderResultListener {
	public void onDBUpdateFinderFinished(UpdateDetails updateDetails);
	public void onAppUpdateFinderFinished(UpdateDetails updateDetails);
}
