package com.rail.network;

import java.util.Locale;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;

@SuppressLint("DefaultLocale")
public class UrlBuilder {

	public final static int PNR_API = 6789;
	public final static int DB_CHECK_API = 6790;
	public final static int APP_UPDATE_CHECK_API = 6791;
	public final int SEAT_AVAILABILITY = PNR_API + 1;
	public final int ROUTE_SCHEDULE = PNR_API + 2;
	public final int LIVE_STATUS = PNR_API + 3;
	private static final String lvpbapikey = "cce2e7ed8f3cd19021c95d87bd70a92c";
	private static final String lvprapikey = "3993088988d8620b244b9282bafd133e";
	private static final String lvbaseUrl = "http://www.livetrainstatusapi.com/test/status/";

	private static final String pbapikey = "7196097a243c509cfeb1d1e2458ccf29";
	private static final String prapikey = "46c834ac0d3c65a7ec339cf4a2fefe56";
	private static final String baseUrl = "http://railpnrapi.com/api/";

	private static final String irtttbaseUrl = "http://1-dot-lucid-inquiry-752.appspot.com/";
	// private static final String irtttbaseUrl = "http://127.0.0.1:57061/";
	// public static String VERSION_XML_URL =
	// "http://storage.googleapis.com/allapps/irttt/apkdetails.xml";
	// public static String VERSION_XML_URL =
	// "http://www.spirulite.com/railapks/irttt/apkdetails.xml";
	// public static String PKG_ROOT_PATH = "/data/data/com.rail.time/";
	// public static String INITIAL_DB_PATH =
	// "http://www.spirulite.com/railapks/trainDb1";
	// public static String FINAL_DB_FOLDER =
	// "/data/data/com.rail.time/databases/";

	private static final String databaseInfo = "getDatabaseInfo.ws";
	private static final String appUpdateInfo =	"getAppUpdateInfo.ws";

	private static String arranged_parm_values = null;
	private static String pbapisign = null;

	public void test() {
		String[] args_avail = { "16232", "SBC", "MV", "02-10-2014", "CC", "GN" };
		String[] args_route = { "16232" };
		String[] args_live = { "16232", "SBC", "20141002" /* date in YYYYMMDD */};
		String res = getURLFor(PNR_API, new String[] { "1234567890" });
		System.out.println("---" + res);
		res = getURLFor(SEAT_AVAILABILITY, args_avail);
		System.out.println("---" + res);
		res = getURLFor(ROUTE_SCHEDULE, args_route);
		System.out.println("---" + res);
		res = getURLFor(LIVE_STATUS, args_live);
		System.out.println("---" + res);
	}

	public static String getURLFor(int type, String[] args) {
		String formedUrl = null;
		if (args == null || args.length < 1)
			return null;

		switch (type) {
		case PNR_API:
			arranged_parm_values = "json" + pbapikey + args[0];
			pbapisign = generatehmaccode(prapikey, arranged_parm_values)
					.toLowerCase(Locale.US);
			formedUrl = baseUrl + "check_pnr/pnr/" + args[0]
					+ "/format/json/pbapikey/" + pbapikey + "/pbapisign/"
					+ pbapisign;
			break;

		case DB_CHECK_API:
			String pbapikeyStr = "7196097a243c509cfeb1d1e2458ccf29";
			String arranged_parm_values = "json" + pbapikey + args[0] != null ? args[0]
					: "";
			String pbapisignStr = generatehmaccode(pbapikeyStr,
					arranged_parm_values).toLowerCase(Locale.US);
			formedUrl = irtttbaseUrl + databaseInfo + "?localappver=" + args[0]
					+ "&localdbver=" + args[1] + "&pbapisign=" + pbapisignStr
					+ "&app=IRTTT";
			break;
			
		case APP_UPDATE_CHECK_API:
			pbapikeyStr = "7196097a243c509cfeb1d1e2458ccf29";
			arranged_parm_values = "json" + pbapikey + args[0] != null ? args[0]
					: "";
			pbapisignStr = generatehmaccode(pbapikeyStr,
					arranged_parm_values).toLowerCase(Locale.US);
			formedUrl = irtttbaseUrl + appUpdateInfo + "?localappver=" + args[0]
					+ "&pbapisign=" + pbapisignStr
					+ "&app=IRTTT";
			break;

		/*
		 * case SEAT_AVAILABILITY: // Args tnum, fscode, tscode, date
		 * (dd-mm-yyyy), class, quota if (args.length < 6) return null;
		 * arranged_parm_values = args[4] + args[3] + "json" + args[1] +
		 * pbapikey + args[5] + args[0] + args[2] ; arranged_parm_values =
		 * arranged_parm_values.toLowerCase(Locale.US); pbapisign =
		 * generatehmaccode(prapikey, arranged_parm_values)
		 * .toLowerCase(Locale.US); formedUrl = baseUrl + "check_avail/tnum/" +
		 * args[0] + "/fscode/" + args[1] + "/tscode/" + args[2] + "/date/" +
		 * args[3] + "/class/" + args[4] + "/quota/" + args[5] +
		 * "/format/json/pbapikey/" + pbapikey + "/pbapisign/" + pbapisign;
		 * break; case ROUTE_SCHEDULE: // Args one train if (args.length < 1)
		 * return null; arranged_parm_values = "json" + pbapikey + args[0];
		 * arranged_parm_values = arranged_parm_values.toLowerCase(Locale.US);
		 * pbapisign = generatehmaccode(prapikey, arranged_parm_values)
		 * .toLowerCase(Locale.US); formedUrl = baseUrl + "route/train/" +
		 * args[0] + "/format/json/pbapikey/" + pbapikey + "/pbapisign/" +
		 * pbapisign; break; case LIVE_STATUS: //Args tnum, scode, date ( format
		 * as yyyymmdd) if (args.length < 3) return null; arranged_parm_values =
		 * lvpbapikey + args[2] + "json" + args[1] + args[0];
		 * arranged_parm_values = arranged_parm_values.toLowerCase(Locale.US);
		 * pbapisign = generatehmaccode(lvprapikey, arranged_parm_values)
		 * .toLowerCase(Locale.US); formedUrl = lvbaseUrl + "tnum/" + args[0] +
		 * "/scode/" + args[1] + "/date/" + args[2] + "/format/json/apikey/" +
		 * lvpbapikey + "/apisign/" + pbapisign; break;
		 */
		}
		return formedUrl;
	}

	private static String generatehmaccode(String mykey, String input) {
		try {
			Mac mac = Mac.getInstance("HmacSHA1");
			SecretKeySpec secret = new SecretKeySpec(mykey.getBytes(),
					mac.getAlgorithm());
			mac.init(secret);
			byte[] digest = mac.doFinal(input.getBytes());
			final char[] hexArray = "0123456789ABCDEF".toCharArray();
			char[] hexChars = new char[digest.length * 2];
			for (int j = 0; j < digest.length; j++) {
				int v = digest[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
				hexChars[j * 2 + 1] = hexArray[v & 0x0F];
			}

			return new String(hexChars);

		} catch (Exception e) {
			return null;
		}
	}
}