package com.rail.persistance;

import java.io.File;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.rail.controller.RailTimeConstants;
import com.rail.model.DbDetails;

public class DatabaseHandler {

	private static SQLiteDatabase myDb;
	private static Context sContext;
	private static DatabaseHandler sDBHandler;

	protected SQLiteDatabase getDatabase() {
		return getMyDb();
	}

	private DatabaseHandler(Context context) {
		myDb = openDatabase(true);
	}

	public static DatabaseHandler getInstance(Context context) {
		sContext = context;
		if (null == sDBHandler || myDb == null
				|| (myDb != null && !myDb.isOpen())) {
			sDBHandler = new DatabaseHandler(context);
		}
		return sDBHandler;
	}

	public SQLiteDatabase openDatabase(boolean force) {

		if (myDb != null && force == true && myDb.isOpen())
			myDb.close();
		if (myDb != null && force == false)
			return myDb;

		myDb = null;
		File root = Environment.getExternalStorageDirectory();
		if (root != null) {
			File dbdir = new File(root, sContext.getPackageName());
			File dbloc = new File(dbdir, RailTimeConstants.DB_NAME);
			if (!dbloc.exists()) {
				myDb = null;
			} else {
				myDb = SQLiteDatabase.openDatabase(dbloc.getAbsolutePath(),
						null, SQLiteDatabase.OPEN_READONLY);

			}
		}
		return myDb;
	}

	public boolean isDBPresent() {
		try {
			if (myDb != null && myDb.isOpen()) {
				myDb.close();
			}
			myDb = openDatabase(true);
			if (myDb == null) {
				System.out.println("Debug: DB is absent" );
				return false;
			} else {
				System.out.println("Debug: DB is present" );
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Debug: DB is absent" );
			return false;
		}
	}

	public SQLiteDatabase getMyDb() {
		return myDb;
	}

	public void setMyDb(SQLiteDatabase myDb) {
		DatabaseHandler.myDb = myDb;
	}

	public DbDetails getRailDbDet() {
		if (myDb == null)
			return null;
		else if (!myDb.isOpen()) {
			myDb = openDatabase(true);
		}
		String getDb = "select dbver,dbdate from db_information";
		// String [] trains = {trainNo};
		try {
			Cursor cur = myDb.rawQuery(getDb, null);
			if (cur != null && !cur.isClosed()) {
				int coldbver = cur.getColumnIndex("dbver");
				int coldbdate = cur.getColumnIndex("dbdate");
				if (cur.moveToFirst()) {
					DbDetails dt = new DbDetails();

					dt.setDbver(cur.getInt(coldbver));
					dt.setDbdate(cur.getString(coldbdate));
					if (dt.getDbdate() == null)
						dt.setDbdate("24-Jun-2012");
					cur.close();
					return dt;
				}
				cur.close();
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}

	public void closeDB() {
		if (myDb != null && myDb.isOpen()) {
			myDb.close();
			myDb = null;
		}
	}

}
