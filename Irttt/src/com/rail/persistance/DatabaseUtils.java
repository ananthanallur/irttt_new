package com.rail.persistance;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.inputmethod.InputMethodManager;

import com.rail.controller.RailTimeConstants;
import com.rail.controller.StationGroups;
import com.rail.model.FinalDataReturned;
import com.rail.model.RowOf7Strings;
import com.rail.model.ScreenData;
import com.rail.model.SimpleTrainDetails;
import com.rail.model.Station;
import com.rail.model.TdForSrcToDst;
import com.rail.model.TdForSrcToDstForStnGrp;
import com.rail.model.TrainDetWithRoute;
import com.rail.model.TrainDetails;
import com.rail.model.TrainRoute;
import com.rail.utilities.Utils;

public class DatabaseUtils {
	private static DatabaseUtils sDatabaseUtils;
	private static Context sContext;

	private DatabaseUtils(Context context) {
		sContext = context;
	}

	public static DatabaseUtils getInstance(Context context) {
		if (sDatabaseUtils == null) {
			sDatabaseUtils = new DatabaseUtils(context);
		}

		return sDatabaseUtils;
	}

	public List<String> getMatchingStations(String name_in) {
		// If string contains hyphen remove it
		String[] new_name = name_in.split(" - ");
		SQLiteDatabase myDb = DatabaseHandler.getInstance(sContext)
				.getDatabase();
		if (new_name.length <= 0)
			return null;
		String name = new_name[0];
		if (myDb == null)
			return null;
		ArrayList<String> stations = new ArrayList<String>();
		String getStaionDet = "SELECT stationName,stationCode FROM station_table WHERE stationName LIKE ? LIMIT 20";
		String[] stArr = { "%" + name + "%" };
		Cursor cur1 = myDb.rawQuery(getStaionDet, stArr);
		if (cur1 != null && !cur1.isClosed()) {
			while (cur1.moveToNext()) {
				String stationName = cur1.getString(0);
				String stationCode = cur1.getString(1);
				stations.add(stationName + " - " + stationCode);
			}
			cur1.close();
		}

		if (name.length() <= 5) {
			String getStaionDet1 = "SELECT stationName,stationCode FROM station_table WHERE stationCode LIKE ? LIMIT 2";
			Cursor cur2 = myDb.rawQuery(getStaionDet1, stArr);
			if (cur2 != null && !cur2.isClosed()) {
				while (cur2.moveToNext()) {
					String stationName = cur2.getString(0);
					String stationCode = cur2.getString(1);
					stations.add(stationName + " - " + stationCode);
				}
				cur2.close();
			}
		}
		return stations;
	}

    public List<Object> getTrainsFromDB(String name_in) {

        List<Object> trains = new ArrayList<Object>();

        String regex = "\\d+";

        String getStaionDet = "SELECT trainName,trainNO FROM train_table WHERE trainName LIKE ? LIMIT 200";
        String[] stArr = {name_in + "%"};
        if (name_in.matches(regex)) {
            getStaionDet = "SELECT trainName,trainNO FROM train_table WHERE trainNO LIKE ? LIMIT 200";
            stArr = new String []{name_in + "%"};
        }
        Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
                .rawQuery(getStaionDet, stArr);

        if (cur1 != null && !cur1.isClosed()) {
            while (cur1.moveToNext()) {
                trains.add(cur1.getString(1)+" - "+cur1.getString(0));
            }
            cur1.close();
        }

        return trains;
    }


	public FinalDataReturned getTrainsMatchingForDay(ScreenData screenData) {
		TdForSrcToDstForStnGrp trainBwSrcNDstGrp = getListOfTrainsFromSrcToDest(
                screenData.getSource(), screenData.getDest(),
                screenData.isConsiderNearbyTrains());
		FinalDataReturned finalData = new FinalDataReturned();
		finalData.setScrId(RailTimeConstants.SCR7);

		if (trainBwSrcNDstGrp == null) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRANS_BW_SRC_N_DST);
			return finalData; // Not expected.
		}

		if (trainBwSrcNDstGrp.getErrorCode() != 0) {
			finalData.setErrCode(trainBwSrcNDstGrp.getErrorCode());
			return finalData;
		}

		List<TdForSrcToDst> trainsFromSrcToDst = trainBwSrcNDstGrp
				.getTdForSrcToDstarray();

		if (trainsFromSrcToDst == null) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRANS_BW_SRC_N_DST);
			return finalData;
		}

		int validTrains = 0;
		finalData.setRows(new ArrayList<RowOf7Strings>());

		for (TdForSrcToDst tdFSrc2Dst : trainsFromSrcToDst) {
			int screenDayofWeek = screenData.getDate()
					.get(Calendar.DAY_OF_WEEK);
			TrainDetails td = tdFSrc2Dst.getTd();

			int dayconsidered = (screenDayofWeek - tdFSrc2Dst.getSrcDatePlus() + 6) % 7;
			if (!(td.getScheduleFromSunday()[dayconsidered])) {
			} else {
				validTrains++;
				RowOf7Strings row = new RowOf7Strings();
				row.setCol1(td.getTrainName());
				row.setCol2(td.getTrainNo());
				row.setCol3(tdFSrc2Dst.getSrcArr());
				row.setCol4("   " + tdFSrc2Dst.getDstArr());
				boolean added = false;
				for (int trinpos = 0; trinpos < validTrains - 1; trinpos++) {
					String arr = finalData.getRows().get(trinpos).getCol3();

					if (arr.compareTo(tdFSrc2Dst.getSrcArr()) > 0) {
						finalData.getRows().add(trinpos, row);
						added = true;
						break;
					}
				}
				if (!added) {
					finalData.getRows().add(row);
				}
			}
		}

		if (validTrains == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_SERVICE_NO_FOR_DAY);
			return finalData;
		}

		screenData.saveToLatestValidData();

		return finalData;

	}

	public TdForSrcToDstForStnGrp getListOfTrainsFromSrcToDest(Station src,
			Station dst, boolean considerStnGrp) {
		if (DatabaseHandler.getInstance(sContext).getDatabase() == null)
			return null;

		TdForSrcToDstForStnGrp tdForSrc2DstGrp = new TdForSrcToDstForStnGrp();

		List<TdForSrcToDst> trains = new ArrayList<TdForSrcToDst>();

		List<TrainRoute> srcTrains = getPassingThroughTrainRouteData(src,
				considerStnGrp, tdForSrc2DstGrp, true);
		List<TrainRoute> dstTrains = getPassingThroughTrainRouteData(dst,
				considerStnGrp, tdForSrc2DstGrp, false);

		if (srcTrains == null || dstTrains == null) {
			return null;
		}

		for (TrainRoute srcTrain : srcTrains) {
			for (TrainRoute dstTrain : dstTrains) {
				if (srcTrain.getTid() == dstTrain.getTid()
						&& srcTrain.getRtid() <= dstTrain.getRtid()) {
					TrainDetails td = getTrainDetails(srcTrain.getTid());
					if (td == null)
						return null;
					TdForSrcToDst tdFS2D = new TdForSrcToDst();
					tdFS2D.setTd(td);
					tdFS2D.setSrcArr(srcTrain.getArriaval());
					tdFS2D.setDstArr(dstTrain.getArriaval());
					tdFS2D.setSrcDatePlus(srcTrain.getDatePlus());
					tdFS2D.setDstDatePlus(dstTrain.getDatePlus());
					trains.add(tdFS2D);
				}
			}
		}

		tdForSrc2DstGrp.setTdForSrcToDstarray(trains);
		return tdForSrc2DstGrp;
	}

	public TrainDetails getTrainDetails(int tid) {
		if (DatabaseHandler.getInstance(sContext).getDatabase() == null)
			return null;
		String getTrain = "select trainName,trainNO,sun,mon,tue,wed,thu,fri,sat from train_table where _id=?";
		String[] stArr = { Integer.valueOf(tid).toString() };
		Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getTrain, stArr);
		TrainDetails td = new TrainDetails();
		if (cur != null && !cur.isClosed()) {
			if (cur.getCount() <= 0) {
				cur.close();
				return null;
			}

			td.setScheduleFromSunday(new boolean[7]);
			cur.moveToFirst();
			td.setTid(tid);
			td.setTrainName(cur.getString(0));
			td.setTrainNo(cur.getString(1));

			String boolVal = cur.getString(2);
			setScheduleFomSunday(td, boolVal, 0);

			boolVal = cur.getString(3);
			setScheduleFomSunday(td, boolVal, 1);

			boolVal = cur.getString(4);
			setScheduleFomSunday(td, boolVal, 2);

			boolVal = cur.getString(5);
			setScheduleFomSunday(td, boolVal, 3);

			boolVal = cur.getString(6);
			setScheduleFomSunday(td, boolVal, 4);

			boolVal = cur.getString(7);
			setScheduleFomSunday(td, boolVal, 5);

			boolVal = cur.getString(8);
			setScheduleFomSunday(td, boolVal, 6);
			cur.close();
		}
		return td;
	}

	private void setScheduleFomSunday(TrainDetails td, String boolVal, int index) {
		if (boolVal.equals("1"))
			td.getScheduleFromSunday()[index] = true;
		else
			td.getScheduleFromSunday()[index] = false;
	}

	public List<TrainRoute> getPassingThroughTrainRouteData(Station s,
			boolean considerStnGrp, TdForSrcToDstForStnGrp tdForSrc2DstGrp,
			boolean srcStn) {
		if (DatabaseHandler.getInstance(sContext).getDatabase() == null)
			return null;
		int stnGrpId;

		ArrayList<String> grpStations;
		if (considerStnGrp) {
			stnGrpId = StationGroups.getGroupStationsId(s.getStationCode());
			if (stnGrpId != -1) {
				grpStations = StationGroups.getGroupStations(stnGrpId);
			} else {
				grpStations = createStCodArr(s.getStationCode());
			}
			if (srcStn)
				tdForSrc2DstGrp.setSrcStationGroupId(stnGrpId);
			else
				tdForSrc2DstGrp.setDestStationGroupId(stnGrpId);
		} else {
			tdForSrc2DstGrp.setSrcStationGroupId(-1);
			tdForSrc2DstGrp.setDestStationGroupId(-1);
			grpStations = createStCodArr(s.getStationCode());
		}

		ArrayList<TrainRoute> trainRoutes = new ArrayList<TrainRoute>();
		String getRoutes = "SELECT _id, trainId, arrival,"
				+ " datePlus, halt, dist FROM route_table"
				+ " where stationId=?";

		String getStaionDet = "SELECT _id,stationName FROM station_table WHERE stationCode=? LIMIT 1";
		String stationName = "";
		for (String stCode : grpStations) {
			String[] stArr = { stCode };
			Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
					.rawQuery(getStaionDet, stArr);
			int stId = -1;
			if (cur1 != null && !cur1.isClosed()) {
				if (cur1.getCount() <= 0) {
					cur1.close();
					continue;
				}

				cur1.moveToFirst();
				stId = cur1.getInt(0);
				stationName = cur1.getString(1);
				cur1.close();
			}

			String[] stIdArr = { Integer.valueOf(stId).toString() };
			Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
					.rawQuery(getRoutes, stIdArr);
			if (cur != null && !cur.isClosed()) {
				if (cur.getCount() <= 0) {
					cur.close();
					continue;
				}

				cur.moveToFirst();
				for (int i = 0; i < cur.getCount(); i++) {
					TrainRoute tr = new TrainRoute();
					tr.setStationCode(stCode);
					tr.setStationName(stationName);
					tr.setRtid(cur.getInt(0));
					tr.setTid(cur.getInt(1));
					tr.setArriaval(cur.getString(2));
					tr.setDatePlus(cur.getInt(3));
					tr.setHalt(cur.getInt(4));
					tr.setDist(cur.getString(5));
					boolean added = false;
					for (TrainRoute rt : trainRoutes) {
						if (rt.getTid() == tr.getTid()) {
							if (tr.getStationCode().equalsIgnoreCase(
									s.getStationCode())) {
								trainRoutes.remove(rt);
								trainRoutes.add(tr);
							}
							added = true;
							break;
						}
					}
					if (!added)
						trainRoutes.add(tr);
					cur.moveToNext();
				}
				cur.close();
			}
		}
		return trainRoutes;
	}

	public ArrayList<String> createStCodArr(String stCode) {
		ArrayList<String> stCodeArr = new ArrayList<String>();
		stCodeArr.add(stCode);
		return stCodeArr;
	}

	public TrainDetWithRoute getCompleteRoute(String trainNo,
			ScreenData screenData, boolean checkDay) {

		if (DatabaseHandler.getInstance(sContext) == null)
			return null;
		String getRoute = "SELECT c._id, b.stationCode, b.stationName, c.arrival,"
				+ " c.datePlus, c.halt, c.dist FROM route_table AS c"
				+ " INNER JOIN station_table AS b WHERE c.trainId=? AND"
				+ " c.stationId=b._id";

		TrainDetails td = getTrainDetails(trainNo);
		if (td == null) {
			return null;
		}

		ArrayList<TrainRoute> trainRoute = new ArrayList<TrainRoute>();
		String[] stArr = { Integer.valueOf(td.getTid()).toString() };
		Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getRoute, stArr);
		if (cur != null && !cur.isClosed()) {
			if (cur.getCount() <= 0) {
				cur.close();
				return null;
			}
			cur.moveToFirst();

			for (int i = 0; i < cur.getCount(); i++) {
				TrainRoute tr = new TrainRoute();
				tr.setTid(td.getTid());
				tr.setRtid(cur.getInt(0));
				tr.setStationCode(cur.getString(1));
				tr.setStationName(cur.getString(2));
				tr.setArriaval(cur.getString(3));
				tr.setDatePlus(cur.getInt(4));
				tr.setHalt(cur.getInt(5));
				tr.setDist(cur.getString(6));

				trainRoute.add(tr);
				cur.moveToNext();
			}
			cur.close();
		}
		TrainDetWithRoute tdWr = new TrainDetWithRoute(td, trainRoute);
		sanityCheckAndFillErrorcode(tdWr, screenData, checkDay);
		return tdWr;
	}

	public boolean isTrainExist(String trainNo, ScreenData screenData,
			boolean checkDay) {

		if (DatabaseHandler.getInstance(sContext) == null)
			return false;
		String getRoute = "SELECT c._id, b.stationCode, b.stationName, c.arrival,"
				+ " c.datePlus, c.halt, c.dist FROM route_table AS c"
				+ " INNER JOIN station_table AS b WHERE c.trainId=? AND"
				+ " c.stationId=b._id";

		TrainDetails td = getTrainDetails(trainNo);
		if (td == null) {
			return false;
		}

		String[] stArr = { Integer.valueOf(td.getTid()).toString() };
		Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getRoute, stArr);
		if (cur != null && !cur.isClosed()) {
			if (cur.getCount() <= 0) {
				cur.close();
				return false;
			}
			cur.close();
		}
		return true;
	}

	public TrainDetails getTrainDetails(String trainNo) {
		if (DatabaseHandler.getInstance(sContext) == null)
			return null;
		TrainDetails td = null;
		String getTrain = "select _id,trainName,sun,mon,tue,wed,thu,fri,sat from train_table where trainNo=?";
		String[] stArr = { trainNo };
		Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getTrain, stArr);
		if (cur != null && !cur.isClosed()) {
			if (cur.getCount() <= 0) {
				cur.close();
				return null;
			}
			td = new TrainDetails();
			td.setScheduleFromSunday(new boolean[7]);
			cur.moveToFirst();
			td.setTrainNo(trainNo);
			td.setTid(cur.getInt(0));
			td.setTrainName(cur.getString(1));

			String boolVal = cur.getString(2);
			setScheduleFomSunday(td, boolVal, 0);
			boolVal = cur.getString(3);
			setScheduleFomSunday(td, boolVal, 1);
			boolVal = cur.getString(4);
			setScheduleFomSunday(td, boolVal, 2);
			boolVal = cur.getString(5);
			setScheduleFomSunday(td, boolVal, 3);
			boolVal = cur.getString(6);
			setScheduleFomSunday(td, boolVal, 4);
			boolVal = cur.getString(7);
			setScheduleFomSunday(td, boolVal, 5);
			boolVal = cur.getString(8);
			setScheduleFomSunday(td, boolVal, 6);
			cur.close();
		}
		return td;
	}

	// Assumption is that train number was present.
	public void sanityCheckAndFillErrorcode(TrainDetWithRoute tdWrt,
			ScreenData screenData, boolean checkDay) {

		if (tdWrt == null)
			return;

		if (screenData == null) {
			tdWrt.setErrorCode(0); // No error return;
			return;
		}

		tdWrt.setOtherGroupSrcStation(false);
		tdWrt.setOtherGroupDstStation(false);

		int sourcePos = -1;
		int destPos = -1;
		TrainRoute source = null;
		TrainRoute dest = null;

		TrainDetails trainDetails = tdWrt.getTd();
		ArrayList<TrainRoute> trainRoute = tdWrt.getRt();

		// Check if train passes through source or dest
		for (int i = 0; i < trainRoute.size(); i++) {
			if (source == null && screenData.getSource() != null) {
				if (trainRoute.get(i).getStationCode().trim()
						.equals(screenData.getSource().getStationCode().trim())) {
					source = trainRoute.get(i);
					sourcePos = i;
				}
			}
			if (dest == null && screenData.getDest() != null) {
				if (trainRoute.get(i).getStationCode().trim()
						.equals(screenData.getDest().getStationCode().trim())) {
					dest = trainRoute.get(i);
					destPos = i;
				}
			}
		}

		if (screenData.getSource() != null && source == null) {
			int gId = StationGroups.getGroupStationsId(screenData.getSource()
					.getStationCode());
			if (screenData.isConsiderNearbyTrains() && gId != -1) {
				ArrayList<String> stCodes = StationGroups.getGroupStations(gId);

				for (int i = 0; i < trainRoute.size(); i++) {

					for (String stCode : stCodes) {
						if (trainRoute.get(i).getStationCode().trim()
								.equals(stCode.trim())) {
							source = trainRoute.get(i);
							sourcePos = i;
							tdWrt.setOtherGroupSrcStation(true);
							break;
						}
					}
				}
			}
		}

		if (screenData.getSource() != null && source == null) {
			tdWrt.setErrorCode(RailTimeConstants.TRAIN_DOES_NOT_PASS_SRC);
			return;
		}

		if (screenData.getDest() != null && dest == null) {
			int gId = StationGroups.getGroupStationsId(screenData.getDest()
					.getStationCode());
			if (screenData.isConsiderNearbyTrains() && gId != -1) {
				ArrayList<String> stCodes = StationGroups.getGroupStations(gId);

				for (int i = 0; i < trainRoute.size(); i++) {

					for (String stCode : stCodes) {
						if (trainRoute.get(i).getStationCode().trim()
								.equals(stCode.trim())) {
							dest = trainRoute.get(i);
							destPos = i;
							tdWrt.setOtherGroupDstStation(true);
							break;
						}
					}

				}
			}
		}
		if (screenData.getDest() != null && dest == null) {
			tdWrt.setErrorCode(RailTimeConstants.TRAIN_DOES_NOT_PASS_DST);
			return;
		}

		if (sourcePos != -1 && destPos != -1 && destPos < sourcePos) {
			tdWrt.setErrorCode(RailTimeConstants.TRAIN_PASSES_IN_OPPOSITE_DIR);
			return;
		}

		if (checkDay) {
			int screenDayofWeek = screenData.getDate()
					.get(Calendar.DAY_OF_WEEK);
			if (source != null) {
				int dayConsidered = (screenDayofWeek - source.getDatePlus() + 6) % 7;
				if (!(trainDetails.getScheduleFromSunday()[dayConsidered])) {
					tdWrt.setErrorCode(RailTimeConstants.TRAIN_SERVICE_NO_FOR_DAY);
					return;
				}
			}

			if (sourcePos == -1 && dest != null) {
				int dayConsidered = (screenDayofWeek - dest.getDatePlus() + 6) % 7;
				if (!(trainDetails.getScheduleFromSunday()[dayConsidered])) {
					tdWrt.setErrorCode(RailTimeConstants.TRAIN_SERVICE_NO_FOR_DAY);
					return;
				}

			}

			if (source == null && dest == null) {
				int dayConsidered = (screenDayofWeek + 6) % 7;
				if (!(trainDetails.getScheduleFromSunday()[dayConsidered])) {
					tdWrt.setErrorCode(RailTimeConstants.TRAIN_SERVICE_NO_FOR_DAY);
					return;
				}
			}
		}
		tdWrt.setSrcRtPos(sourcePos);
		tdWrt.setDstRtPos(destPos);
	}

	public FinalDataReturned getTrainsMatchingForWeek(ScreenData screenData) {
		// MyLog.v("showMySCR8");
		TdForSrcToDstForStnGrp trainBwSrcNDstGrp = getListOfTrainsFromSrcToDest(
				screenData.getSource(), screenData.getDest(),
				screenData.isConsiderNearbyTrains());

		List<TdForSrcToDst> trainsFromSrcToDst = trainBwSrcNDstGrp
				.getTdForSrcToDstarray();
		FinalDataReturned finalData = new FinalDataReturned();
		finalData.setScrId(RailTimeConstants.SCR8);

		if (trainBwSrcNDstGrp == null || trainBwSrcNDstGrp.getErrorCode() != 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRANS_BW_SRC_N_DST);
			return finalData;
		}

		int validTrains = 0;

		StringBuilder sb;
		finalData.setRows(new ArrayList<RowOf7Strings>());

		for (TdForSrcToDst tdFSrc2Dst : trainsFromSrcToDst) {

			RowOf7Strings row = new RowOf7Strings();
			validTrains++;
			TrainDetails td = tdFSrc2Dst.getTd();

			row.setCol1(td.getTrainNo());
			row.setCol2(td.getTrainName());
			row.setCol3(tdFSrc2Dst.getSrcArr());
			row.setCol4(tdFSrc2Dst.getDstArr());
			row.setDays(new boolean[7]);

			for (int daysOfWeek = 0; daysOfWeek < 7; daysOfWeek++) {

				int dayconsider = (daysOfWeek - tdFSrc2Dst.getSrcDatePlus() + 7) % 7;

				if (td.getScheduleFromSunday()[dayconsider])
					row.getDays()[daysOfWeek] = true;
				else {
					row.getDays()[daysOfWeek] = false;
				}

			}
			boolean added = false;
			for (int trinpos = 0; trinpos < validTrains - 1; trinpos++) {
				String arr = finalData.getRows().get(trinpos).getCol3();
				if (arr.compareTo(tdFSrc2Dst.getSrcArr()) > 0) {
					finalData.getRows().add(trinpos, row);
					added = true;
					break;
				}
			}
			if (!added)
				finalData.getRows().add(row);
		}

		if (validTrains == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRANS_BW_SRC_N_DST);
			return finalData;
		}

		sb = new StringBuilder();
		sb.append("From " + screenData.getSource().getStationName());
		sb.append(" To " + screenData.getDest().getStationName());

		finalData.setHeading(sb.toString());

		screenData.saveToLatestValidData();

		return finalData;

	}

	public FinalDataReturned getPassingTrainsForDay(ScreenData screenData) {
		TdForSrcToDstForStnGrp trainBwSrcNDstGrp = new TdForSrcToDstForStnGrp();
		FinalDataReturned finalData = new FinalDataReturned();
		List<TrainRoute> stationTrains = getPassingThroughTrainRouteData(
				screenData.getSource(), screenData.isConsiderNearbyTrains(),
				trainBwSrcNDstGrp, true);

		finalData.setScrId(RailTimeConstants.SCR9);

		if (stationTrains == null || stationTrains.size() == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRAINS_FOR_STN); // TODO
			return finalData;
		}

		finalData.setRows(new ArrayList<RowOf7Strings>());

		int validTrains = 0;
		for (int i = 0; i < stationTrains.size(); i++) {

			TrainDetails td = getTrainDetails(stationTrains.get(i).getTid());
			if (td == null)
				continue;

			// check if train runs on the specified date.
			int screenDayofWeek = screenData.getDate()
					.get(Calendar.DAY_OF_WEEK);
			int dayConsidered = (screenDayofWeek
					- stationTrains.get(i).getDatePlus() + 6) % 7;
			if (!(td.getScheduleFromSunday()[dayConsidered])) {
				continue;
			}

			validTrains++;
			RowOf7Strings row = new RowOf7Strings();

			row.setCol2(td.getTrainName());
			row.setCol1(td.getTrainNo());
			row.setCol3(stationTrains.get(i).getArriaval());
			row.setCol4(stationTrains.get(i).getHalt() + "m");
			row.setCol5(getNextStation(stationTrains.get(i).getRtid(),
					stationTrains.get(i).getTid()));

			boolean added = false;
			for (int trinpos = 0; trinpos < validTrains - 1; trinpos++) {
				String arr = finalData.getRows().get(trinpos).getCol3();
				if (arr.compareTo(stationTrains.get(i).getArriaval()) > 0) {
					finalData.getRows().add(trinpos, row);
					added = true;
					break;
				}
			}
			if (!added)
				finalData.getRows().add(row);
		}

		if (validTrains == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRAINS_FOR_STN);
			return finalData;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(screenData.getSource().getStationName()
				+ " Passing by trains\n");
		sb.append(Utils.convertDateToString(screenData.getDate()));
		sb.append("   " + Utils.convertDateToDayOfWeek(screenData.getDate()));

		finalData.setHeading(sb.toString());

		screenData.saveToLatestValidData();

		return finalData;

	}

	public FinalDataReturned getPassingTrainsForWeek(ScreenData screenData) {
		FinalDataReturned finalData = new FinalDataReturned();
		TdForSrcToDstForStnGrp trainBwSrcNDstGrp = new TdForSrcToDstForStnGrp();
		List<TrainRoute> stationTrains = getPassingThroughTrainRouteData(
				screenData.getSource(), screenData.isConsiderNearbyTrains(),
				trainBwSrcNDstGrp, true);

		finalData.setScrId(RailTimeConstants.SCR10);

		if (stationTrains == null || stationTrains.size() == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRAINS_FOR_STN);
			return finalData;
		}

		finalData.setRows(new ArrayList<RowOf7Strings>());

		for (int i = 0; i < stationTrains.size(); i++) {

			TrainDetails td = getTrainDetails(stationTrains.get(i).getTid());
			if (td == null)
				continue;

			RowOf7Strings row = new RowOf7Strings();

			row.setCol1(td.getTrainNo());
			row.setCol2(td.getTrainName());
			row.setCol3(stationTrains.get(i).getArriaval());
			row.setCol4(stationTrains.get(i).getHalt() + "m");
			row.setCol5(getNextStation(stationTrains.get(i).getRtid(),
					stationTrains.get(i).getTid()));
			row.setDays(new boolean[7]);

			for (int dayOfWeek = 0; dayOfWeek < 7; dayOfWeek++) {

				int daytoconsider = (dayOfWeek
						- stationTrains.get(i).getDatePlus() + 7) % 7;

				if (td.getScheduleFromSunday()[daytoconsider]) {
					row.getDays()[dayOfWeek] = true;
				} else {
					row.getDays()[dayOfWeek] = false;
				}
			}

			boolean added = false;
			for (int trinpos = 0; trinpos < i; trinpos++) {
				String arr = finalData.getRows().get(trinpos).getCol3();

				if (arr.compareTo(stationTrains.get(i).getArriaval()) > 0) {
					finalData.getRows().add(trinpos, row);
					added = true;
					break;
				}
			}
			if (!added)
				finalData.getRows().add(row);

		}

		StringBuilder sb = new StringBuilder();
		sb.append(screenData.getSource().getStationName()
				+ " Passing by trains");
		finalData.setHeading(sb.toString());

		screenData.saveToLatestValidData();
		return finalData;

	}

	private String getNextStation(int rtId, int tId) {
		String getStation = "SELECT b.stationName"
				+ " FROM route_table AS a"
				+ " INNER JOIN station_table AS b WHERE a._id=? AND a.trainId=? AND"
				+ " a.stationId=b._id LIMIT 1";

		String station = "";
		String[] stArr = { Integer.valueOf(rtId + 1).toString(),
				Integer.valueOf(tId).toString() };
		Cursor cur = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getStation, stArr);
		if (cur != null && !cur.isClosed()) {
			if (cur.getCount() <= 0) {
				cur.close();
				return "Last stop";
			}
			cur.moveToFirst();
			int colStName = cur.getColumnIndex("stationName");
			station = cur.getString(colStName);
			int maxLen = Math.min(station.length(), 10);
			station = station.substring(0, maxLen);
			cur.close();
		}
		return station;
	}

	public FinalDataReturned getTrainsByName(ScreenData screenData) {
		FinalDataReturned finalData = new FinalDataReturned();
		// MyLog.v("showMySCR12");
		List<SimpleTrainDetails> trainNameList = getTrainsMatchingName(screenData
				.getTrainName());
		finalData.setScrId(RailTimeConstants.SCR12);

		if (trainNameList == null || trainNameList.size() == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRAINS_WITH_NAME);
			return finalData;
		}

		finalData.setRows(new ArrayList<RowOf7Strings>());
		for (SimpleTrainDetails td : trainNameList) {

			RowOf7Strings row = new RowOf7Strings();
			row.setCol1(td.getTrainNo());
			row.setCol2(td.getTrainName());

			finalData.getRows().add(row);

		}
		return finalData;

	}
    public boolean isTrainByNumFound(String num) {
    boolean isValid = false;

        String getStaionDet = "SELECT trainName,trainNO FROM train_table WHERE trainNO LIKE ? LIMIT 200";
        String[] stArr = {num};
        Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
                .rawQuery(getStaionDet, stArr);

        if (cur1 != null && !cur1.isClosed()) {
            while (cur1.moveToNext()) {
                isValid = true;
            }
            cur1.close();
        }

        return isValid;

    }

	public FinalDataReturned getTrainsByNum(ScreenData screenData) {
		FinalDataReturned finalData = new FinalDataReturned();
		// MyLog.v("showMySCR12");
		List<SimpleTrainDetails> trainNameList = getTrainsMatchingNum(screenData
				.getTrainNo());
		finalData.setScrId(RailTimeConstants.SCR12);

		if (trainNameList == null || trainNameList.size() == 0) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NO_TRAINS_WITH_NUM);
			return finalData;
		}

		finalData.setRows(new ArrayList<RowOf7Strings>());
		for (SimpleTrainDetails td : trainNameList) {

			RowOf7Strings row = new RowOf7Strings();
			row.setCol1(td.getTrainNo());
			row.setCol2(td.getTrainName());

			finalData.getRows().add(row);

		}
		return finalData;

	}
	public List<SimpleTrainDetails> getTrainsMatchingName(String name) {

		List<SimpleTrainDetails> trains = new ArrayList<SimpleTrainDetails>();
		String getStaionDet = "SELECT trainName,trainNO FROM train_table WHERE trainName LIKE ? LIMIT 200";
		String[] stArr = { "%" + name + "%" };
		Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getStaionDet, stArr);

		if (cur1 != null && !cur1.isClosed()) {
			while (cur1.moveToNext()) {
				SimpleTrainDetails stD = new SimpleTrainDetails();
				stD.setTrainName(cur1.getString(0));
				stD.setTrainNo(cur1.getString(1));
				trains.add(stD);
			}
			cur1.close();
		}

		return trains;
	}

	public List<SimpleTrainDetails> getTrainsMatchingNum(String num) {

		List<SimpleTrainDetails> trains = new ArrayList<SimpleTrainDetails>();
		String getStaionDet = "SELECT trainName,trainNO FROM train_table WHERE trainNO LIKE ? LIMIT 200";
		String[] stArr = { num + "%" };
		Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getStaionDet, stArr);

		if (cur1 != null && !cur1.isClosed()) {
			while (cur1.moveToNext()) {
				SimpleTrainDetails stD = new SimpleTrainDetails();
				stD.setTrainName(cur1.getString(0));
				stD.setTrainNo(cur1.getString(1));
				trains.add(stD);
			}
			cur1.close();
		}

		return trains;
	}

	public List<String> getTrainNoMatchingName(String name) {

		List<String> trains = new ArrayList<String>();
		String getStaionDet = "SELECT trainNO,trainName FROM train_table WHERE trainName LIKE ? LIMIT 200";
		String[] stArr = { "%" + name + "%" };
		Cursor cur1 = DatabaseHandler.getInstance(sContext).getDatabase()
				.rawQuery(getStaionDet, stArr);

		if (cur1 != null && !cur1.isClosed()) {
			while (cur1.moveToNext()) {
				String trainNum = cur1.getString(0);
				trainNum = trainNum + "=" + cur1.getString(1);
				trains.add(trainNum);
			}
			cur1.close();
		}

		return trains;
	}

	public String getTrainRunningDetails(TrainDetWithRoute result) {
		TrainDetails trainDetails = result.getTd();

		StringBuilder sb = new StringBuilder();
		sb.append("Runs on ");
		int dayCount = 0;

		StringBuilder days = new StringBuilder();
		String append = "";
		for (int i = 0; i < 7; i++) {
			if (trainDetails.getScheduleFromSunday()[i]) {
				switch (i) {
				case 0:
					days.append("Sun");
					break;
				case 1:
					days.append(append + "Mon");
					break;
				case 2:
					days.append(append + "Tue");
					break;
				case 3:
					days.append(append + "Wed");
					break;
				case 4:
					days.append(append + "Thu");
					break;
				case 5:
					days.append(append + "Fri");
					break;
				case 6:
					days.append(append + "Sat");
					break;
				}
				append = ",";
				dayCount++;
			}
		}

		if (dayCount == 7) {
			sb.append("all days");
		} else {
			sb.append(days.toString() + " from starting station.");
		}

		// addAdmobAdd(R.id.linearLayoutscr3_2);
		return sb.toString();
	}

	public FinalDataReturned getTrainByNoForDay(ScreenData screenData) {
		// MyLog.v("showMySCR5");
		FinalDataReturned finalData = new FinalDataReturned();
		TrainDetWithRoute completeRoute = getCompleteRoute(
				screenData.getTrainNo(), screenData, false);
		finalData.setScrId(RailTimeConstants.SCR5);

		if (completeRoute == null) {
			finalData.setErrCode(RailTimeConstants.TRAIN_NOT_FOUND);
			return finalData;
		}

		if (completeRoute.getErrorCode() != 0) {
			finalData.setErrCode(completeRoute.getErrorCode());
			return finalData;
		}

		TrainDetails trainDetails = completeRoute.getTd();

		ArrayList<TrainRoute> trainRoute = completeRoute.getRt();
		finalData.setRows(new ArrayList<RowOf7Strings>());

		// from source to final destination
		for (int i = 0; i < trainRoute.size(); i++) {
			RowOf7Strings row = new RowOf7Strings();
			row.setCol1(trainRoute.get(i).getStationName());
			row.setCol5(trainRoute.get(i).getStationCode());
			row.setCol2(trainRoute.get(i).getArriaval());
			row.setCol3(trainRoute.get(i).getHalt() + "m");
			row.setCol4(trainRoute.get(i).getDist() + "");
			finalData.getRows().add(row);
		}

		StringBuilder sb = new StringBuilder();
		sb.append(trainDetails.getTrainName() + "\n");
		sb.append("# " + trainDetails.getTrainNo() + "\n");
		sb.append(getTrainRunningDetails(completeRoute)
				+ " from source station.");

		finalData.setHeading(sb.toString());
		screenData.saveToLatestValidData();
		return finalData;

	}

	public FinalDataReturned getTrainWithNumberAndStation(ScreenData screenData) {
		FinalDataReturned finalData = new FinalDataReturned();
		TrainDetWithRoute trainDetWithRoute = getCompleteRoute(
				screenData.getTrainNo(), screenData, false);
		TrainRoute source = null;

		finalData.setScrId(RailTimeConstants.SCR3);

		if (trainDetWithRoute == null) {
			finalData.setErrCode(RailTimeConstants.TRAIN_DOES_NOT_PASS_SRC);
			return finalData;
		}

		TrainDetails trainDetails = trainDetWithRoute.getTd();
		ArrayList<TrainRoute> trainRoute = trainDetWithRoute.getRt();

		if (trainDetWithRoute.getErrorCode() != 0) {
			finalData.setErrCode(trainDetWithRoute.getErrorCode());
			return finalData;
		}

		finalData.setRows(new ArrayList<RowOf7Strings>());

		// int screenDayofWeek = screenData.date.get(Calendar.DAY_OF_WEEK);
		// MyLog.v("screen day " + screenDayofWeek);

		if (trainDetWithRoute.getSrcRtPos() != -1) {
			source = trainRoute.get(trainDetWithRoute.getSrcRtPos());
		}

		if (source != null) {
			// from source to final destination
			Double st = 0.0;
			try {
				if (trainDetWithRoute.getSrcRtPos() < trainRoute.size())
					st = Double.parseDouble(trainRoute.get(
							trainDetWithRoute.getSrcRtPos()).getDist());
			} catch (NumberFormatException e) {
			}
			for (int i = trainDetWithRoute.getSrcRtPos(); i < trainRoute.size(); i++) {

				RowOf7Strings row = new RowOf7Strings();

				row.setCol1(trainRoute.get(i).getStationName());
				row.setCol5(trainRoute.get(i).getStationCode());
				row.setCol2(trainRoute.get(i).getArriaval());
				row.setCol3(trainRoute.get(i).getHalt() + "m");
				try {
					NumberFormat nf = new DecimalFormat("#.#");
					Double d = Double.parseDouble(trainRoute.get(i).getDist())
							- st;
					nf.format(d);
					row.setCol4(nf.format(d));
				} catch (NumberFormatException e) {
					row.setCol4("0.0");
				}
				finalData.getRows().add(row);
			}
		} else {

			for (int i = 0; i <= trainDetWithRoute.getDstRtPos(); i++) {
				RowOf7Strings row = new RowOf7Strings();
				row.setCol1(trainRoute.get(i).getStationName());
				row.setCol5(trainRoute.get(i).getStationCode());
				row.setCol2(trainRoute.get(i).getArriaval());
				row.setCol3(trainRoute.get(i).getHalt() + "m");
				row.setCol4(trainRoute.get(i).getDist());
				finalData.getRows().add(row);
			}
		}

		StringBuilder sb = new StringBuilder();
		sb.append(trainDetails.getTrainNo() + "," + trainDetails.getTrainName()
				+ "\n");
		sb.append(Utils.convertDateToString(screenData.getDate()));
		sb.append("   " + Utils.convertDateToDayOfWeek(screenData.getDate()));

		finalData.setHeading(sb.toString());

		screenData.saveToLatestValidData();
		return finalData;
	}

}