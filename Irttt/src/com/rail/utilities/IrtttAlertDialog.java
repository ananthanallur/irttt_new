package com.rail.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.network.DownloadManager;

public class IrtttAlertDialog {
	private static Context mContext;

	private static String mMessage;

	public View findViewById(int id) {
		return RuntimeData.getAlert().findViewById(id);
	}

	public void setView(View view) {
		RuntimeData.getAlert().setView(view);
	}

	public IrtttAlertDialog() {

	}

	public IrtttAlertDialog(Context context, String title, String message,
			String positiveButton, OnClickListener pListener,
			String negativeButton, OnClickListener nListener) {
		mContext = context;
		mMessage = message;

		RuntimeData.setBuilder(new AlertDialog.Builder(mContext));
		RuntimeData.getBuilder().setTitle(title);
		RuntimeData.getBuilder().setMessage(mMessage);
		RuntimeData.getBuilder().setPositiveButton(positiveButton, pListener);
		RuntimeData.getBuilder().setNegativeButton(negativeButton, nListener);
		RuntimeData.getBuilder().setCancelable(false);
		RuntimeData.setAlert(RuntimeData.getBuilder().create());

	}

	public void setMessage(String mMessage) {
		RuntimeData.getAlert().setMessage(mMessage);

	}

	public void setDialogTitle(String title) {
		RuntimeData.getAlert().setTitle(title);
	}

	public boolean isShowing() {
		return RuntimeData.getAlert().isShowing();
	}

	public void showDialog(int messageTextSize) {
		RuntimeData.getAlert().show();
		TextView msg = (TextView) RuntimeData.getAlert().findViewById(
				android.R.id.message);
		msg.setTextSize(TypedValue.COMPLEX_UNIT_SP, messageTextSize);
	}

	public void showDialog() {
		RuntimeData.getAlert().show();

	}

	public void dismissDialog() {
		RuntimeData.getAlert().dismiss();
	}

	public static void showAlert(Context context, int id) {
		mContext = context;
		if (Build.VERSION.SDK_INT > 11) {
			RuntimeData.setBuilder(new AlertDialog.Builder(context, 5));
		} else {
			RuntimeData.setBuilder(new AlertDialog.Builder(context));
		}
		switch (id) {
		case RailTimeConstants.DOWNLOAD_SUCCESS:
			RuntimeData.getBuilder().setMessage("Download successfull.");
			try {
				int currAppVersion = context
						.getApplicationContext()
						.getPackageManager()
						.getPackageInfo(
								context.getApplicationContext()
										.getPackageName(), 0).versionCode;
				SharedPreferences sf = context.getSharedPreferences(
						RailTimeConstants.PREFS_NAME, 0);
				sf.edit().putInt("previousAppVersion", currAppVersion).commit();

			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			RuntimeData.getBuilder().setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			break;
		case RailTimeConstants.DOWNLOAD_FAILED:
			RuntimeData.getBuilder().setMessage("Download failed. Try again?.");

			RuntimeData.getBuilder().setNegativeButton("No",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			RuntimeData.getBuilder().setPositiveButton(R.string.sentence_yes_text,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							
							DownloadManager manager = new DownloadManager();
							RuntimeData.setDwnldMgr(manager);
							manager.handleDownloadYes(mContext);
							dialog.dismiss();
						}
					});
			break;
		case RailTimeConstants.NO_NET_CONNECTION:
			RuntimeData.getBuilder().setMessage(
					"Ooops... No internet connection.");
			RuntimeData.getBuilder().setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			break;
		case RailTimeConstants.NOT_ENOUGH_STORAGE:
			RuntimeData
					.getBuilder()
					.setMessage(
							"Oops..Space not sufficient...You will need at least 8 MB of writable space in internal storage or SD card.");
			RuntimeData.getBuilder().setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.dismiss();
						}
					});
			break;
		}

		RuntimeData.getBuilder().setCancelable(false);

		RuntimeData.setAlert(RuntimeData.getBuilder().create());

		Activity parentAct = (Activity) mContext;
		if (RuntimeData.getAlert() != null && !parentAct.isFinishing()) {

			if ((Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR2)
					&& !parentAct.isDestroyed())
				RuntimeData.getAlert().show();
			else
				RuntimeData.getAlert().show();
		}
	}

}
