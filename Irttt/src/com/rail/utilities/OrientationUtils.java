package com.rail.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

public class OrientationUtils {
	private static Activity act;

	private OrientationUtils() {
	}

	/** Locks the device window in landscape mode. */
	public static void lockOrientationLandscape(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}

	/** Locks the device window in portrait mode. */
	public static void lockOrientationPortrait(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	public static void lockOrientation(Activity activity) {
		act = activity;
		Display display = ((WindowManager) activity
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int rotation = display.getRotation();
		int tempOrientation = activity.getResources().getConfiguration().orientation;
		int orientation = 0;
		switch (tempOrientation) {
		case Configuration.ORIENTATION_LANDSCAPE:
			if (rotation == Surface.ROTATION_0
					|| rotation == Surface.ROTATION_90)
				orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
			else
				orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
			break;
		case Configuration.ORIENTATION_PORTRAIT:
			if (rotation == Surface.ROTATION_0
					|| rotation == Surface.ROTATION_270)
				orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
			else
				orientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
		}
		activity.setRequestedOrientation(orientation);
	}

	/** Unlocks the device window in user defined screen mode. */
	public static void unlockOrientation(Activity activity) {
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
	}

	public static void unlockOrientation() {
		if (act != null)
			act.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER);
	}

}