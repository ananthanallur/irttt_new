package com.rail.utilities;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.AlertDialog;

import com.rail.network.DownloadManager;

import android.app.ProgressDialog;

public class RuntimeData {
	private static ProgressDialog progress_dialog;
	private static boolean isRunning = false;
	private static AdRequest request;
	private static AdView adView;
	private static AlertDialog.Builder builder = null;
	private static AlertDialog alert = null;
	private static boolean alertRemoved;
	private static boolean progressRemoved;
	private static boolean bgDwnldInProgress;
	private static DownloadManager dwnldMgr;

	public static DownloadManager getDwnldMgr() {
		return dwnldMgr;
	}

	public static void setDwnldMgr(DownloadManager dwnldMgr) {
		RuntimeData.dwnldMgr = dwnldMgr;
	}

	public static boolean isBgDwnldInProgress() {
		return bgDwnldInProgress;
	}

	public static void setBgDwnldInProgress(boolean bgDwnldInProgress) {
		RuntimeData.bgDwnldInProgress = bgDwnldInProgress;
	}

	public static boolean isProgressRemoved() {
		return progressRemoved;
	}

	public static void setProgressRemoved(boolean progressRemoved) {
		RuntimeData.progressRemoved = progressRemoved;
	}

	public static boolean isAlertRemoved() {
		return alertRemoved;
	}

	public static void setAlertRemoved(boolean alertRemoved) {
		RuntimeData.alertRemoved = alertRemoved;
	}

	public static AlertDialog.Builder getBuilder() {
		return builder;
	}

	public static void setBuilder(AlertDialog.Builder builder) {
		RuntimeData.builder = builder;
	}

	public static AlertDialog getAlert() {
		return alert;
	}

	public static void setAlert(AlertDialog alert) {
		RuntimeData.alert = alert;
	}

	public static ProgressDialog getProgress_dialog() {
		return progress_dialog;
	}

	public static AdRequest getRequest() {
		return request;
	}

	public static void setRequest(AdRequest request) {
		RuntimeData.request = request;
	}

	public static AdView getAdView() {
		return adView;
	}

	public static void setAdView(AdView adView) {
		RuntimeData.adView = adView;
	}

	public static void setProgress_dialog(ProgressDialog progress_dialog) {
		RuntimeData.progress_dialog = progress_dialog;
	}

	public static boolean isRunning() {
		return isRunning;
	}

	public static void setRunning(boolean isRunning) {
		RuntimeData.isRunning = isRunning;
	}
}
