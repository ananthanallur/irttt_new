package com.rail.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.Calendar;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.util.TypedValue;

import com.rail.controller.RailTimeConstants;

public class Utils {
	public static enum EXTERNAL_STORAGE_STATE {
		SD_AVAILABLE_WRITABLE, SD_AVAILABLE_NONWRITABLE, SD_NOT_AVAILABLE
	}

	public static int getColor(Context context,int resId){
		TypedValue typedValue = new TypedValue();
		Resources.Theme theme = context.getTheme();
		theme.resolveAttribute(resId, typedValue, true);
		return typedValue.data;
	}

    public static int getColorRes(Context context,int resId){
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(resId, typedValue, true);
        return typedValue.resourceId;
    }


	public static int getMyAppVerCode(Context context) {
		int mVersionNumber;
		String pkg = context.getPackageName();
		try {
			mVersionNumber = context.getPackageManager().getPackageInfo(pkg, 0).versionCode;
		} catch (NameNotFoundException e) {
			mVersionNumber = 0;
		}
		return mVersionNumber;
	}

	public static int getStorageOption(Context context) {
		File extF = Environment.getExternalStorageDirectory();
		File intF = context.getFilesDir();

		// 0 Store both files on SD. That means check if 8MB is available on SD.
		// 1 means at least 4 MB available on SD and 4MB available on main.
		// 2 means main has 8MB
		// 3 sufficient means space is not available
		EXTERNAL_STORAGE_STATE sdState = Utils.getExternalStorageState();
		float mbAvailExt = Utils.megabytesAvailable(extF);
		float mbAvailInt = Utils.megabytesAvailable(intF);

		if (sdState == EXTERNAL_STORAGE_STATE.SD_AVAILABLE_WRITABLE) {
			// Check if 8MB is available
			if (mbAvailExt > (RailTimeConstants.MIN_STORAGE_FOR_DB * 2))
				return RailTimeConstants.DOWNLOAD_EXT_KEEP_EXT;
			if (mbAvailExt > RailTimeConstants.MIN_STORAGE_FOR_DB
					&& mbAvailInt > RailTimeConstants.MIN_STORAGE_FOR_DB)
				return RailTimeConstants.DOWNLOAD_INT_KEEP_EXT;
		}
		if (mbAvailInt > (RailTimeConstants.MIN_STORAGE_FOR_DB * 2))
			return RailTimeConstants.DOWNLOAD_INT_KEEP_INT;
		return RailTimeConstants.DOWNLOAD_SPACE_CONSTRAINT;
	}

	public static String convertDateToDayOfWeek(Calendar date) {
		int d = date.get(Calendar.DAY_OF_WEEK) - 1;
		d = d % 7;
		return RailTimeConstants.WEEKDAYS[d];
	}

	public static String convertDateToString(Calendar date) {

		StringBuilder sb = new StringBuilder();
		sb.append(Integer.valueOf(date.get(Calendar.DAY_OF_MONTH)).toString());
		sb.append(" ");
		int month = date.get(Calendar.MONTH);
		sb.append(RailTimeConstants.MONTHS_SHORT[month]);
		return sb.toString();
	}

	public static float megabytesAvailable(File f) {
		if (f == null)
			return 0;
		StatFs stat = new StatFs(f.getPath());
		long bytesAvailable = (long) stat.getBlockSize()
				* (long) stat.getAvailableBlocks();
		return bytesAvailable / (1024.f * 1024.f);
	}

	public static EXTERNAL_STORAGE_STATE getExternalStorageState() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return EXTERNAL_STORAGE_STATE.SD_AVAILABLE_WRITABLE;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return EXTERNAL_STORAGE_STATE.SD_AVAILABLE_NONWRITABLE;
		} else {
			return EXTERNAL_STORAGE_STATE.SD_NOT_AVAILABLE;
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		// if no network is available networkInfo will be null
		// otherwise check if we are connected
		if (networkInfo != null && networkInfo.isConnected()) {
			System.out.println("Debug: Network available =yes" );
			return true;
		}
		System.out.println("Debug: Network available =no" );
		return false;
	}

	public static boolean copyFile(File sourceFolder, String srcFilename,
			String destFolder, String destFilename) {
		// InputStream in;
		FileOutputStream f;
		FileInputStream s;
		try {

			s = new FileInputStream(new File(sourceFolder, srcFilename));
			f = new FileOutputStream(new File(destFolder, destFilename));
			// in = c.getInputStream();
			// MyLog.v("Source: " + sourceFolder.getAbsolutePath() +
			// srcFilename);
			// MyLog.v("Dest: " + destFolder + destFilename);
			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = s.read(buffer)) >= 0) {
				f.write(buffer, 0, len1);
			}
			s.close();
			f.close();
			return true;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static String getAppVersion(Context context) {
		try {
			return context
					.getApplicationContext()
					.getPackageManager()
					.getPackageInfo(
							context.getApplicationContext().getPackageName(),
							0).versionCode
					+ "";
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "0";
	}
}
