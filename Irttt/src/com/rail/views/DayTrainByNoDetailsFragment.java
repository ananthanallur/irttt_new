package com.rail.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.FinalDataReturned;
import com.rail.model.RowOf7Strings;
import com.rail.model.ScreenData;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class DayTrainByNoDetailsFragment extends Fragment {
	private boolean isScrolled;
	private FrontController mFrontController;
	private ListView mListView;
	private TextView mLineText1;
	private TextView mLineText2;
	private TextView mLineText3;
	private FinalDataReturned resultData;
	private int temp;
	private List<Integer> selectedTrains = new ArrayList<Integer>();

	private StringBuffer textBuffer = new StringBuffer();
	private StringBuffer htmlBuffer = new StringBuffer();
	private Bitmap myBitmap;
	private LayoutInflater inflater;
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		if (resultData != null && resultData.getRows() != null
				&& resultData.getRows().size() > 0) {
			getActivity().getMenuInflater().inflate(R.menu.all_share, menu);
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					textBuffer = new StringBuffer();
					htmlBuffer = new StringBuffer();
					ScreenData data = (ScreenData) getArguments()
							.getSerializable("data");
					
					textBuffer.append("** Trains details for #"+data.getTrainNo()+" "+trainName);
					htmlBuffer.append("<html> Trains details for <b>#"
							+data.getTrainNo()+" "+trainName + "</b><br/><br/>");
					htmlBuffer.append("Station Name&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Arr&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Halt&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Dist&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Code&nbsp;&nbsp;&nbsp;<br/>");

					for (RowOf7Strings trainDetails : resultData.getRows()) {
						textBuffer.append("\n**");
						textBuffer.append("Station Name->"
								+ trainDetails.getCol1().trim());
						textBuffer.append(";Arr->"
								+ trainDetails.getCol2().trim());
						textBuffer.append(";Halt->"
								+ trainDetails.getCol3().trim());
						textBuffer.append(";Dist->"
								+ trainDetails.getCol4().trim()+"km");
						textBuffer.append(";Code->"
								+ trainDetails.getCol5().trim() + ";");

						htmlBuffer.append(trainDetails.getCol1().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol2().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol3().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol4().trim()+"km");
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol5().trim() + "<br/>");
					}
					textBuffer.append("**");
					textBuffer.append("\n");
					textBuffer
							.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
					htmlBuffer
							.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");
				}
			});
		}
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.item3:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, textBuffer.toString());
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Train details from IRTTT");
			sendIntent.setType("text/plain");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;
		case R.id.item4:
			sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					Html.fromHtml(htmlBuffer.toString()));
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Train details from IRTTT");
			sendIntent.setType("text/html");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;

		case R.id.item5:
			View v1 = getActivity().getWindow().getDecorView().getRootView();
			v1.setDrawingCacheEnabled(true);
			myBitmap = v1.getDrawingCache();
			saveBitmap(myBitmap);
			v1.destroyDrawingCache();

			v1.setDrawingCacheEnabled(false);
			break;

		default:
			break;
		}

		return true;
	}
	
	public void saveBitmap(Bitmap bitmap) {
		File root = Environment.getExternalStorageDirectory();
		if (root != null) {
			File extFolder = new File(root, getActivity().getPackageName());
			File picFolder = new File(extFolder, "pictures");
			if(!picFolder.exists()){
				picFolder.mkdir();
			}
			for (File file : picFolder.listFiles()) {
				file.delete();
			}
			File imagePath = new File(picFolder,System.currentTimeMillis()+"_screenshot.png");
	
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			sendMail(imagePath.getAbsolutePath());
		} catch (FileNotFoundException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}

	public void sendMail(String path) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"Train details from IRTTT");
		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		emailIntent.putExtra(
				android.content.Intent.EXTRA_TEXT,
				Html.fromHtml("<html> Trains details for <b>#"
						+data.getTrainNo()+" "+trainName + "</b><br/><br/></html>"));
		
		emailIntent.setType("image/png");
		Uri myUri = Uri.parse("file://" + path);
			
		emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		} catch (Exception e) {
			// ignore if not activity to handle
		}
	}
	
	public void toggleSelected(Integer position) {
		if (selectedTrains.contains(position)) {
			selectedTrains.remove(position);
		} else {
			selectedTrains.add(position);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		Toast.makeText(getActivity(),
				resultData.getRows().get(info.position).getCol1(),
				Toast.LENGTH_SHORT).show();
		return super.onContextItemSelected(item);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFrontController = FrontController.getInstance(getActivity());
		if (resultData == null) {
			RuntimeData.setProgress_dialog(new ProgressDialog(getActivity()));
			RuntimeData.getProgress_dialog().setMessage("Please wait...");
			RuntimeData.getProgress_dialog().setCancelable(false);
			RuntimeData.getProgress_dialog().show();
			new FetchTrainsTask().execute();
		} else {
			processResult(resultData);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		isScrolled = false;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		View rootView = inflater.inflate(R.layout.fgmt_trainslist, container,
				false);
		initUI(rootView);
		setHasOptionsMenu(true);
		temp++;
		setRetainInstance(true);
		return rootView;
	}

	private void initUI(View rootView) {
		mListView = (ListView) rootView.findViewById(R.id.listView1);
		mListView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				ScreenData data = (ScreenData) getArguments().getSerializable("data");
				if(view.getFirstVisiblePosition()==0){
					((UserActivity)getActivity()).setTitle("TRAIN BY NAME / NUMBER");
					((UserActivity)getActivity()).setCompoundTitle(null);
				}else{
					((UserActivity)getActivity()).setTitle(Html.fromHtml("<html><small><b>"+"#" + data.getTrainNo()+"<br/>"
							+ trainName+"</b>"+"</small></html>"));
					((UserActivity)getActivity()).setCompoundTitle(getResources().getDrawable(R.drawable.a12));
				}

			}
		});
		
		
		
		
		
		View view2 = inflater.inflate(R.layout.lyt_headerpane,null); 
		View view3 = inflater.inflate(R.layout.lyt_train_list_item_type_4,null);
		view3.setBackgroundColor(getActivity().getResources().getColor(R.color.brown));

        mLineText1 = (TextView) view2
                .findViewById(R.id.textView1STsrcheading);
        mLineText2 = (TextView) view2.findViewById(R.id.textView1);
        mLineText3 = (TextView) view2.findViewById(R.id.textView2);

		((TextView) view3.findViewById(R.id.textView3))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView4))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView5))
				.setTextColor(getActivity().getResources().getColor(
                        android.R.color.white));
		((TextView) view3.findViewById(R.id.textView6))
				.setTextColor(getActivity().getResources().getColor(
                        android.R.color.white));
		((TextView) view3.findViewById(R.id.textView7))
				.setTextColor(getActivity().getResources().getColor(
                        android.R.color.white));
        Typeface custom_font = Typeface
                .createFromAsset(getActivity().getAssets(), "nato_sans_bold.ttf");
        mLineText1.setTypeface(custom_font,
                Typeface.BOLD);
        mLineText2.setTypeface(custom_font,
                Typeface.BOLD);
        mLineText3.setTypeface(custom_font,
                Typeface.BOLD);
        ((TextView) view3.findViewById(R.id.textView3))
                .setTypeface(custom_font,
                        Typeface.NORMAL);
        ((TextView) view3.findViewById(R.id.textView4))
                .setTypeface(custom_font,
                        Typeface.NORMAL);
        ((TextView) view3.findViewById(R.id.textView5))
                .setTypeface(custom_font,
                        Typeface.NORMAL);
        ((TextView) view3.findViewById(R.id.textView6))
                .setTypeface(custom_font,
                        Typeface.NORMAL);
        ((TextView) view3.findViewById(R.id.textView7))
                .setTypeface(custom_font,
                        Typeface.NORMAL);
		LinearLayout layout = new LinearLayout(getActivity());
		layout.setOrientation(LinearLayout.VERTICAL);

		layout.addView(view2);
		layout.addView(view3);
		mListView.addHeaderView(layout,null,false);


		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		mFrontController.showAdv(mListView);
		RuntimeData.getAdView().setAdListener(new AdListener() {


            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
				/*if(!isScrolled){
				mListView.scrollTo(0, 0);
				isScrolled=true;
				}*/
            }
        });
		
		mLineText1.setVisibility(View.GONE);
		mLineText2.setText("#" + data.getTrainNo()+"  "+data.getTrainName());
		mLineText3.setVisibility(View.GONE);
	
		((UserActivity) getActivity()).setTitle(getArguments().getString(
				"title"));
		

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (resultData != null && temp > 1) {
			processResult(resultData);
		}
	}

	private void processResult(FinalDataReturned result) {
		if (result.getErrCode() == RailTimeConstants.TRAIN_DOES_NOT_PASS_SRC) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("Train doesnt pass through this station.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getFragmentManager().popBackStackImmediate();
						}
					});
			builder.create().show();
		} else if (result.getErrCode() == RailTimeConstants.TRAIN_NOT_FOUND) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("There are no trains matching this number.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getFragmentManager().popBackStackImmediate();
						}
					});
			builder.create().show();
		} else {
			mListView.setAdapter(new TrainDetailAdapter(result.getRows(),
					getActivity()));
			String heading[] = result.getHeading().split("\n");
			trainName = heading[0];
			mLineText2.setText(heading[1] + "  " + heading[0] + "\n" + heading[2]);
			getActivity().supportInvalidateOptionsMenu();

		}
	}
	
	private String trainName = "";

	class FetchTrainsTask extends AsyncTask<Void, Void, FinalDataReturned> {

		@Override
		protected FinalDataReturned doInBackground(Void... params) {
			return mFrontController
					.getTrainByNoForDay((ScreenData) getArguments()
							.getSerializable("data"));
		}

		@Override
		protected void onPostExecute(FinalDataReturned result) {
			super.onPostExecute(result);
			resultData = result;
			processResult(result);
			if (RuntimeData.getProgress_dialog() != null) {
				RuntimeData.getProgress_dialog().dismiss();
				RuntimeData.setProgress_dialog(null);
			}
		}

	}

	class TrainDetailAdapter extends BaseAdapter {
		private List<RowOf7Strings> rows = new ArrayList<RowOf7Strings>();
		private Context mContext;

		public TrainDetailAdapter(List<RowOf7Strings> rows, Context context) {
			this.rows = rows;
			this.mContext = context;
		}

		@Override
		public int getCount() {
			return rows.size();
		}

		@Override
		public Object getItem(int position) {
			return rows.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.lyt_train_list_item_type_4, null, false);
			}
			TextView stationName = (TextView) convertView
					.findViewById(R.id.textView3);
			TextView trarr = (TextView) convertView
					.findViewById(R.id.textView4);
			TextView halt = (TextView) convertView.findViewById(R.id.textView5);
			TextView dist = (TextView) convertView.findViewById(R.id.textView6);
			TextView code = (TextView) convertView.findViewById(R.id.textView7);
			RowOf7Strings trainDetails = rows.get(position);
			stationName.setText(trainDetails.getCol1().trim());
			trarr.setText(trainDetails.getCol2().trim());
			halt.setText(trainDetails.getCol3() + "m");
			dist.setText(trainDetails.getCol4().trim());
			code.setText(trainDetails.getCol5().trim());
			return convertView;
		}

	}
}
