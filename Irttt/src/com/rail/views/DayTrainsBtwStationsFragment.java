package com.rail.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.FinalDataReturned;
import com.rail.model.RowOf7Strings;
import com.rail.model.ScreenData;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DayTrainsBtwStationsFragment extends Fragment implements
		OnCreateContextMenuListener {
	private FrontController mFrontController;
	private ListView mListView;
	private TextView mLineText1;
	private TextView mLineText2;
	private TextView mLineText3;
	private FinalDataReturned resultData;
	private LayoutInflater inflater;
	private int temp = 0;
	private View rootView;
	private List<Integer> selectedTrains = new ArrayList<Integer>();
	private boolean isScrolled;
	private StringBuffer textBuffer = new StringBuffer();
	private StringBuffer htmlBuffer = new StringBuffer();
	private Bitmap myBitmap;

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		if (resultData != null && resultData.getRows() != null
				&& resultData.getRows().size() > 0) {
			getActivity().getMenuInflater().inflate(R.menu.all_share, menu);
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					textBuffer = new StringBuffer();
					htmlBuffer = new StringBuffer();
					ScreenData data = (ScreenData) getArguments()
							.getSerializable("data");
					Calendar date = data.getDate();
					String month[] = { "Jan", "Feb", "Mar", "Apr", "May",
							"Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
					textBuffer.append("** Day trains btw "
							+ data.getSource().getStationName().toUpperCase(Locale.US)
							+ " and "
							+ data.getDest().getStationName().toUpperCase(Locale.US)
							+ " on " + date.get(Calendar.DAY_OF_MONTH) + " "
							+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US)
							+ " " + date.get(Calendar.YEAR));
					htmlBuffer.append("<html> Day trains btw <b>"
							+ data.getSource().getStationName().toUpperCase(Locale.US)
							+ " </b>and <b>"
							+ data.getDest().getStationName().toUpperCase(Locale.US)
							+ " </b>on <b>" + date.get(Calendar.DAY_OF_MONTH)
							+ " "
							+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US)
							+ " " + date.get(Calendar.YEAR) + "</b><br/><br/>");
					htmlBuffer.append("TrNum&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("TrName&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Dep&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Arr&nbsp;&nbsp;&nbsp;<br/>");

					for (RowOf7Strings trainDetails : resultData.getRows()) {
						textBuffer.append("\n**");
						textBuffer.append("TrNum->"
								+ trainDetails.getCol2().trim());
						textBuffer.append(";TrName->"
								+ trainDetails.getCol1().trim());
						textBuffer.append(";Dep->"
								+ trainDetails.getCol3().trim());
						textBuffer.append(";Arr->"
								+ trainDetails.getCol4().trim() + ";");

						htmlBuffer.append(trainDetails.getCol2().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol1().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol3().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getCol4().trim() + "<br/>");
					}
					textBuffer.append("**");
					textBuffer.append("\n");
					textBuffer
							.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
					htmlBuffer
							.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");
				}
			});
		}
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.item3:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, textBuffer.toString());
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Day train details from IRTTT");
			sendIntent.setType("text/plain");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;
		case R.id.item4:
			sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					Html.fromHtml(htmlBuffer.toString()));
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Day train details from IRTTT");
			sendIntent.setType("text/html");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
				}

			break;

		case R.id.item5:
			View v1 = getActivity().getWindow().getDecorView().getRootView();
			v1.setDrawingCacheEnabled(true);
			myBitmap = v1.getDrawingCache();
			saveBitmap(myBitmap);
			v1.destroyDrawingCache();

			v1.setDrawingCacheEnabled(false);
			break;

		default:
			break;
		}

		return true;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		isScrolled = false;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		((ActionBarActivity) getActivity()).getSupportActionBar().show();
		rootView = inflater.inflate(R.layout.fgmt_trainslist, container,
				false);
		setHasOptionsMenu(true);
		temp++;
		this.inflater = inflater;
		initUI();
		setRetainInstance(true);
		return rootView;
	}

	public void toggleSelected(Integer position) {
		if (selectedTrains.contains(position)) {
			selectedTrains.remove(position);
		} else {
			selectedTrains.add(position);
		}
	}

	public void saveBitmap(Bitmap bitmap) {
		File root = Environment.getExternalStorageDirectory();
		if (root != null) {
			File extFolder = new File(root, getActivity().getPackageName());
			File picFolder = new File(extFolder, "pictures");
			if(!picFolder.exists()){
				picFolder.mkdir();
			}
			for (File file : picFolder.listFiles()) {
				file.delete();
			}
			File imagePath = new File(picFolder,System.currentTimeMillis()+"_screenshot.png");
	
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			sendMail(imagePath.getAbsolutePath());
		} catch (FileNotFoundException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}

	public void sendMail(String path) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"Day train details from IRTTT");
		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		Calendar date = data.getDate();
		String month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" };
		emailIntent.putExtra(
				android.content.Intent.EXTRA_TEXT,
				Html.fromHtml("<html> Day trains btw <b>"
						+ data.getSource().getStationName().toUpperCase(Locale.US)
						+ " </b>and <b>"
						+ data.getDest().getStationName().toUpperCase(Locale.US)
						+ " </b>on <b>" + date.get(Calendar.DAY_OF_MONTH) + " "
						+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US) + " "
						+ date.get(Calendar.YEAR) + "</b><br/><br/></html>"));
		emailIntent.setType("image/png");
		Uri myUri = Uri.parse("file://" + path);
		emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		} catch (Exception e) {
			// ignore if not activity to handle
		}
	}

	private void initUI() {

		mListView = (ListView) rootView.findViewById(R.id.listView1);
		View view = inflater.inflate(R.layout.lyt_headerpane, null);
        view.setEnabled(false);
		mListView.addHeaderView(view,null,false);
		mListView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				ScreenData data = (ScreenData) getArguments().getSerializable("data");
				if(view.getFirstVisiblePosition()==0){
					((UserActivity)getActivity()).setTitle("TRAINS BETWEEN STATIONS");
					((UserActivity)getActivity()).setCompoundTitle(null);
				}else{
					Calendar date = data.getDate();
					String month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
							"Aug", "Sep", "Oct", "Nov", "Dec" };
					((UserActivity)getActivity()).setTitle(Html.fromHtml("<html><small><b>"+data.getSource().getStationName().toUpperCase(Locale.US)
							+ " - " + data.getDest().getStationName().toUpperCase(Locale.US)+"</b><br/>"+date.get(Calendar.DAY_OF_MONTH) + " "
									+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US) + " "
									+ date.get(Calendar.YEAR)+"</small></html>"));
					((UserActivity)getActivity()).setCompoundTitle(getResources().getDrawable(R.drawable.a10));
				}
				
			}
		});
		mLineText1 = (TextView) view
				.findViewById(R.id.textView1STsrcheading);
		mLineText2 = (TextView) view.findViewById(R.id.textView1);
		mLineText3 = (TextView) view.findViewById(R.id.textView2);
		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		mLineText1.setText(data.getSource().getStationName().toUpperCase(Locale.US));
		Calendar date = data.getDate();
		String month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" };
		mLineText2.setText(date.get(Calendar.DAY_OF_MONTH) + " "
				+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US) + " "
				+ date.get(Calendar.YEAR));
		mLineText3.setText(
				data.getDest().getStationName().toUpperCase(Locale.US));
		Typeface custom_font = Typeface
				.createFromAsset(getActivity().getAssets(), "nato_sans_bold.ttf");
		mLineText1.setTypeface(custom_font,
				Typeface.BOLD);
        mLineText2.setTypeface(custom_font,
                Typeface.BOLD);
		mLineText3.setTypeface(custom_font,
				Typeface.BOLD);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			mListView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

				@Override
				public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
					return false;
				}

				@Override
				public void onDestroyActionMode(ActionMode mode) {

				}

				@Override
				public boolean onCreateActionMode(ActionMode mode, Menu menu) {
					MenuInflater inflater = getActivity().getMenuInflater();
					inflater.inflate(R.menu.share, menu);
					final int checkedCount = mListView.getCheckedItemCount();

					switch (checkedCount) {
					case 0:
						mode.setTitle(null);
						break;
					case 1:
						mode.setTitle("1 train selected");
						break;
					default:
						mode.setTitle(checkedCount + " trains selected");
						break;
					}
					return true;
				}

				@Override
				public boolean onActionItemClicked(final ActionMode mode,
						MenuItem item) {
					String month[] = null;
					switch (item.getItemId()) {
					case R.id.item8:
						textBuffer = new StringBuffer();
						ScreenData data = (ScreenData) getArguments()
								.getSerializable("data");
						Calendar date = data.getDate();
						month = new String[] { "Jan", "Feb", "Mar", "Apr",
								"May", "Jun", "Jul", "Aug", "Sep", "Oct",
								"Nov", "Dec" };
						textBuffer.append("** Day trains btw "
								+ data.getSource().getStationName()
										.toUpperCase(Locale.US) + " and "
								+ data.getDest().getStationName().toUpperCase(Locale.US)
								+ " on " + date.get(Calendar.DAY_OF_MONTH)
								+ " "
								+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US)
								+ " " + date.get(Calendar.YEAR));

						for (Integer integer : selectedTrains) {
							RowOf7Strings trainDetails = (RowOf7Strings) mListView
									.getItemAtPosition(integer);
							textBuffer.append("\n**");
							textBuffer.append("TrNum->"
									+ trainDetails.getCol2().trim());
							textBuffer.append(";TrName->"
									+ trainDetails.getCol1().trim());
							textBuffer.append(";Dep->"
									+ trainDetails.getCol3().trim());
							textBuffer.append(";Arr->"
									+ trainDetails.getCol4().trim() + ";");

						}
						textBuffer.append("**");
						textBuffer.append("\n");
						textBuffer
								.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
						Intent sendIntent = new Intent();
						sendIntent.setAction(Intent.ACTION_SEND);
						sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
								"Day train details from IRTTT");
						sendIntent.putExtra(Intent.EXTRA_TEXT,
								textBuffer.toString());
						sendIntent.setType("text/plain");
						try {
							startActivity(sendIntent);
						} catch (Exception e) {
							// ignore if not activity to handle
						}
						new Handler().post(new Runnable() {

							@Override
							public void run() {
								selectedTrains.clear();
								mode.finish();
							}
						});

						break;
					case R.id.item7:
						htmlBuffer = new StringBuffer();
						data = (ScreenData) getArguments().getSerializable(
								"data");
						date = data.getDate();
						month = new String[] { "Jan", "Feb", "Mar", "Apr",
								"May", "Jun", "Jul", "Aug", "Sep", "Oct",
								"Nov", "Dec" };

						htmlBuffer.append("<html> Day trains btw <b>"
								+ data.getSource().getStationName()
										.toUpperCase(Locale.US) + " </b>and <b>"
								+ data.getDest().getStationName().toUpperCase(Locale.US)
								+ " </b>on <b>"
								+ date.get(Calendar.DAY_OF_MONTH) + " "
								+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US)
								+ " " + date.get(Calendar.YEAR)
								+ "</b><br/><br/>");
						htmlBuffer.append("TrNum&nbsp;&nbsp;&nbsp;");
						htmlBuffer.append("TrName&nbsp;&nbsp;&nbsp;");
						htmlBuffer.append("Dep&nbsp;&nbsp;&nbsp;");
						htmlBuffer.append("Arr&nbsp;&nbsp;&nbsp;<br/>");

						for (Integer integer : selectedTrains) {
							RowOf7Strings trainDetails = (RowOf7Strings) mListView
									.getItemAtPosition(integer);

							htmlBuffer.append(trainDetails.getCol2().trim());
							htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
									+ trainDetails.getCol1().trim());
							htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
									+ trainDetails.getCol3().trim());
							htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
									+ trainDetails.getCol4().trim() + "<br/>");
						}
						htmlBuffer
								.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");

						sendIntent = new Intent();
						sendIntent.setAction(Intent.ACTION_SEND);
						sendIntent.putExtra(Intent.EXTRA_TEXT,
								Html.fromHtml(htmlBuffer.toString()));
						sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
								"Day train details from IRTTT");
						sendIntent.setType("text/html");
						try {
							startActivity(sendIntent);
						} catch (Exception e) {
							// ignore if not activity to handle
						}

						new Handler().post(new Runnable() {

							@Override
							public void run() {
								selectedTrains.clear();
								mode.finish();
							}
						});

						break;
					}
					return true;
				}

				@Override
				public void onItemCheckedStateChanged(ActionMode mode,
						int position, long id, boolean checked) {
					toggleSelected(position);
					final int checkedCount = mListView.getCheckedItemCount();

					switch (checkedCount) {
					case 0:
						mode.setTitle(null);
						break;
					case 1:
						mode.setTitle("1 train selected");
						break;
					default:
						mode.setTitle(checkedCount + " trains selected");
						break;
					}
				}
			});

		}

		((UserActivity) getActivity()).setTitle(getArguments().getString(
				"title"));
		mFrontController.showAdv(mListView);
		RuntimeData.getAdView().setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				/*if(isScrolled){
					mListView.scrollTo(0, 0);
					isScrolled = true;
				}*/
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		RuntimeData.setProgress_dialog(new ProgressDialog(getActivity()));
		mFrontController = FrontController.getInstance(getActivity());
		if (resultData == null) {
			RuntimeData.getProgress_dialog().setMessage("Please wait...");
			RuntimeData.getProgress_dialog().setCancelable(false);
			RuntimeData.getProgress_dialog().show();
			new FetchTrainsTask().execute();
		} else {
			processResult(resultData);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (resultData != null && temp > 1) {
			processResult(resultData);
		}
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			registerForContextMenu(mListView);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Share"); 
        menu.add(0, R.id.item7, 0, "As Html"); 
        menu.add(0, R.id.item8, 0, "As Plain Text"); 
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		
		String[] month;
		switch (item.getItemId()) {

		case R.id.item8:
			textBuffer = new StringBuffer();
			ScreenData data = (ScreenData) getArguments().getSerializable(
					"data");
			Calendar date = data.getDate();
			month = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
					"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
			textBuffer.append("** Day trains btw "
					+ data.getSource().getStationName().toUpperCase(Locale.US) + " and "
					+ data.getDest().getStationName().toUpperCase(Locale.US) + " on "
					+ date.get(Calendar.DAY_OF_MONTH) + " "
					+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US) + " "
					+ date.get(Calendar.YEAR));

			RowOf7Strings trainDetails = resultData.getRows()
					.get(info.position);
			textBuffer.append("\n**");
			textBuffer.append("TrNum->" + trainDetails.getCol2().trim());
			textBuffer.append(";TrName->" + trainDetails.getCol1().trim());
			textBuffer.append(";Dep->" + trainDetails.getCol3().trim());
			textBuffer.append(";Arr->" + trainDetails.getCol4().trim() + ";");

			textBuffer.append("**");
			textBuffer.append("\n");
			textBuffer
					.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, textBuffer.toString());
			sendIntent.setType("text/plain");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;
		case R.id.item7:
			htmlBuffer = new StringBuffer();
			data = (ScreenData) getArguments().getSerializable("data");
			date = data.getDate();
			month = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
					"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

			htmlBuffer.append("<html> Day trains btw <b>"
					+ data.getSource().getStationName().toUpperCase(Locale.US)
					+ " </b>and <b>"
					+ data.getDest().getStationName().toUpperCase(Locale.US)
					+ " </b>on <b>" + date.get(Calendar.DAY_OF_MONTH) + " "
					+ month[date.get(Calendar.MONTH)].toUpperCase(Locale.US) + " "
					+ date.get(Calendar.YEAR) + "</b><br/><br/>");
			htmlBuffer.append("&nbsp;&nbsp;&nbsp;");
			htmlBuffer.append("TrNum&nbsp;&nbsp;&nbsp;");
			htmlBuffer.append("TrName&nbsp;&nbsp;&nbsp;");
			htmlBuffer.append("Dep&nbsp;&nbsp;&nbsp;");
			htmlBuffer.append("Arr&nbsp;&nbsp;&nbsp;<br/>");

			trainDetails = resultData.getRows().get(info.position);

			htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
					+ trainDetails.getCol2().trim());
			htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
					+ trainDetails.getCol1().trim());
			htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
					+ trainDetails.getCol3().trim());
			htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
					+ trainDetails.getCol4().trim() + "<br/>");

			htmlBuffer
					.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");

			sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					Html.fromHtml(htmlBuffer.toString()));
			sendIntent.setType("text/html");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}
			break;
		}

		return super.onContextItemSelected(item);

	}

	@Override
	public void onStop() {
		super.onStop();
		RuntimeData.setProgress_dialog(null);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.getProgress_dialog().isShowing()) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgressRemoved(true);
		}
	}

	class FetchTrainsTask extends AsyncTask<Void, Void, FinalDataReturned> {

		@Override
		protected FinalDataReturned doInBackground(Void... params) {
			return mFrontController
					.getTrainsMatchingForDay((ScreenData) getArguments()
							.getSerializable("data"));
		}

		@Override
		protected void onPostExecute(FinalDataReturned result) {
			super.onPostExecute(result);
			resultData = result;
			processResult(result);
			if (RuntimeData.getProgress_dialog() != null) {
				RuntimeData.getProgress_dialog().dismiss();
				RuntimeData.setProgress_dialog(null);
			}

		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		resultData = null;
	}

	private void processResult(FinalDataReturned result) {
		getActivity().supportInvalidateOptionsMenu();
		if (result.getErrCode() == RailTimeConstants.TRAIN_SERVICE_NO_FOR_DAY) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("There are no trains running between these stations on this day.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getFragmentManager().popBackStackImmediate();
						}
					});
			builder.create().show();
		} else if (result.getErrCode() == RailTimeConstants.TRAIN_NO_TRANS_BW_SRC_N_DST) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("There are no trains running between these stations.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							getFragmentManager().popBackStackImmediate();
						}
					});
			builder.create().show();
		} else {
			mListView.setAdapter(new TrainAdapter(result.getRows(),
					getActivity()));
			mListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					ScreenData data = new ScreenData(getActivity());
					data.setTrainNo(view.getTag().toString().split("#")[0]);
					data.setTrainName(view.getTag().toString().split("#")[1]);
					TrainDetailsFragment trainDetails = new TrainDetailsFragment();
					Bundle bundle = new Bundle();
					bundle.putSerializable("data", data);
					bundle.putBoolean("fetchData", false);
					bundle.putString("title", getArguments().getString("title"));
					trainDetails.setArguments(bundle);

					ft.replace(R.id.fragment_container, trainDetails)
							.addToBackStack(null);
					ft.commit();
				}
			});
		}

	}

	class TrainAdapter extends BaseAdapter {
		private List<RowOf7Strings> rows = new ArrayList<RowOf7Strings>();

		// private Context mContext;

		public TrainAdapter(List<RowOf7Strings> rows2, Context context) {
			this.rows = rows2;
			// this.mContext = context;
		}

		@Override
		public int getCount() {
			return rows.size();
		}

		@Override
		public Object getItem(int position) {
			return rows.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = inflater.inflate(
						R.layout.lyt_train_list_item_type_1, null, false);
			}
			TextView trno = (TextView) convertView.findViewById(R.id.textView3);
			TextView trname = (TextView) convertView
					.findViewById(R.id.textView4);
			TextView trdep = (TextView) convertView
					.findViewById(R.id.textView5);
			TextView trarr = (TextView) convertView
					.findViewById(R.id.textView6);
			RowOf7Strings trainDetails = rows.get(position);
			trno.setText(trainDetails.getCol2().trim());
			trname.setText(trainDetails.getCol1().trim());
			trdep.setText(trainDetails.getCol3().trim());
			trarr.setText(trainDetails.getCol4().trim());

			convertView.setTag(trainDetails.getCol2().trim() + "#"
					+ trainDetails.getCol1().trim());
			return convertView;
		}

	}

}
