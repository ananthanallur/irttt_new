package com.rail.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.rail.lite.R;

public class HelpFragment extends Fragment {
	private WebView mWebView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fgmt_help, container,
				false);
		initUI(rootView);
		((UserActivity)getActivity()).setTitle(R.string.help_text);
		setRetainInstance(true);
		return rootView;
	}
	

	final class RailWebViewClient extends WebViewClient {
		Context ctx;

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return false;
		}

	}

	
	private void initUI(View rootView) {
		
		mWebView = (WebView) rootView.findViewById(R.id.help);
		RailWebViewClient wVClient = new RailWebViewClient();
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.setWebViewClient(wVClient);
		mWebView.getSettings().setAppCacheMaxSize(1024 * 100);
		mWebView.getSettings().setAppCachePath(
				getActivity().getCacheDir().getPath());
		mWebView.getSettings().setAppCacheEnabled(true);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		mWebView.loadUrl("file:///android_asset/expandcoll_v1.html");
	}
}
