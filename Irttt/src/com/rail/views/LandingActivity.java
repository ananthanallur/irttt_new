package com.rail.views;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.persistance.DatabaseHandler;
import com.rail.utilities.IrtttAlertDialog;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class LandingActivity extends Activity implements OnClickListener {
	private FrontController mFrontController;
	private boolean isScrolled;
	private Intent mSourceIntent;
	private TextView mTimeTxt;
	private TextView mDateTxt;
	private View updateLayout;
	private TextView updatemsg;
	private SharedPreferences sf;
	private String path;
	private int size;
	private int ver;
	private CountDownRunner clockCounter;
	private Thread clockThread;
	private Handler handler;

	public void doWork() {
		handler.post(new Runnable() {
			public void run() {
				try {
					mTimeTxt.setText((String) DateFormat.format("hh:mm aaa",
							new Date()));
				} catch (Exception e) {

				}
			}
		});
	}

	class CountDownRunner implements Runnable {
		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					doWork();
					Thread.sleep(2000); // Pause of 2 Second
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		mSourceIntent = intent;
		handleNotification();
	}

	private boolean handleNotification() {
		boolean isNotification = true;
		if (isNotification = mSourceIntent.getBooleanExtra("isNotification",
				false)) {
			mSourceIntent.removeExtra("isNotification");
			if (Build.VERSION.SDK_INT > 11) {
				RuntimeData.setBuilder(new AlertDialog.Builder(this, 5));
			} else {
				RuntimeData.setBuilder(new AlertDialog.Builder(this));
			}
			StringBuilder sb = new StringBuilder();
			sb.append("Latest version of database dated "
					+ mSourceIntent.getStringExtra("date")
					+ " is available. By downloading this DB you will get the latest railway time table.");
			RuntimeData.getBuilder().setCancelable(false);
			RuntimeData.getBuilder().setTitle("Database update available");
			RuntimeData.getBuilder().setMessage(sb.toString());
			RuntimeData.getBuilder().setPositiveButton("Update",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							mFrontController.downloadDb(LandingActivity.this);
						}
					});
			RuntimeData.getBuilder().setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							RuntimeData.setAlert(null);
							finish();
						}
					});
			RuntimeData.setAlert(RuntimeData.getBuilder().create());
			RuntimeData.getAlert().show();
		}
		return isNotification;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sf = getSharedPreferences(RailTimeConstants.PREFS_NAME, 0);
		mSourceIntent = getIntent();
		setContentView(R.layout.activity_landing);
		initUI();
		mFrontController = FrontController.getInstance(this);
		if (!mFrontController.isEulaAccepted()
				&& !(RuntimeData.getAlert() != null && RuntimeData
						.isAlertRemoved())) {
			if (Build.VERSION.SDK_INT > 11) {
				RuntimeData.setBuilder(new AlertDialog.Builder(this, 5));
			} else {
				RuntimeData.setBuilder(new AlertDialog.Builder(this));
			}
			RuntimeData.getBuilder().setCancelable(false);
			RuntimeData.getBuilder().setTitle("End User License Agreement");
			RuntimeData.getBuilder().setMessage(
					getResources().getText(R.string.eula_agreement));
			RuntimeData.getBuilder().setPositiveButton("Accept",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							RuntimeData.setAlert(null);
							FrontController.getInstance(LandingActivity.this)
									.setEulaAccepted();
							if (DatabaseHandler.getInstance(
									LandingActivity.this).isDBPresent()) {
								FrontController.getInstance(
										LandingActivity.this).initDatabase();
							} else {
								updatemsg
										.setText("Database is not dowloaded for offline use. Download now?");
								updateLayout.findViewById(R.id.button2)
										.setOnClickListener(
												updateYesClickListener);
								updateLayout.findViewById(R.id.button1)
										.setOnClickListener(
												updateNoClickListener);
								updateLayout.setVisibility(View.VISIBLE);
							}

						}
					});
			RuntimeData.getBuilder().setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							RuntimeData.setAlert(null);
							finish();
						}
					});
			RuntimeData.setAlert(RuntimeData.getBuilder().create());
			RuntimeData.getAlert().show();
			TextView textView = (TextView) RuntimeData.getAlert().findViewById(
					android.R.id.message);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12.5f);
		} else {
			if (DatabaseHandler.getInstance(LandingActivity.this).isDBPresent()) {
				if (!handleNotification()) {
					FrontController.getInstance(LandingActivity.this)
							.initDatabase();
				}
			} else if (Utils.isNetworkAvailable(LandingActivity.this)) {
				updatemsg
						.setText("Database is not downloaded for offline use. Download now?");
				updateLayout.findViewById(R.id.button2).setOnClickListener(
						updateYesClickListener);
				updateLayout.findViewById(R.id.button1).setOnClickListener(
						updateNoClickListener);
				updateLayout.setVisibility(View.VISIBLE);
			}
		}
	}

	private OnClickListener updateYesClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			downloadDB();
			updateLayout.setVisibility(View.GONE);
		}
	};
	private OnClickListener updateNoClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			updateLayout.setVisibility(View.GONE);
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		clockThread = new Thread(clockCounter);
		clockThread.start();
		RuntimeData.setRunning(true);

		if (RuntimeData.getAlert() != null && RuntimeData.isAlertRemoved()) {
			RuntimeData.getAlert().show();
			RuntimeData.setAlertRemoved(false);
		}
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.isProgressRemoved()) {
			RuntimeData.getProgress_dialog().show();
			RuntimeData.setProgressRemoved(false);
		}
		isScrolled = false;
		refreshUpdateUI();

		if (RailTimeConstants.SHOW_ADS) {
			FrontController.getInstance(this).showAdv(this,
					findViewById(R.id.lyt_parent_landing));
			RuntimeData.getAdView().setAdListener(new AdListener() {

				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					if (!isScrolled) {
						((ScrollView) findViewById(R.id.scrollView)).scrollTo(
								0, 0);
						isScrolled = true;
					}
				}
			});
			if (RuntimeData.getAdView() != null) {
				RuntimeData.getAdView().resume();
			}

		}

	}

	@Override
	public void onPause() {
		clockThread.interrupt();
		super.onPause();

		RuntimeData.setRunning(false);
		if (RuntimeData.getAlert() != null
				&& RuntimeData.getAlert().isShowing()) {
			RuntimeData.getAlert().dismiss();
			RuntimeData.setAlertRemoved(true);
		}
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.getProgress_dialog().isShowing()) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgressRemoved(true);
		}
		if (RailTimeConstants.SHOW_ADS) {

			if (RuntimeData.getAdView() != null) {
				RuntimeData.getAdView().pause();
			}
		}

	}

	private boolean canShowDBUpdate() {
		int dbalertCount = sf
				.getInt(RailTimeConstants.NOTIFICATION_DB_COUNT, 0);
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(System.currentTimeMillis());
		today.set(Calendar.HOUR, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		long prefDate = sf.getLong("updateday", 0);
		Calendar prefDay = Calendar.getInstance();
		prefDay.setTimeInMillis(prefDate);
		if (prefDay.before(today)) {
			sf.edit().putLong("updateday", today.getTimeInMillis()).commit();
			return true && (dbalertCount < 5);
		}
		return false;
	}

	private void refreshUpdateUI() {
		if (Utils.isNetworkAvailable(LandingActivity.this)) {
			if (isDBUpdateAvailable() && canShowDBUpdate()) {
				updatemsg
						.setText("A database update is available. Update now?");
				updateLayout.findViewById(R.id.button2).setOnClickListener(
						updateYesClickListener);
				updateLayout.findViewById(R.id.button1).setOnClickListener(
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								updateLayout.setVisibility(View.GONE);
							}
						});
				updateLayout.setVisibility(View.VISIBLE);
			} else {
				updateLayout.setVisibility(View.GONE);
			}
		}
	}

	private boolean isDBUpdateAvailable() {
		return sf.getBoolean(RailTimeConstants.DB_UPDATE_FOUND, false);
	}

	private void initUI() {
		findViewById(R.id.l2).setOnClickListener(this);
		findViewById(R.id.l3).setOnClickListener(this);
		findViewById(R.id.l4).setOnClickListener(this);
		findViewById(R.id.l5).setOnClickListener(this);
		findViewById(R.id.l6).setOnClickListener(this);
		findViewById(R.id.l8).setOnClickListener(this);
		findViewById(R.id.l10).setOnClickListener(this);
		updateLayout = findViewById(R.id.update);
		updatemsg = (TextView) updateLayout.findViewById(R.id.tv1);
		mTimeTxt = (TextView) findViewById(R.id.textView1);
		mDateTxt = (TextView) findViewById(R.id.textView);
		mDateTxt.setText((String) DateFormat.format("dd MMM, EEEE", Calendar
				.getInstance().getTime()));
		clockCounter = new CountDownRunner();
		handler = new Handler();
		Typeface custom_font = Typeface
				.createFromAsset(getAssets(), "4365.ttf");
		((TextView) findViewById(R.id.textViewx)).setTypeface(custom_font,
				Typeface.BOLD);
	}

	@Override
	public void onClick(View v) {

		// If DB is not downloaded still user can go inside settings and PNR
		// From here user can open navigation and go to any place say passing by
		// trains
		// there are crashes observed.. so disabling this option. Let user
		// always download DB
		// and then do any operation.
		if (v.getId() == R.id.l4 || v.getId() == R.id.l8
				|| v.getId() == R.id.l10) {
			loadScreen(v.getId());
		} else if (!DatabaseHandler.getInstance(LandingActivity.this)
				.isDBPresent()) {
			if (RuntimeData.isBgDwnldInProgress()) {
				RuntimeData.getDwnldMgr().showDownloadProgress(
						LandingActivity.this);
			} else {
				if (!Utils.isNetworkAvailable(LandingActivity.this)) {
					IrtttAlertDialog.showAlert(LandingActivity.this,
							RailTimeConstants.NO_NET_CONNECTION);
					return;
				} else {
					StringBuffer sb = new StringBuffer();
					sb.append("The database file is not downloaded yet or got corrupted. You will be able to use offline time-table only");
					sb.append(" if DB is available locally. You need to have internet connection ");
					sb.append("to be able to download the DB.");
					sb.append("\n\nWould you like to download for free now?");

					RuntimeData.setBuilder(new AlertDialog.Builder(this));
					if (Build.VERSION.SDK_INT > 11) {
						RuntimeData
								.setBuilder(new AlertDialog.Builder(this, 5));
					} else {
						RuntimeData.setBuilder(new AlertDialog.Builder(this));
					}

					RuntimeData.getBuilder().setCancelable(false);

					RuntimeData.getBuilder().setMessage(sb.toString());
					RuntimeData.getBuilder().setTitle("DB Download");
					RuntimeData.getBuilder().setPositiveButton(
							R.string.sentence_yes_text,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									mFrontController
											.downloadDb(LandingActivity.this);
									updateLayout.setVisibility(View.GONE);
								}
							});
					RuntimeData.getBuilder().setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									RuntimeData.setAlert(null);
								}
							});
					RuntimeData.setAlert(RuntimeData.getBuilder().create());
					RuntimeData.getAlert().show();
				}
			}
		} else {
			if (RuntimeData.isBgDwnldInProgress()) {
				RuntimeData.getDwnldMgr().showDownloadProgress(
						LandingActivity.this);
			} else {
				loadScreen(v.getId());
			}
		}

	}

	private void loadScreen(int id) {
		Intent intent = new Intent(LandingActivity.this, UserActivity.class);
		switch (id) {
		case R.id.l2:
			intent.putExtra("screen", 1);
			startActivity(intent);
			break;

		case R.id.l3:
			intent.putExtra("screen", 2);
			startActivity(intent);
			break;

		case R.id.l4:
			intent.putExtra("screen", 3);
			startActivity(intent);
			break;

		case R.id.l5:
			intent.putExtra("screen", 4);
			startActivity(intent);
			break;

		case R.id.l8:
			intent.putExtra("screen", 7);
			startActivity(intent);
			break;

		case R.id.l10:
			intent.putExtra("screen", 6);
			startActivity(intent);
			break;
		default:
			break;
		}

	}

	private void downloadDB() {
		String latestDbDate = sf
				.getString(RailTimeConstants.LATEST_DB_DATE, "");
		int latestDbSize = sf.getInt(RailTimeConstants.LATEST_DB_SIZE, -1);
		String latestDbPath = sf
				.getString(RailTimeConstants.LATEST_DB_PATH, "");
		if (latestDbDate.length() == 0 || latestDbSize <= 0
				|| latestDbPath.length() == 0) {
			// INvalid state. Just return
			return;
		}
		mFrontController.downloadDb(LandingActivity.this);
	}

	public int getVer() {
		return ver;
	}

	public void setVer(int ver) {
		this.ver = ver;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		DatabaseHandler.getInstance(LandingActivity.this).closeDB();
		clockCounter = null;
		handler = null;
		RuntimeData.setProgress_dialog(null);
		RuntimeData.setAlert(null);
	}
}
