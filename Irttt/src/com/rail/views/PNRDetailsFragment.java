package com.rail.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.lite.R;
import com.rail.model.PNRData;
import com.rail.model.PNRData.Passenger;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class PNRDetailsFragment extends Fragment  {
	private boolean isScrolled;
	private FrontController mFrontController;
	private TextView mTrainNumberTxt;
	private TextView mTrainNameTxt;
	private TextView mClassTxt;
	private TextView mNumPassengersTxt;
	private TextView mFromToTxt;
	private TextView mReserveFromTxt;
	private TextView mReserveToTxt;
	private TextView mPnrTxt;
	private TextView mDojTxt;
	private TextView mChartPrepTxt;
	private ProgressBar mProgressBar;
	private LinearLayout mListView;
	private View ll1;
	private LayoutInflater inflater;
	private View ll2;
	private AsyncTask<String, Void, PNRData> execute;
	private String pnrstr;
	private PNRData pnrData;
	private StringBuffer textBuffer = new StringBuffer();
	private StringBuffer htmlBuffer = new StringBuffer();
	private Bitmap myBitmap;
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		if (pnrData != null) {
			getActivity().getMenuInflater().inflate(R.menu.all_share, menu);
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					textBuffer = new StringBuffer();
					htmlBuffer = new StringBuffer();
					textBuffer.append("** PNR status for PNR - "+pnrData.getPnrNumber().toUpperCase(Locale.US));
					textBuffer.append("\n**");
					textBuffer.append("TrNum->"
							+ pnrData.getTrainNumber().trim());
					textBuffer.append(";TrName->"
							+ pnrData.getTrainName().trim());
					textBuffer.append(";Chart prepared->"
							+ pnrData.getChartPrepared().trim());
					textBuffer.append(";Class->"
							+ pnrData.getTypeOfClass().trim());
					textBuffer.append(";From & To->"
							+ pnrData.getFromNTo().trim());
					textBuffer.append(";Reservation from->"
							+ pnrData.getReservationFrom().trim());
					textBuffer.append(";Reservation to->"
							+ pnrData.getReservationTo().trim());
					textBuffer.append(";Num of Passengers->"
							+ pnrData.getNumberOfPassengers().trim());
					textBuffer.append(";Date of Journey->"
							+ pnrData.getDateOfJourney().trim());
					textBuffer.append(";Num of Passengers->"
							+ pnrData.getNumberOfPassengers().trim());
					textBuffer.append(";Passengers->");
					htmlBuffer.append("<html> PNR status for PNR - <b>"+pnrData.getPnrNumber().toUpperCase(Locale.US)
								+ "</b><br/><br/>");
					htmlBuffer.append("TrNum&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getTrainNumber().trim()+"<br/>"+"</b>");
					htmlBuffer.append("TrName&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getTrainName().trim()+"<br/>"+"</b>");
					htmlBuffer.append("From & To&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getFromNTo().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Date of Journey&nbsp;&nbsp;&nbsp;<b>"+pnrData.getDateOfJourney().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Reservation from&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getReservationFrom().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Reservation to&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getReservationTo().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Class&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getTypeOfClass().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Num of Passengers&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getNumberOfPassengers().trim()+"<br/>"+"</b>");
					htmlBuffer.append("Chart prepared&nbsp;&nbsp;&nbsp;<b>"+ pnrData.getChartPrepared().trim()+"<br/>"+"</b>");
					htmlBuffer.append("<b>Passengers&nbsp;&nbsp;&nbsp;</b><br/>");
					htmlBuffer.append("Passenger num&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Booking status&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Current status&nbsp;&nbsp;&nbsp;");
					for (Passenger passenger : pnrData.getPassengers()) {
						textBuffer.append("*Passenger num-" + passenger.getPassengerNum().trim() + "*");
						textBuffer.append("Current status-" + passenger.getCurrentStatus().trim());
						htmlBuffer.append("<br/><b>&nbsp;&nbsp;&nbsp;" + passenger.getPassengerNum().trim()+"</b>");
						htmlBuffer.append("<b>&nbsp;&nbsp;&nbsp;" + passenger.getBookingStatus().trim()+"</b>");
						htmlBuffer.append("<b>&nbsp;&nbsp;&nbsp;" + passenger.getCurrentStatus().trim()+"</b>");
					}
					textBuffer.append(";");
					textBuffer.append("**");
					textBuffer.append("\n");
					textBuffer
					.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
					htmlBuffer
					.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");
				}
			});
		}
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.item3:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, textBuffer.toString());
			sendIntent.setType("text/plain");
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"PNR status from IRTTT");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;
		case R.id.item4:
			sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					Html.fromHtml(htmlBuffer.toString()));
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"PNR status from IRTTT");
			sendIntent.setType("text/html");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;

		case R.id.item5:
			View v1 = getActivity().getWindow().getDecorView().getRootView();
			v1.setDrawingCacheEnabled(true);
			myBitmap = Bitmap.createBitmap(v1.getDrawingCache());
			saveBitmap(myBitmap);
			v1.destroyDrawingCache();
			v1.setDrawingCacheEnabled(false);
			myBitmap=null;
			break;

		default:
			break;
		}

		return true;
	}
	
	public void saveBitmap(Bitmap bitmap) {
		File root = Environment.getExternalStorageDirectory();
		if (root != null) {
			File extFolder = new File(root, getActivity().getPackageName());
			File picFolder = new File(extFolder, "pictures");
			if(!picFolder.exists()){
				picFolder.mkdir();
			}
			for (File file : picFolder.listFiles()) {
				file.delete();
			}
			File imagePath = new File(picFolder,System.currentTimeMillis()+"_screenshot.png");
	
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(imagePath);
			bitmap.compress(CompressFormat.PNG, 100, fos);
			fos.flush();
			fos.close();
			sendMail(imagePath.getAbsolutePath());
		} catch (FileNotFoundException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("GREC", e.getMessage(), e);
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	}

	public void sendMail(String path) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"PNR status from IRTTT");
		
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html
				.fromHtml("<html> PNR status for <b>"
						+ pnrData.getPnrNumber()
								.toUpperCase(Locale.US)
						+ " </b><br/><br/></html>"));
		emailIntent.setType("image/png");
		Uri myUri = Uri.parse("file://" + path);
		emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		} catch (Exception e) {
			// ignore if not activity to handle
		}
	};
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		pnrstr = bundle.getString("pnr");
		mFrontController = FrontController.getInstance(getActivity());
		execute = new PNRWebTask().execute(pnrstr);
	}

	@Override
	public void onResume() {
		super.onResume();
		isScrolled = false;
		getActivity().supportInvalidateOptionsMenu();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		View rootView = inflater.inflate(R.layout.fgmt_pnrdetails, container,
				false);
		initUI(rootView);
		this.inflater = inflater;
		setHasOptionsMenu(true);
		((UserActivity) getActivity()).setTitle(getActivity().getResources()
				.getString(R.string.pnr_status_text) + " : " + pnrstr);
		loadData(pnrstr);
		setRetainInstance(true);
		return rootView;
	}

	private void loadData(String pnrstr) {

		if (pnrData == null) {
			execute = new PNRWebTask().execute(pnrstr);
		} else {
			processResult(pnrData);
		}

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		execute.cancel(true);
	}

	public class PNRWebTask extends AsyncTask<String, Void, PNRData> {

		@Override
		protected PNRData doInBackground(String... params) {
			return mFrontController.getPNRFromInternet(params[0]);
		}

		protected void onPostExecute(PNRData pnrData) {
			if (!execute.isCancelled()) {
				processResult(pnrData);
				PNRDetailsFragment.this.pnrData = pnrData;
			}
		}

	}

	private void setAdapter(List<Passenger> passengers) {
		mListView.removeAllViews();
		int i = 0;
		for (Passenger passenger : passengers) {
			int position = i++;
			View convertView = inflater.inflate(R.layout.lyt_psgr_pnr, null,
					false);

			TextView slNo = (TextView) convertView
					.findViewById(R.id.textView12);
			TextView bookingStatus = (TextView) convertView
					.findViewById(R.id.textView13);
			TextView currentStatus = (TextView) convertView
					.findViewById(R.id.textView14);

			slNo.setText(passenger.getPassengerNum());
			bookingStatus.setText(passenger.getBookingStatus());
			currentStatus.setText(passenger.getCurrentStatus());
			if (position % 2 == 0) {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(R.color.light_gray));
			} else {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(android.R.color.background_light));
			}
			mListView.addView(convertView);
		}

	};

	private void initUI(final View rootView) {
		
		mTrainNumberTxt = (TextView) rootView.findViewById(R.id.textView20);
		mTrainNameTxt = (TextView) rootView.findViewById(R.id.textView21);
		mFromToTxt = (TextView) rootView.findViewById(R.id.textView22);
		mDojTxt = (TextView) rootView.findViewById(R.id.textView31);
		mPnrTxt = (TextView) rootView.findViewById(R.id.textView32);
		mChartPrepTxt = (TextView) rootView.findViewById(R.id.textView33);
		mReserveFromTxt = (TextView) rootView.findViewById(R.id.textView41);
		mReserveToTxt = (TextView) rootView.findViewById(R.id.textView42);
		mNumPassengersTxt = (TextView) rootView.findViewById(R.id.textView50);
		mClassTxt = (TextView) rootView.findViewById(R.id.textView52);
		mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress);

		mListView = (LinearLayout) rootView.findViewById(R.id.listView1);
		rootView.findViewById(R.id.linearLayout1).setBackgroundColor(getActivity().getResources().getColor(R.color.brown));
		((TextView) rootView.findViewById(R.id.textView12))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) rootView.findViewById(R.id.textView13))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) rootView.findViewById(R.id.textView14))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		ll1 = rootView.findViewById(R.id.ll11);
		ll2 = rootView.findViewById(R.id.linearLayout1);
		mFrontController.showAdv(getActivity(), rootView);
		RuntimeData.getAdView().setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				if(!isScrolled){
					((ScrollView)rootView.findViewById(R.id.scrollView)).scrollTo(0, 0);
					isScrolled = true;
				}
			}
		});
	}

	public void processResult(PNRData pnrData) {

		if (pnrData != null && pnrData.getPnrNumber() != null
				&& pnrData.getPnrNumber().length() > 0
				&& pnrData.getTrainName() != null) {
			getActivity().supportInvalidateOptionsMenu();
			mTrainNameTxt.setText(pnrData.getTrainName());
			mTrainNumberTxt.setText(pnrData.getTrainNumber());
			mChartPrepTxt.setText(pnrData.getChartPrepared());
			mClassTxt.setText(pnrData.getTypeOfClass());
			mFromToTxt.setText(pnrData.getFromNTo());
			mReserveFromTxt.setText(pnrData.getReservationFrom());
			mReserveToTxt.setText(pnrData.getReservationTo());
			mNumPassengersTxt.setText(pnrData.getNumberOfPassengers());
			mDojTxt.setText(pnrData.getDateOfJourney());
			mPnrTxt.setText(pnrData.getPnrNumber());

			setAdapter(pnrData.getPassengers());
			mListView.setOnTouchListener(new OnTouchListener() {
				// Setting on Touch Listener for handling the touch
				// inside
				// ScrollView
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// Disallow the touch request for parent scroll on
					// touch
					// of child view
					v.getParent().requestDisallowInterceptTouchEvent(true);
					return false;
				}
			});
			mTrainNameTxt.setVisibility(View.VISIBLE);
			mTrainNumberTxt.setVisibility(View.VISIBLE);
			mChartPrepTxt.setVisibility(View.VISIBLE);
			mClassTxt.setVisibility(View.VISIBLE);
			mFromToTxt.setVisibility(View.VISIBLE);
			mReserveFromTxt.setVisibility(View.VISIBLE);
			mReserveToTxt.setVisibility(View.VISIBLE);
			mNumPassengersTxt.setVisibility(View.VISIBLE);
			mDojTxt.setVisibility(View.VISIBLE);
			mPnrTxt.setVisibility(View.VISIBLE);
			mProgressBar.setVisibility(View.GONE);
			ll1.setVisibility(View.VISIBLE);
			ll2.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);
		} else {
			// show error alert
			mProgressBar.setVisibility(View.GONE);
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("Unable to fetch your PNR info.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							if(getFragmentManager()!=null)
							getFragmentManager().popBackStackImmediate();
						}
					});
			AlertDialog alertDialog = builder.create();
			alertDialog.setCancelable(false);
			alertDialog.show();
		}

	}

	
}
