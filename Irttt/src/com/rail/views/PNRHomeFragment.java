package com.rail.views;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.utilities.IrtttAlertDialog;
import com.rail.utilities.Utils;

public class PNRHomeFragment extends Fragment implements OnClickListener {
	private FrontController mFrontController;
	private EditText mPnrEdt;
	private Button mCheckStatusBtn;
	private LinearLayout mListView;
	private List<String> PNRHistory;
	private ImageButton mClearText;
	private TextView r1, r2;
	private LinearLayout l1;
	private View rootView;
	private String pnrText;
	private LayoutInflater inflater;
	private LinearLayout advlayout;

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menu.add("Clear all PNR recents");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		mFrontController.clearRecentPNR();
		PNRHistory = mFrontController.getPNRHistory();
		setAdapter(PNRHistory);
		if (PNRHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
			mListView.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
			mListView.setVisibility(View.VISIBLE);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		this.inflater = inflater;
		rootView = inflater.inflate(R.layout.fgmt_pnrhome, container, false);
		setHasOptionsMenu(true);
		initUI(rootView);
		((UserActivity) getActivity()).setTitle(R.string.pnr_status_text);
		setRetainInstance(true);
		if (pnrText != null) {
			mPnrEdt.setText(pnrText);
		}
		return rootView;
	}
	@Override
	public void onResume() {
		super.onResume();
		getActivity().supportInvalidateOptionsMenu();
	}
	private void initUI(final View rootView) {
		mFrontController = FrontController.getInstance(getActivity());
		mListView = (LinearLayout) rootView.findViewById(R.id.listView1);
		mPnrEdt = (EditText) rootView.findViewById(R.id.actv_src_station_name);
		mCheckStatusBtn = (Button) rootView.findViewById(R.id.button1);
		mCheckStatusBtn.setOnClickListener(this);
		mCheckStatusBtn.setText(Html
				.fromHtml("CHECK<b><big> PNR STATUS</big> <b/> "));
		mPnrEdt.addTextChangedListener(new PNRTextChangedListener());
		mClearText = (ImageButton) rootView
				.findViewById(R.id.imageButton1stsrcsearch);
		mClearText.setOnClickListener(this);
		PNRHistory = mFrontController.getPNRHistory();
		mFrontController.showAdv(getActivity(), rootView);
		advlayout = (LinearLayout) rootView
				.findViewById(R.id.linearLayoutmain_relative5);

		advlayout.setVisibility(View.VISIBLE);
		setAdapter(PNRHistory);
		mListView.setOnTouchListener(new OnTouchListener() {
			// Setting on Touch Listener for handling the touch inside
			// ScrollView
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// Disallow the touch request for parent scroll on touch of
				// child view
				v.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

		r1 = (TextView) rootView.findViewById(R.id.textView1);
		r2 = (TextView) rootView.findViewById(R.id.textView2);
		l1 = (LinearLayout) rootView.findViewById(R.id.linearLayout1);
		if (PNRHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
		}
	}

	String pnrHistoryStr = "";

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		menu.clear();
		super.onCreateContextMenu(menu, v, menuInfo);
		pnrHistoryStr = v.getTag().toString();
		menu.add("Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		mFrontController.removeRecentPNRHistory(pnrHistoryStr);
		pnrHistoryStr = "";
		PNRHistory = mFrontController.getPNRHistory();
		setAdapter(PNRHistory);
		if (PNRHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
		}
		return super.onContextItemSelected(item);
	}

	public class PNRTextChangedListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {

			if (mPnrEdt != null && mPnrEdt.getText().toString().length() > 0) {
				mCheckStatusBtn.setEnabled(true);
				mClearText.setVisibility(View.VISIBLE);
			} else {
				mCheckStatusBtn.setEnabled(false);
				mClearText.setVisibility(View.GONE);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence userInput, int start,
				int before, int count) {

		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			PNRHistory = mFrontController.storePNRHistory(mPnrEdt.getText()
					.toString());
			hideKeyboard();
			if (Utils.isNetworkAvailable(getActivity())) {
				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				pnrText = mPnrEdt.getText().toString();
				PNRDetailsFragment pnrDetails = new PNRDetailsFragment();
				Bundle bundle = new Bundle();
				bundle.putString("pnr", mPnrEdt.getText().toString());
				pnrDetails.setArguments(bundle);

				ft.replace(R.id.fragment_container, pnrDetails).addToBackStack(
						null);
				ft.commit();
			} else {
				IrtttAlertDialog.showAlert(getActivity(),
						RailTimeConstants.NO_NET_CONNECTION);
			}
			break;
		case R.id.imageButton1stsrcsearch:
			mPnrEdt.setText("");
			break;
		default:
			break;
		}

	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mPnrEdt.getWindowToken(), 0);
	}

	private void setAdapter(List<String> pnrs) {
		mListView.removeAllViews();
		int i = 0;
		for (String _pnr : pnrs) {
			int position = i++;
			View convertView = inflater.inflate(R.layout.lyt_pnr_history_item,
					null, false);
			convertView.findViewById(R.id.imageView1).setVisibility(
					View.VISIBLE);
			TextView slNo = (TextView) convertView.findViewById(R.id.textView3);
			TextView pnr = (TextView) convertView.findViewById(R.id.textView4);
			TextView status = (TextView) convertView
					.findViewById(R.id.textView5);
			status.setVisibility(View.INVISIBLE);
			slNo.setText(position + 1 + "");
			pnr.setText(_pnr);

			if (position % 2 == 0) {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(R.color.light_gray));
			} else {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(android.R.color.background_light));
			}
			convertView.setTag(_pnr);

			convertView.findViewById(R.id.imageView1).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							mFrontController.removeRecentPNRHistory(((View) v
									.getParent()).getTag().toString());
							pnrHistoryStr = "";
							PNRHistory = mFrontController.getPNRHistory();
							setAdapter(PNRHistory);
							if (PNRHistory.size() == 0) {
								r2.setVisibility(View.GONE);
								r1.setVisibility(View.GONE);
								l1.setVisibility(View.GONE);
							} else {
								r1.setVisibility(View.VISIBLE);
								r2.setVisibility(View.VISIBLE);
								l1.setVisibility(View.VISIBLE);
							}
						}
					});

			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					hideKeyboard();
					if (Utils.isNetworkAvailable(getActivity())) {

						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();

						PNRDetailsFragment pnrDetails = new PNRDetailsFragment();
						Bundle bundle = new Bundle();
						bundle.putString("pnr", (String) v.getTag());
						pnrDetails.setArguments(bundle);

						ft.replace(R.id.fragment_container, pnrDetails)
						.addToBackStack(null);
						ft.commit();
					} else {
						IrtttAlertDialog.showAlert(getActivity(),
								RailTimeConstants.NO_NET_CONNECTION);
					}				}
			});
			mListView.addView(convertView);

		}

	}
}
