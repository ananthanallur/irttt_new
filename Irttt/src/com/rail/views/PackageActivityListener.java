package com.rail.views;

import java.util.Calendar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.UpdateDetails;
import com.rail.network.UpdateFinder;
import com.rail.network.UpdateFinderResultListener;
import com.rail.persistance.DatabaseHandler;
import com.rail.utilities.Utils;

public class PackageActivityListener extends BroadcastReceiver implements
		UpdateFinderResultListener {
	private Context context;
	private SharedPreferences sharedPref;

	@Override
	public void onReceive(final Context ctx, Intent arg1) {
		try {
			this.context = ctx;
			sharedPref = context.getSharedPreferences(RailTimeConstants.PREFS_NAME, 0);
			if (Utils.isNetworkAvailable(context) && canFindDBUpdate()
					&& DatabaseHandler.getInstance(context).isDBPresent()) {
				UpdateFinder updateFinder = new UpdateFinder(context);
				updateFinder.findDBUpdate(this);
			}
		}catch (Exception e){
			//Ignore Error
		}
	}

	private void notifyUpdates(String date) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon,
				"DB updates available", when);
		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context, LandingActivity.class);
		notificationIntent.putExtra("isNotification", true);
		notificationIntent.putExtra("date", date);
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
				| Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		notification.setLatestEventInfo(context, title,
				"Database update available", intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(RailTimeConstants.APK_DB_NOTIFIER_ID,
				notification);

	}

	
	@Override
	public void onDBUpdateFinderFinished(UpdateDetails dbUpdateDetails) {
		try {
			int cloudDBVer = sharedPref.getInt(RailTimeConstants.SERVER_DB_VER, -1);
			int notificationDBVer = sharedPref.getInt(
					RailTimeConstants.NOTIFICATION_DB_VER, -1);
			if (cloudDBVer > notificationDBVer) {
				sharedPref.edit()
						.putInt(RailTimeConstants.NOTIFICATION_DB_VER, cloudDBVer)
						.commit();
				sharedPref.edit()
						.putInt(RailTimeConstants.NOTIFICATION_DB_COUNT, 0)
						.commit();
			} else {
				int count = sharedPref.getInt(
						RailTimeConstants.NOTIFICATION_DB_COUNT, 0);
				sharedPref.edit()
						.putInt(RailTimeConstants.NOTIFICATION_DB_COUNT, count + 1)
						.commit();
			}
			if (dbUpdateDetails != null && canShowDBUpdate() && dbUpdateDetails.isUpdateAvailable()) {
				String date = sharedPref.getString(
						RailTimeConstants.LATEST_DB_DATE, "-");
				notifyUpdates(date);
			}
		}catch (Exception e){
			//Ignore Error
		}
	}

	private boolean canShowDBUpdate() {
		int dbNotificationCount = sharedPref.getInt(
				RailTimeConstants.NOTIFICATION_DB_COUNT, 0);
		return (dbNotificationCount < 3);
	}

	private boolean canFindDBUpdate() {
		// DB update URL check can be triggered only once for day
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(System.currentTimeMillis());
		today.set(Calendar.HOUR, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);

		long prefDate = sharedPref.getLong(RailTimeConstants.TODAY, 0);
		Calendar prefDay = Calendar.getInstance();
		prefDay.setTimeInMillis(prefDate);
		if (prefDay.before(today)) {
			sharedPref.edit()
					.putLong(RailTimeConstants.TODAY, today.getTimeInMillis())
					.commit();
			return true;
		}
		return false;
	}

	@Override
	public void onAppUpdateFinderFinished(UpdateDetails updateDetails) {
	}
}
