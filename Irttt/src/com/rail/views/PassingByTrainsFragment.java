package com.rail.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.rail.controller.FrontController;
import com.rail.lite.R;
import com.rail.model.ScreenData;
import com.rail.model.Station;

public class PassingByTrainsFragment extends Fragment implements
		OnClickListener {
	private AutoCompleteTextView src_station_name_actv;
	private ArrayAdapter<String> actv_adapter;
	private List<String> stationNames = new ArrayList<String>();
	private FrontController mFrontController;
	private ImageButton mClearSrc;
	private CheckBox mUserNearBy;
	private TextView mDateTxt, mMonthTxt, mYearTxt;
	private ImageView mCalendarImg;
	private int day, month=-1, year;
	private Button mDayBtn;
	private Button mWeekButton;
	private DatePickerDialog datePickerDialog;
	private LinearLayout mStationHistory;
	private List<String> passingTrainStationHistory;
	private TextView r1, r2;
	private LinearLayout l1;
	private LayoutInflater inflater;
	private View rootView;
	private LinearLayout advlayout;
	private String src;
	private boolean isChecked;

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menu.add("Clear all passing trains recents");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		mFrontController.clearRecentPassingTrains();
		passingTrainStationHistory = mFrontController
				.getPassingTrainStationHistory();
		setAdapter(passingTrainStationHistory);
		if (passingTrainStationHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().supportInvalidateOptionsMenu();
		
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		setHasOptionsMenu(true);
		this.inflater = inflater;
		rootView = inflater.inflate(R.layout.fgmt_passingbytrainshome,
				container, false);
		mFrontController = FrontController.getInstance(getActivity());
		if (day == 0 || month == -1 || year == 0) {
			Calendar cal = Calendar.getInstance();
			day = cal.get(Calendar.DAY_OF_MONTH);
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);
		}
		((UserActivity) getActivity())
				.setTitle(R.string.passing_by_trains_text);
		initUI();
		setRetainInstance(true);
		
		advlayout.setVisibility(View.VISIBLE);
		if (src != null) {
			src_station_name_actv.setText(src);
			updateDate();
			mUserNearBy.setChecked(isChecked);
		}
		return rootView;
	}

	

	private void initUI() {
		actv_adapter = new AutoCompleteAdapter(getActivity(),
				R.layout.lyt_auto_comp_item, R.id.textView1, stationNames);
		src_station_name_actv = (AutoCompleteTextView) rootView
				.findViewById(R.id.actv_src_station_name);
		src_station_name_actv
				.addTextChangedListener(new SrcStationTextChangedListener());
		src_station_name_actv.setAdapter(actv_adapter);
		mClearSrc = (ImageButton) rootView
				.findViewById(R.id.imageButton1stsrcsearch);
		mClearSrc.setOnClickListener(this);
		mDateTxt = (TextView) rootView.findViewById(R.id.textView3);
		mMonthTxt = (TextView) rootView.findViewById(R.id.textView7);
		mYearTxt = (TextView) rootView.findViewById(R.id.textView8);
		mCalendarImg = (ImageView) rootView.findViewById(R.id.imageButton3);
		mUserNearBy = (CheckBox) rootView.findViewById(R.id.checkBox1);
		mDayBtn = (Button) rootView.findViewById(R.id.button1);
		mWeekButton = (Button) rootView.findViewById(R.id.button2);
		mStationHistory = (LinearLayout) rootView.findViewById(R.id.listView1);
		advlayout = (LinearLayout) rootView
				.findViewById(R.id.linearLayoutmain_relative5);

		mCalendarImg.setOnClickListener(this);
		rootView.findViewById(R.id.linearLayout1sttraveldate).setOnClickListener(this);
		mDayBtn.setOnClickListener(this);
		mWeekButton.setOnClickListener(this);
		mDayBtn.setText(Html
				.fromHtml("FIND TRAINS FOR THE<br/><b><big> DAY</big></b>"));
		mDayBtn.setLineSpacing(1, 1.1f);
		mWeekButton.setText(Html
				.fromHtml("FIND TRAINS FOR THE<br/><b><big>WEEK</big></b>"));
		mWeekButton.setLineSpacing(1, 1.1f);
		passingTrainStationHistory = mFrontController
				.getPassingTrainStationHistory();

		setAdapter(passingTrainStationHistory);
		r1 = (TextView) rootView.findViewById(R.id.textView1);
		r2 = (TextView) rootView.findViewById(R.id.textView2);
		l1 = (LinearLayout) rootView.findViewById(R.id.linearLayout1);
		if (passingTrainStationHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
		}

		updateDate();
		src_station_name_actv.setOnItemClickListener(new OnItemClickListener() {

			

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(src_station_name_actv.getWindowToken(), 0);
			}
			
		});
		FrontController.getInstance(getActivity()).showAdv(getActivity(),
				rootView);
		
	}

	private String station = "";

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		station = v.getTag().toString();
		menu.add("Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		mFrontController.removeRecentPassingTrains(station);
		passingTrainStationHistory = mFrontController
				.getPassingTrainStationHistory();

		setAdapter(passingTrainStationHistory);
		if (passingTrainStationHistory.size() == 0) {
			r2.setVisibility(View.GONE);
			r1.setVisibility(View.GONE);
			l1.setVisibility(View.GONE);
		} else {
			r1.setVisibility(View.VISIBLE);
			r2.setVisibility(View.VISIBLE);
			l1.setVisibility(View.VISIBLE);
		}
		return super.onContextItemSelected(item);
	}

	private void updateDate() {
		String month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" };
		mDateTxt.setText(PassingByTrainsFragment.this.day + "");
		mMonthTxt.setText(month[PassingByTrainsFragment.this.month]
				.toUpperCase(Locale.ENGLISH));
		mYearTxt.setText(PassingByTrainsFragment.this.year + "");
	}

	public class SrcStationTextChangedListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			stationNames = mFrontController.getStationNamesFromDB(s.toString()
					.toString());
			actv_adapter.notifyDataSetChanged();
			actv_adapter = new AutoCompleteAdapter(getActivity(),
					R.layout.lyt_auto_comp_item, R.id.textView1, stationNames);
			src_station_name_actv.setAdapter(actv_adapter);
			if (src_station_name_actv != null
					&& src_station_name_actv.getText().toString().length() > 0) {
				mDayBtn.setEnabled(true);
				mWeekButton.setEnabled(true);
				mClearSrc.setVisibility(View.VISIBLE);
			} else {
				mDayBtn.setEnabled(false);
				mWeekButton.setEnabled(false);
				mClearSrc.setVisibility(View.INVISIBLE);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence userInput, int start,
				int before, int count) {

		}

	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(src_station_name_actv.getWindowToken(), 0);
	}
	
	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		FragmentTransaction ft = null;
		hideKeyboard();
		switch (v.getId()) {
		case R.id.button1:
			Calendar cal;
			ScreenData data;
			Station srcStation;
			Bundle bundle;
			// mFrontController.saveRecentStation(src_station_name_actv.getText().toString());
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Unrecognized field");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
			
			if (src_station_name_actv.getText().toString().split(" - ").length < 2) {

				builder.setMessage("Please complete the station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else {
				ft = getFragmentManager().beginTransaction();
				data = new ScreenData(getActivity());
				srcStation = new Station(src_station_name_actv.getText()
						.toString().split(" - ")[1], src_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				data.setSource(srcStation);
				cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_MONTH, day);
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.YEAR, year);
				data.setDate(cal);
				data.setConsiderNearbyTrains(mUserNearBy.isChecked());
				DayTrainsPassingFragment dayTrainsBtwStations = new DayTrainsPassingFragment();
				bundle = new Bundle();
				bundle.putSerializable("data", data);
				bundle.putString("title",
						getString(R.string.passing_by_trains_text));
				dayTrainsBtwStations.setArguments(bundle);
				passingTrainStationHistory = mFrontController
						.storePassingTrainStationHistory(src_station_name_actv
								.getText().toString());
				src = src_station_name_actv.getText().toString();

				ft.replace(R.id.fragment_container, dayTrainsBtwStations)
						.addToBackStack(null);
				ft.commit();
			}
			break;
		case R.id.imageButton1stsrcsearch:
			src_station_name_actv.setText("");
			break;
		case R.id.linearLayout1sttraveldate:
		case R.id.imageButton3:
			datePickerDialog = new DatePickerDialog(getActivity(),
					new OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							PassingByTrainsFragment.this.month = monthOfYear;
							PassingByTrainsFragment.this.day = dayOfMonth;
							PassingByTrainsFragment.this.year = year;
							updateDate();
						}
					}, year, month, day);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				Calendar cal2 = Calendar.getInstance();
				cal2.set(Calendar.HOUR_OF_DAY,
						cal2.getMinimum(Calendar.HOUR_OF_DAY));
				cal2.set(Calendar.MINUTE, cal2.getMinimum(Calendar.MINUTE));
				cal2.set(Calendar.SECOND, cal2.getMinimum(Calendar.SECOND));
				cal2.set(Calendar.MILLISECOND,
						cal2.getMinimum(Calendar.MILLISECOND));
				datePickerDialog.getDatePicker().setMinDate(
						cal2.getTimeInMillis());
			}

			datePickerDialog.show();
			break;

		case R.id.imageButton1:
			cal = Calendar.getInstance();
			day = cal.get(Calendar.DAY_OF_MONTH);
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);
			updateDate();
			break;

		case R.id.button2:
			builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Unrecognized field");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

			if (src_station_name_actv.getText().toString().split(" - ").length < 2) {

				builder.setMessage("Please complete the station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else {
				ft = getFragmentManager().beginTransaction();
				data = new ScreenData(getActivity());
				srcStation = new Station(src_station_name_actv.getText()
						.toString().split(" - ")[1], src_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				data.setSource(srcStation);
				cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_MONTH, day);
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.YEAR, year);
				data.setDate(cal);
				data.setConsiderNearbyTrains(mUserNearBy.isChecked());
				WeekTrainsPassingFragment weekTrainsBtwStations = new WeekTrainsPassingFragment();
				bundle = new Bundle();
				bundle.putSerializable("data", data);
				src = src_station_name_actv.getText().toString();
				bundle.putString("title",
						getString(R.string.passing_by_trains_text));
				weekTrainsBtwStations.setArguments(bundle);
				passingTrainStationHistory = mFrontController
						.storePassingTrainStationHistory(src_station_name_actv
								.getText().toString());

				ft.replace(R.id.fragment_container, weekTrainsBtwStations)
						.addToBackStack(null);
				ft.commit();
			}
			break;

		default:
			break;
		}

	}

	class AutoCompleteAdapter extends ArrayAdapter<String> {

		public AutoCompleteAdapter(Context context, int resource,
				int textViewResourceId, List<String> objects) {
			super(context, resource, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			((TextView) view.findViewById(R.id.textView1))
					.setTextColor(getActivity().getResources().getColor(
							android.R.color.black));
			return view;
		}
	}

	private void setAdapter(List<String> passingTrainStationHistory2) {
		mStationHistory.removeAllViews();
		int i = 0;
		for (String stn : passingTrainStationHistory2) {
			int position = i++;
			View convertView = inflater.inflate(
					R.layout.lyt_train_list_item_type_3, null, false);

			TextView slNo = (TextView) convertView.findViewById(R.id.textView3);
			TextView station = (TextView) convertView
					.findViewById(R.id.textView4);
			convertView.findViewById(R.id.imageView1).setVisibility(View.VISIBLE);
			convertView.findViewById(R.id.imageView1).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mFrontController.removeRecentPassingTrains(((View)v.getParent()).getTag().toString());
					passingTrainStationHistory = mFrontController
							.getPassingTrainStationHistory();

					setAdapter(passingTrainStationHistory);
					if (passingTrainStationHistory.size() == 0) {
						r2.setVisibility(View.GONE);
						r1.setVisibility(View.GONE);
						l1.setVisibility(View.GONE);
					} else {
						r1.setVisibility(View.VISIBLE);
						r2.setVisibility(View.VISIBLE);
						l1.setVisibility(View.VISIBLE);
					}
				}
			});
			slNo.setText(position + 1 + "");
			station.setText(stn);
			convertView.setTag(stn);
			slNo.setTypeface(null, Typeface.BOLD);
			station.setTypeface(null, Typeface.BOLD);
			if (position % 2 == 0) {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(R.color.light_gray));
			} else {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(android.R.color.background_light));
			}
			convertView.findViewById(R.id.linearLayout1).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					src_station_name_actv.setText(((View)v.getParent()).getTag().toString());
					new Handler().post(new Runnable() {
						public void run() {
							((ScrollView)getActivity().findViewById(R.id.parent)).scrollTo(0, 0);
							src_station_name_actv.dismissDropDown();
						}
					});
				}
			});

			mStationHistory.addView(convertView);

		}

		mStationHistory.setVisibility(View.VISIBLE);

	}
}