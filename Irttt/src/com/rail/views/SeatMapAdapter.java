package com.rail.views;

import com.rail.lite.R;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SeatMapAdapter extends ArrayAdapter<String> {

	Context mContext;
	String[] mSelections; 
	
	public SeatMapAdapter(Context seatmapFragment, int txtViewResourceId, String[] objects) {
		super(seatmapFragment, txtViewResourceId, objects);
		mContext = seatmapFragment;
		mSelections = objects;
	}

	@Override
	public View getDropDownView(int position, View cnvtView, ViewGroup prnt) {
		return getCustomView(position, cnvtView, prnt);
	}

	@Override
	public View getView(int pos, View cnvtView, ViewGroup prnt) {
		return getCustomView(pos, cnvtView, prnt);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {
		LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = li.inflate(R.layout.lyt_seatmap_item, parent,
				false);
		TextView main_text = (TextView) view.findViewById(R.id.legend_text);
		main_text.setText(Html.fromHtml("<b><big>" + mSelections[position].substring(0, 2) + "</big></b>" 
											+ "" + mSelections[position].substring(2, mSelections[position].length()) +""));
		
		return view;
	}
}
