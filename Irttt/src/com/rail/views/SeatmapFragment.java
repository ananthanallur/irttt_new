package com.rail.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.lite.R;
import com.rail.utilities.RuntimeData;

public class SeatmapFragment extends Fragment {
	private boolean isScrolled;
	private View rootView;
	private FrontController mFrontController;
	private int prevSeatMapSelection;
	private Spinner mSpinner;
	private ImageView mSelectedImage;
	private final int SLEEPER = 8;
	private final int FIRST_AC = 1;
	private final int SECOND_AC = 2;
	private final int THIRD_AC = 3;
	private final int EC = 5;
	private final int GAREEBRATH = 6;
	private final int CHAIR_CAR = 4;
	private final int SECOND_CLASS = 7;
	
	
	private Context mContext;
	private String[] selections;
	Animation animationFadeIn;
	private LinearLayout border;
	
	@Override
	public void onResume() {
		super.onResume();
		isScrolled = false;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mContext = getActivity();
		rootView = inflater.inflate(R.layout.fgmt_seatmap, container,
				false);
		mFrontController = FrontController.getInstance(getActivity());
		initUI();
		loadData();
		((UserActivity) getActivity()).setTitle(R.string.seat_map);
		setRetainInstance(true);
		return rootView;
	}
	
	private void loadData() {
		animationFadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fadein);
		selections = mContext.getResources().getStringArray(R.array.seatmap_entries);
		prevSeatMapSelection = mFrontController.getPreviousSeatMapselection();
		mSpinner.setAdapter(new SeatMapAdapter(mContext, R.layout.lyt_seatmap_item,	selections));
		mSpinner.setSelection(prevSeatMapSelection);
		mSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				  changeSelection(position);
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				mSpinner.setSelection(0);
			}
		});
	    mFrontController.showAdv(getActivity(), rootView);
	    RuntimeData.getAdView().setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				if(!isScrolled){
					((ScrollView)rootView.findViewById(R.id.parent)).scrollTo(0, 0);
					isScrolled = true;
				}
			}
		});
	}

	private void initUI() {
		mSpinner = (Spinner) rootView.findViewById(R.id.seatmap_selecter);
		mSelectedImage = (ImageView) rootView.findViewById(R.id.seatmap_imageView);
		border=(LinearLayout)rootView.findViewById(R.id.seatmap_layout);
	}

	private void changeSelection(int pos) {
		switch (pos+1) {
		case SLEEPER:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.sleeper);
			mFrontController.setPreviousSeatMapselection(7);
			break;
		case FIRST_AC:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.first_ac);
			mFrontController.setPreviousSeatMapselection(0);
			break;
		case SECOND_AC:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.second_ac);
			mFrontController.setPreviousSeatMapselection(1);
			break;
		case THIRD_AC:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.third_ac);
			mFrontController.setPreviousSeatMapselection(2);
			break;
		case CHAIR_CAR:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.chair_car);
			mFrontController.setPreviousSeatMapselection(3);
			break;
		case EC:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.ec);
			mFrontController.setPreviousSeatMapselection(4);
			break;
		case GAREEBRATH:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.garibrath);
			mFrontController.setPreviousSeatMapselection(5);
			break;
		case SECOND_CLASS:
			mSelectedImage.startAnimation(animationFadeIn);
			border.startAnimation(animationFadeIn);
			mSelectedImage.setImageResource(R.drawable.second_class);
			mFrontController.setPreviousSeatMapselection(6);
			break;

		default:
			break;
		}
		mSpinner.setSelection(pos);
		border.setVisibility(View.VISIBLE);
	}
}
