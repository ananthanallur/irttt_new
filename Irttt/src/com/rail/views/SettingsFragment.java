package com.rail.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.UpdateDetails;
import com.rail.model.DbDetails;
import com.rail.network.UpdateFinder;
import com.rail.network.UpdateFinderResultListener;
import com.rail.persistance.DatabaseHandler;
import com.rail.utilities.IrtttAlertDialog;
import com.rail.utilities.OrientationUtils;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class SettingsFragment extends Fragment implements OnClickListener,
		UpdateFinderResultListener {
	private boolean isRunning = false;
	private Button btn4;
	private Button btn5;
	private View downloadBtn;
	private Context mContext;
	private BroadcastReceiver registerReceiver;
	private AlertDialog alert = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		registerReceiver = new DownloadListener();
		getActivity().registerReceiver(registerReceiver,
				new IntentFilter("DOWNLOAD_SUCCESS"));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		View rootView = inflater.inflate(R.layout.fgmt_settings, container,
				false);
		initUI(rootView);
		((UserActivity) getActivity()).setTitle(R.string.settings_text);
		setRetainInstance(true);
		return rootView;
	}

	private void initUI(final View rootView) {
		btn4 = (Button) rootView.findViewById(R.id.button4);
		btn5 = (Button) rootView.findViewById(R.id.button7);
		downloadBtn = rootView.findViewById(R.id.button8);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		downloadBtn.setOnClickListener(this);
		FrontController.getInstance(getActivity()).showAdv(getActivity(),
				rootView);
		RuntimeData.getAdView().setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				View scrollView = rootView.findViewById(R.id.scrollView);
				if (scrollView != null) {
					((ScrollView) scrollView).scrollTo(0, 0);
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		if (!Utils.isNetworkAvailable(mContext)) {
			IrtttAlertDialog.showAlert(mContext,
					RailTimeConstants.NO_NET_CONNECTION);
			return;
		}
		switch (v.getId()) {
		case R.id.button4:
			checkAppUpdate();
			break;
		case R.id.button7:
			checkDbUpdate();
			break;
		case R.id.button8:
			downloadDbUpdate();
			break;
		case R.id.button5:
			openAppInPlayStore();
			break;

		default:
			break;
		}

	}

	private void openAppInPlayStore() {
		final String appPackageName = mContext.getPackageName(); // getPackageName()
																	// from
																	// Context
																	// or
																	// Activity
																	// object
		try {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://play.google.com/store/apps/details?id="
							+ appPackageName)));
		}
	}

	private void checkDbUpdate() {
		final UpdateFinder checker = new UpdateFinder(getActivity());
		checker.findDBUpdate(this);
		setProgressDialog(checker);
	}

	private void checkAppUpdate() {
		final UpdateFinder checker = new UpdateFinder(getActivity());
		checker.findAppUpdate(this);
		setProgressDialog(checker);
	}

	private void setProgressDialog(final UpdateFinder checker) {
		RuntimeData.setProgress_dialog(new ProgressDialog(mContext));
		RuntimeData.getProgress_dialog().setProgressStyle(
				ProgressDialog.STYLE_SPINNER);
		RuntimeData.getProgress_dialog().setMessage("Checking please wait..");
		RuntimeData.getProgress_dialog().setCancelable(false);
		RuntimeData.getProgress_dialog().setButton(
				DialogInterface.BUTTON_POSITIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						checker.cancel();
						RuntimeData.setProgress_dialog(null);
					}
				});
		RuntimeData.getProgress_dialog().show();
	}

	public void updateUi() {
		if (this.isRunning) {
			SharedPreferences sf = mContext.getSharedPreferences(
					RailTimeConstants.PREFS_NAME, 0);
			int latestDbVer = sf.getInt(RailTimeConstants.SERVER_DB_VER, -1);
			DbDetails dbDt = FrontController.getInstance(mContext)
					.checkAndGetRailDbDet();

			int currentAppVer = Utils.getMyAppVerCode(mContext);
			int latestAppVer = sf.getInt(RailTimeConstants.PLAY_APP_VER, -1);
			String latestDbDate = sf.getString(
					RailTimeConstants.LATEST_DB_DATE, "Info Unavailable");

			TextView tv = (TextView) getActivity().findViewById(R.id.app)
					.findViewById(R.id.text1);
			tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
					+ currentAppVer + "</font></big></b>"));

			tv = (TextView) getActivity().findViewById(R.id.text2);
			String ver = latestAppVer == -1 ? "Info Unavailable" : latestAppVer
					+ "";
			tv.setText(Html.fromHtml("<b><big><font color=\"black\">" + ver
					+ "</font></big></b>"));

			Button b = (Button) getActivity().findViewById(R.id.button5);
			if (latestAppVer > currentAppVer) {
				b.setEnabled(true);
			} else
				b.setEnabled(false);

			if (dbDt != null) {
				tv = (TextView) getActivity().findViewById(R.id.text3);
				tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
						+ dbDt.getDbver() + "</font></big></b>"));

				tv = (TextView) getActivity().findViewById(R.id.text4);
				tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
						+ dbDt.getDbdate() + "</font></big></b>"));
			} else {
				tv = (TextView) getActivity().findViewById(R.id.text3);
				tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
						+ "Info Unavailable" + "</font></big></b>"));

				tv = (TextView) getActivity().findViewById(R.id.text4);
				tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
						+ "Info Unavailable" + "</font></big></b>"));
			}

			tv = (TextView) getActivity().findViewById(R.id.text5);
			tv.setText(Html.fromHtml("<b><big><font color=\"black\">"
					+ latestDbDate + "</font></big></b>"));

			b = (Button) getActivity().findViewById(R.id.button8);
			getActivity().findViewById(R.id.button5).setOnClickListener(this);
			if ((dbDt != null && latestDbVer > dbDt.getDbver())
					|| (dbDt == null && latestDbVer > 0)) {
				b.setEnabled(true);
			} else
				b.setEnabled(false);
			b.invalidate();

		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.v("tag", "Settings: onResume");
		if (RuntimeData.getProgress_dialog() != null) {
			RuntimeData.getProgress_dialog().show();
		}
		updateUi();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.getProgress_dialog().isShowing()) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgressRemoved(true);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		isRunning = true;
	}

	@Override
	public void onStop() {
		super.onStop();
		isRunning = false;
		RuntimeData.setProgress_dialog(null);
		if (getActivity() != null)
			try {
				getActivity().unregisterReceiver(registerReceiver);
			} catch (Exception e) {
			}
		if (alert != null) {
			alert.dismiss();
			alert = null;
		}
	}

	private void downloadDbUpdate() {
		SharedPreferences sf = mContext.getSharedPreferences(
				RailTimeConstants.PREFS_NAME, 0);
		String latestDbDate = sf
				.getString(RailTimeConstants.LATEST_DB_DATE, "");
		int latestDbSize = sf.getInt(RailTimeConstants.LATEST_DB_SIZE, -1);
		String latestDbPath = sf
				.getString(RailTimeConstants.LATEST_DB_PATH, "");
		if (latestDbDate.length() == 0 || latestDbSize <= 0
				|| latestDbPath.length() == 0) {
			// INvalid state. Just return
			return;
		}
		FrontController.getInstance(mContext).downloadDb(mContext);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			DatabaseHandler.getInstance(mContext).openDatabase(true);
			updateUi();
		}

	};

	@Override
	public void onDBUpdateFinderFinished(UpdateDetails dbUpdateDetails) {
		processUpdateDetails(dbUpdateDetails, "database");

	}

	private void processUpdateDetails(UpdateDetails updateDetails, String type) {
		if (RuntimeData.getProgress_dialog() != null) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgress_dialog(null);
			OrientationUtils.unlockOrientation((Activity) mContext);
		}
		if (updateDetails != null) {
			if (isRunning) {
				if (updateDetails.isUpdateAvailable()) {
					downloadBtn.setEnabled(true);
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mContext);
					builder.setMessage("Update for " + type + " available.");
					builder.setCancelable(false);
					builder.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
								}
							});
					alert = builder.create();
					alert.show();
				} else {
					downloadBtn.setEnabled(false);
					AlertDialog.Builder builder = new AlertDialog.Builder(
							mContext);
					builder.setMessage("You have the latest version of " + type
							+ ".");
					builder.setCancelable(false);
					builder.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
								}
							});
					alert = builder.create();
					alert.show();
				}

			}
		} else {
			if (isRunning) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setMessage("Check updates failed. Please try later.");
				builder.setCancelable(false);
				builder.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
		updateUi();

	}

	@Override
	public void onAppUpdateFinderFinished(UpdateDetails updateDetails) {
		SharedPreferences sf = mContext.getSharedPreferences(
				RailTimeConstants.PREFS_NAME, 0);
		if (updateDetails != null && updateDetails.isUpdateAvailable()) {
			sf.edit()
					.putInt(RailTimeConstants.PLAY_APP_VER,
							updateDetails.getAppVer()).commit();
		} else {
			sf.edit()
					.putInt(RailTimeConstants.PLAY_APP_VER,
							Integer.parseInt(Utils.getAppVersion(mContext)))
					.commit();
		}
		processUpdateDetails(updateDetails, "application");

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (getActivity() != null)
			try {
				getActivity().unregisterReceiver(registerReceiver);
			} catch (Exception e) {
			}
	}

	class DownloadListener extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			updateUi();
		}

	}

}
