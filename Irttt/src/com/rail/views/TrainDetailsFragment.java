package com.rail.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.lite.R;
import com.rail.model.ScreenData;
import com.rail.model.TrainDetWithRoute;
import com.rail.model.TrainDetails;
import com.rail.model.TrainRoute;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class TrainDetailsFragment extends Fragment {
	private boolean isScrolled;
	private FrontController mFrontController;
	private ListView mListView;
	private TextView mLineText1;
	private TextView mLineText2;
	private TextView mLineText3;
	private TrainDetWithRoute resultData;
	private int temp;
	private StringBuffer textBuffer = new StringBuffer();
	private StringBuffer htmlBuffer = new StringBuffer();
	private Bitmap myBitmap;
	private LayoutInflater inflater;

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		menu.clear();
		if (resultData != null && resultData.getRt() != null
				&& resultData.getRt().size() > 0) {
			getActivity().getMenuInflater().inflate(R.menu.all_share, menu);
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					textBuffer = new StringBuffer();
					htmlBuffer = new StringBuffer();
					ScreenData data = (ScreenData) getArguments()
							.getSerializable("data");

					textBuffer.append("** Train details for #"
							+ data.getTrainNo() + " " + data.getTrainName());
					htmlBuffer.append("<html> Train details for <b>#"
							+ data.getTrainNo() + " " + data.getTrainName()
							+ "</b><br/><br/>");
					htmlBuffer.append("Station Name&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Arr&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Halt&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Dist&nbsp;&nbsp;&nbsp;");
					htmlBuffer.append("Code&nbsp;&nbsp;&nbsp;<br/>");

					for (TrainRoute trainDetails : resultData.getRt()) {
						textBuffer.append("\n**");
						textBuffer.append("Station Name->"
								+ trainDetails.getStationName().trim());
						textBuffer.append(";Arr->"
								+ trainDetails.getArriaval().trim());
						textBuffer.append(";Halt->" + trainDetails.getHalt()
								+ "m");
						textBuffer.append(";Dist->"
								+ trainDetails.getDist().trim() + "km");
						textBuffer.append(";Code->"
								+ trainDetails.getStationCode().trim() + ";");

						htmlBuffer.append(trainDetails.getStationName().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getArriaval().trim());
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getHalt() + "m");
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getDist().trim() + "km");
						htmlBuffer.append("&nbsp;&nbsp;&nbsp;"
								+ trainDetails.getStationCode().trim()
								+ "<br/>");
					}
					textBuffer.append("**");
					textBuffer.append("\n");
					textBuffer
							.append("Sent from IRTTT<https://play.google.com/store/apps/details?id=com.rail.lite&hl=en>");
					htmlBuffer
							.append("<br/>Sent from  <a href=\"https://play.google.com/store/apps/details?id=com.rail.lite&hl=en\"><b>IRTTT</b></a>  </html>");
				}
			});
		}
		super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.item3:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, textBuffer.toString());
			sendIntent.setType("text/plain");
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Train details from IRTTT");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;
		case R.id.item4:
			sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					Html.fromHtml(htmlBuffer.toString()));
			sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Train details from IRTTT");
			sendIntent.setType("text/html");
			try {
				startActivity(sendIntent);
			} catch (Exception e) {
				// ignore if not activity to handle
			}

			break;

		case R.id.item5:
			View v1 = getActivity().getWindow().getDecorView().getRootView();
			v1.setDrawingCacheEnabled(true);
			myBitmap = v1.getDrawingCache();
			saveBitmap(myBitmap);
			v1.destroyDrawingCache();

			v1.setDrawingCacheEnabled(false);
			break;

		default:
			break;
		}

		return true;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFrontController = FrontController.getInstance(getActivity());
		RuntimeData.setProgress_dialog(new ProgressDialog(getActivity()));
		if (resultData == null) {
			RuntimeData.getProgress_dialog().setMessage("Please wait...");
			RuntimeData.getProgress_dialog().setCancelable(false);
			RuntimeData.getProgress_dialog().show();
			new FetchTrainsTask().execute();
		} else {
			processResult(resultData);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		isScrolled = false;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.inflater = inflater;
		View rootView = inflater.inflate(R.layout.fgmt_trainslist, container,
				false);
		setHasOptionsMenu(true);
		initUI(rootView);
		setRetainInstance(true);
		temp++;
		return rootView;
	}

	public void saveBitmap(Bitmap bitmap) {
		File root = Environment.getExternalStorageDirectory();
		if (root != null) {
			File extFolder = new File(root, getActivity().getPackageName());
			File picFolder = new File(extFolder, "pictures");
			if (!picFolder.exists()) {
				picFolder.mkdir();
			}
			for (File file : picFolder.listFiles()) {
				file.delete();
			}
			File imagePath = new File(picFolder, System.currentTimeMillis()
					+ "_screenshot.png");

			FileOutputStream fos;
			try {
				fos = new FileOutputStream(imagePath);
				bitmap.compress(CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();
				sendMail(imagePath.getAbsolutePath());
			} catch (FileNotFoundException e) {
				Log.e("GREC", e.getMessage(), e);
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("GREC", e.getMessage(), e);
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void sendMail(String path) {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				"Train details from IRTTT");
		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		emailIntent.putExtra(
				android.content.Intent.EXTRA_TEXT,
				Html.fromHtml("<html> Train details for <b>#"
						+ data.getTrainNo() + " " + data.getTrainName()
						+ "</b><br/><br/></html>"));

		emailIntent.setType("image/png");
		Uri myUri = Uri.parse("file://" + path);

		emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
		try {
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		} catch (Exception e) {
			// ignore if not activity to handle
		}
	}

	private void initUI(View rootView) {
		mListView = (ListView) rootView.findViewById(R.id.listView1);
		mListView.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				ScreenData data = (ScreenData) getArguments().getSerializable(
						"data");
				if (view.getFirstVisiblePosition() == 0) {
					((UserActivity) getActivity())
							.setTitle(getArguments().getString(
									"title"));
					((UserActivity) getActivity()).setCompoundTitle(null);
				} else {
					
					((UserActivity) getActivity()).setTitle(Html
							.fromHtml("<html><small><b>" + "#"
									+ data.getTrainNo() + "<br/>" + data.getTrainName()
									+ "</b>" + "</small></html>"));
					((UserActivity) getActivity())
							.setCompoundTitle(getResources().getDrawable(
									R.drawable.a12));
				}

			}
		});

		View view2 = inflater.inflate(R.layout.lyt_headerpane, null);
		View view3 = inflater
				.inflate(R.layout.lyt_train_list_header_type_4, null);

		view3.setBackgroundColor(getActivity().getResources().getColor(
				R.color.brown));
		((TextView) view3.findViewById(R.id.textView3))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView4))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView5))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView6))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view3.findViewById(R.id.textView7))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));


		LinearLayout layout = new LinearLayout(getActivity());
		layout.setOrientation(LinearLayout.VERTICAL);


		mLineText1 = (TextView) view2.findViewById(R.id.textView1STsrcheading);
		mLineText2 = (TextView) view2.findViewById(R.id.textView1);
		mLineText3 = (TextView) view2.findViewById(R.id.textView2);
		Typeface custom_font = Typeface
				.createFromAsset(getActivity().getAssets(), "nato_sans_bold.ttf");
		mLineText1.setTypeface(custom_font,
				Typeface.BOLD);
		mLineText2.setTypeface(custom_font,
				Typeface.BOLD);
		mLineText3.setTypeface(custom_font,
				Typeface.BOLD);
		((TextView) view3.findViewById(R.id.textView3))
				.setTypeface(custom_font,
						Typeface.NORMAL);
		((TextView) view3.findViewById(R.id.textView4))
				.setTypeface(custom_font,
						Typeface.NORMAL);
		((TextView) view3.findViewById(R.id.textView5))
				.setTypeface(custom_font,
						Typeface.NORMAL);
		((TextView) view3.findViewById(R.id.textView6))
				.setTypeface(custom_font,
						Typeface.NORMAL);
		((TextView) view3.findViewById(R.id.textView7))
				.setTypeface(custom_font,
						Typeface.NORMAL);
		layout.addView(view2);
		layout.addView(view3);
		mListView.addHeaderView(layout,null,false);


		mFrontController.showAdv(mListView);
		RuntimeData.getAdView().setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				/*
				 * if(!isScrolled){ mListView.scrollTo(0, 0); isScrolled= true;
				 * }
				 */
			}
		});

		ScreenData data = (ScreenData) getArguments().getSerializable("data");
		mLineText1.setVisibility(View.GONE);
		mLineText2.setText("# " + data.getTrainNo()+" "+data.getTrainName());
		mLineText3.setVisibility(View.GONE);
		((UserActivity) getActivity()).setTitle(getArguments().getString(
				"title"));
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (resultData != null && temp > 1) {
			processResult(resultData);
		}

	}

	private void processResult(TrainDetWithRoute result) {
		mListView.setAdapter(new TrainDetailAdapter(result, getActivity()));
		getActivity().supportInvalidateOptionsMenu();
		getTrainRunningDetails(result);

	}

	@Override
	public void onStop() {
		super.onStop();
		RuntimeData.setProgress_dialog(null);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		resultData = null;
	}

	class FetchTrainsTask extends AsyncTask<Void, Void, TrainDetWithRoute> {

		@Override
		protected TrainDetWithRoute doInBackground(Void... params) {
			return mFrontController.getCompleteRoute(
					((ScreenData) getArguments().getSerializable("data"))
							.getTrainNo(), null, false);
		}

		@Override
		protected void onPostExecute(TrainDetWithRoute result) {
			super.onPostExecute(result);

			resultData = result;
			processResult(result);
			if (RuntimeData.getProgress_dialog() != null) {
				RuntimeData.getProgress_dialog().dismiss();
				RuntimeData.setProgress_dialog(null);
			}

		}

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.getProgress_dialog().isShowing()) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgressRemoved(true);
		}
	}

	public void getTrainRunningDetails(TrainDetWithRoute result) {
		TrainDetails trainDetails = result.getTd();

		StringBuilder sb = new StringBuilder();
		sb.append("Runs on ");
		int dayCount = 0;

		StringBuilder days = new StringBuilder();
		String append = "";
		for (int i = 0; i < 7; i++) {
			if (trainDetails.getScheduleFromSunday()[i]) {
				switch (i) {
				case 0:
					days.append("Sun");
					break;
				case 1:
					days.append(append + "Mon");
					break;
				case 2:
					days.append(append + "Tue");
					break;
				case 3:
					days.append(append + "Wed");
					break;
				case 4:
					days.append(append + "Thu");
					break;
				case 5:
					days.append(append + "Fri");
					break;
				case 6:
					days.append(append + "Sat");
					break;
				}
				append = ",";
				dayCount++;
			}
		}

		if (dayCount == 7) {
			sb.append("all days");
		} else {
			sb.append(days.toString() + " from starting station.");
		}

		// addAdmobAdd(R.id.linearLayoutscr3_2);

		mLineText2.append("\n" + (sb.toString()));
	}

	class TrainDetailAdapter extends BaseAdapter {
		private TrainDetWithRoute tdwr = null;
		private Context mContext;

		public TrainDetailAdapter(TrainDetWithRoute tdwr, Context context) {
			this.tdwr = tdwr;
			this.mContext = context;
		}

		@Override
		public int getCount() {
			return tdwr.getRt().size();
		}

		@Override
		public Object getItem(int position) {
			return tdwr.getRt().get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(
						R.layout.lyt_train_list_item_type_4, null, false);
			}
			TextView stationName = (TextView) convertView
					.findViewById(R.id.textView3);
			TextView trarr = (TextView) convertView
					.findViewById(R.id.textView4);
			TextView halt = (TextView) convertView.findViewById(R.id.textView5);
			TextView dist = (TextView) convertView.findViewById(R.id.textView6);
			TextView code = (TextView) convertView.findViewById(R.id.textView7);
			TrainRoute trainDetails = tdwr.getRt().get(position);
			stationName.setText(trainDetails.getStationName().trim());
			trarr.setText(trainDetails.getArriaval().trim());
			halt.setText(trainDetails.getHalt() + "m");
			dist.setText(trainDetails.getDist().trim());
			code.setText(trainDetails.getStationCode().trim());


			return convertView;
		}

	}

}
