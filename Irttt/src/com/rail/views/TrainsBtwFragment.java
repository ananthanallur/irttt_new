package com.rail.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.lite.R;
import com.rail.model.ScreenData;
import com.rail.model.Station;
import com.rail.utilities.RuntimeData;

public class TrainsBtwFragment extends Fragment implements OnClickListener {
	private boolean isScrolled;
	private AutoCompleteTextView src_station_name_actv;
	private AutoCompleteTextView dest_station_name_actv;
	private ArrayAdapter<String> actv_adapter;
	private TextView mDateTxt, mMonthTxt, mYearTxt;
	private ImageView mCalendarImg;
	private List<String> stationNames = new ArrayList<String>();
	private FrontController mFrontController;
	private int day, month = -1, year;
	private Button mDayBtn;
	private Button mWeekButton;
	private Button mRecents;
	private DatePickerDialog datePickerDialog;
	private ImageButton mClearSrc;
	private ImageButton mClearDest;
	private CheckBox mUserNearBy;
	private ImageButton mSwitchStations;
	private Integer selection = -1;
	private List<String> trainsBtwStationHistory;
	private View rootView;
	private String src;
	private String dest;
	private SrcStationTextChangedListener sourceWatcher;
	private DestStationTextChangedListener destWatcher;
	private boolean isChecked;
	AlertDialog alert = null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		this.rootView = inflater.inflate(R.layout.fgmt_srctodsthome, container,
				false);
		mFrontController = FrontController.getInstance(getActivity());
		if (day == 0 || month == -1 || year == 0) {
			Calendar cal = Calendar.getInstance();
			day = cal.get(Calendar.DAY_OF_MONTH);
			month = cal.get(Calendar.MONTH);
			year = cal.get(Calendar.YEAR);
		}
		setHasOptionsMenu(true);
		setRetainInstance(true);
		initUI();

		if (src != null && dest != null) {
			System.out.println("came here");
			src_station_name_actv.setText(src);
			dest_station_name_actv.setText(dest);
			updateDate();
			mUserNearBy.setChecked(isChecked);
		}
		return rootView;
	}

	private void initUI() {
		((UserActivity) getActivity())
				.setTitle(R.string.trains_between_stations_text);
		actv_adapter = new AutoCompleteAdapter(getActivity(),
				R.layout.lyt_auto_comp_item, R.id.textView1, stationNames);
		src_station_name_actv = (AutoCompleteTextView) rootView
				.findViewById(R.id.actv_src_station_name);
		dest_station_name_actv = (AutoCompleteTextView) rootView
				.findViewById(R.id.actv_dest_station_name);
		mRecents = (Button) rootView.findViewById(R.id.btn1);
		mRecents.setText(Html.fromHtml("LOAD FROM<b><big> RECENTS</big></b>"));
		mRecents.setOnClickListener(this);
		sourceWatcher = new SrcStationTextChangedListener();
		src_station_name_actv.addTextChangedListener(sourceWatcher);
		destWatcher = new DestStationTextChangedListener();
		dest_station_name_actv.addTextChangedListener(destWatcher);
		src_station_name_actv.setAdapter(actv_adapter);
		dest_station_name_actv.setAdapter(actv_adapter);
		mDateTxt = (TextView) rootView.findViewById(R.id.textView3);
		mMonthTxt = (TextView) rootView.findViewById(R.id.textView7);
		mYearTxt = (TextView) rootView.findViewById(R.id.textView8);
		mCalendarImg = (ImageView) rootView.findViewById(R.id.imageButton3);
		mDayBtn = (Button) rootView.findViewById(R.id.button1);
		mWeekButton = (Button) rootView.findViewById(R.id.button2);
		mDayBtn.setText(Html
				.fromHtml("FIND TRAINS FOR THE<br/><b><big> DAY</big></b>"));
		mDayBtn.setLineSpacing(1, 1.1f);
		mWeekButton.setText(Html
				.fromHtml("FIND TRAINS FOR THE<br/><b><big>WEEK</big></b>"));
		mWeekButton.setLineSpacing(1, 1.1f);

		mClearDest = (ImageButton) rootView
				.findViewById(R.id.imageButton1stdstsearch);
		mClearSrc = (ImageButton) rootView
				.findViewById(R.id.imageButton1stsrcsearch);
		mUserNearBy = (CheckBox) rootView.findViewById(R.id.checkBox1);
		mClearDest.setOnClickListener(this);
		mClearSrc.setOnClickListener(this);
		mSwitchStations = (ImageButton) rootView
				.findViewById(R.id.imageButton4);
		mSwitchStations.setOnClickListener(this);
		mCalendarImg.setOnClickListener(this);
		rootView.findViewById(R.id.linearLayout1sttraveldate)
				.setOnClickListener(this);
		mDayBtn.setOnClickListener(this);
		mWeekButton.setOnClickListener(this);
		updateDate();
		trainsBtwStationHistory = mFrontController.getTrainsBtwStationHistory();
		dest_station_name_actv
				.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						InputMethodManager imm = (InputMethodManager) getActivity()
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(
								dest_station_name_actv.getWindowToken(), 0);

					}

				});
		mFrontController.showAdv(getActivity(), rootView);
		 RuntimeData.getAdView().setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					/*if(!isScrolled){
						View scrollView = rootView.findViewById(R.id.scrollView);
						if(scrollView!=null){
							((ScrollView)scrollView).scrollTo(0, 0);
							isScrolled = true;
						}
					}*/
				}
			});
	}

	private void updateDate() {
		String month[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
				"Aug", "Sep", "Oct", "Nov", "Dec" };
		mDateTxt.setText(TrainsBtwFragment.this.day + "");
		mMonthTxt.setText(month[TrainsBtwFragment.this.month]
				.toUpperCase(Locale.US));
		mYearTxt.setText(TrainsBtwFragment.this.year + "");
	}

	@Override
	public void onResume() {
		super.onResume();
		if (trainsBtwStationHistory.size() == 0) {
			mRecents.setEnabled(false);
		} else {
			mRecents.setEnabled(true);
		}
		isScrolled = false;
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if (alert != null) {
			alert.dismiss();
			alert = null;
		}

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menu.add("Clear Trains btw history");

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// mFrontController.clearTrainsBtwStationHistory();

		final List<String> selection = new ArrayList<String>();

		AlertDialog.Builder trainBtwBuilder = new AlertDialog.Builder(
				getActivity())
				.setTitle("Recents")
				.setMultiChoiceItems(
						trainsBtwStationHistory.toArray(new String[trainsBtwStationHistory
								.size()]),
						new boolean[trainsBtwStationHistory.size()],
						new DialogInterface.OnMultiChoiceClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which, boolean isChecked) {
								if (isChecked) {
									selection.add(trainsBtwStationHistory
											.get(which));
								} else {
									selection.remove(trainsBtwStationHistory
											.get(which));
								}
							}
						});

		trainBtwBuilder.setPositiveButton("Delete",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						mFrontController.delTrainsBtwStationHistory(selection);
						trainsBtwStationHistory = mFrontController
								.getTrainsBtwStationHistory();
						if (trainsBtwStationHistory.size() == 0) {
							mRecents.setEnabled(false);
						} else {
							mRecents.setEnabled(true);
						}
					}
				});
		trainBtwBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				});

		if (trainsBtwStationHistory.size() == 0) {
			trainBtwBuilder.setMessage("No History");

		}
		AlertDialog alert = trainBtwBuilder.create();
		alert.show();
		if (trainsBtwStationHistory.size() == 0) {
			alert.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
		}

		return super.onOptionsItemSelected(item);
	}

	public class SrcStationTextChangedListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			stationNames = mFrontController.getStationNamesFromDB(s.toString());
			actv_adapter.notifyDataSetChanged();
			actv_adapter = new AutoCompleteAdapter(getActivity(),
					R.layout.lyt_auto_comp_item, R.id.textView1, stationNames);
			src_station_name_actv.setAdapter(actv_adapter);
			if (src_station_name_actv != null
					&& src_station_name_actv.getText().toString().length() > 0) {
				mClearSrc.setVisibility(View.VISIBLE);
			} else {
				mClearSrc.setVisibility(View.INVISIBLE);
			}
			if ((dest_station_name_actv != null
					&& dest_station_name_actv.getText().toString().length() > 0
					&& src_station_name_actv != null && src_station_name_actv
					.getText().toString().length() > 0)) {
				mDayBtn.setEnabled(true);
				mWeekButton.setEnabled(true);
				mSwitchStations.setVisibility(View.VISIBLE);

			} else {
				mDayBtn.setEnabled(false);
				mWeekButton.setEnabled(false);
				mSwitchStations.setVisibility(View.GONE);
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence userInput, int start,
				int before, int count) {

		}

	}

	public class DestStationTextChangedListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			stationNames = mFrontController.getStationNamesFromDB(s.toString()
					.toString());
			actv_adapter.notifyDataSetChanged();
			actv_adapter = new AutoCompleteAdapter(getActivity(),
					R.layout.lyt_auto_comp_item, R.id.textView1, stationNames);
			dest_station_name_actv.setAdapter(actv_adapter);
			if (dest_station_name_actv != null
					&& dest_station_name_actv.getText().toString().length() > 0) {
				mClearDest.setVisibility(View.VISIBLE);
				if (src_station_name_actv != null
						&& src_station_name_actv.getText().toString().length() > 0) {
					mDayBtn.setEnabled(true);
					mWeekButton.setEnabled(true);
					mSwitchStations.setVisibility(View.VISIBLE);
				}
			} else {
				mClearDest.setVisibility(View.INVISIBLE);
				mDayBtn.setEnabled(false);
				mWeekButton.setEnabled(false);
				mSwitchStations.setVisibility(View.GONE);
			}

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence userInput, int start,
				int before, int count) {

		}

	}

	@Override
	public void onClick(View v) {
		FragmentTransaction ft = null;
		Calendar cal = null;

		switch (v.getId()) {
		case R.id.btn1:

			v.startAnimation(AnimationUtils.loadAnimation(getActivity(),
					R.anim.click_anim));
			ArrayAdapter<String> recentsAdapter = new ArrayAdapter<String>(
					getActivity(), R.layout.lyt_simple_alert_layout,
					trainsBtwStationHistory);
			AlertDialog.Builder trainBtwBuilder = new AlertDialog.Builder(
					getActivity()).setTitle("Recents").setSingleChoiceItems(
					recentsAdapter, selection,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							src_station_name_actv
									.setText(trainsBtwStationHistory.get(which)
											.split(" to ")[0].trim());
							dest_station_name_actv
									.setText(trainsBtwStationHistory.get(which)
											.split(" to ")[1].trim());
							selection = which;
							dialog.cancel();
						}
					});
			if (trainsBtwStationHistory.size() == 0) {
				trainBtwBuilder.setMessage("No History");
			}
			alert = trainBtwBuilder.create();
			alert.show();

			break;
		case R.id.linearLayout1sttraveldate:
		case R.id.imageButton3:
			v.startAnimation(AnimationUtils.loadAnimation(getActivity(),
					R.anim.click_anim));
			datePickerDialog = new DatePickerDialog(getActivity(),
					new OnDateSetListener() {
						@Override
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							TrainsBtwFragment.this.month = monthOfYear;
							TrainsBtwFragment.this.day = dayOfMonth;
							TrainsBtwFragment.this.year = year;
							updateDate();
						}
					}, year, month, day);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				Calendar cal2 = Calendar.getInstance();
				cal2.set(Calendar.HOUR_OF_DAY,
						cal2.getMinimum(Calendar.HOUR_OF_DAY));
				cal2.set(Calendar.MINUTE, cal2.getMinimum(Calendar.MINUTE));
				cal2.set(Calendar.SECOND, cal2.getMinimum(Calendar.SECOND));
				cal2.set(Calendar.MILLISECOND,
						cal2.getMinimum(Calendar.MILLISECOND));
				datePickerDialog.getDatePicker().setMinDate(
						cal2.getTimeInMillis());
			}
			datePickerDialog.show();
			break;
		case R.id.button1:
			ScreenData data;
			Station srcStation;
			Station destStation;
			Bundle bundle;
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Unrecognized field");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

			if (src_station_name_actv.getText().toString().split(" - ").length < 2) {

				builder.setMessage("Please complete the Source station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else if (dest_station_name_actv.getText().toString().split(" - ").length < 2) {
				builder.setMessage("Please complete the Destination station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else {
				ft = getFragmentManager().beginTransaction();
				data = new ScreenData(getActivity());
				src = src_station_name_actv.getText().toString();
				dest = dest_station_name_actv.getText().toString();
				srcStation = new Station(src_station_name_actv.getText()
						.toString().split(" - ")[1], src_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				destStation = new Station(dest_station_name_actv.getText()
						.toString().split(" - ")[1], dest_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				data.setSource(srcStation);
				data.setDest(destStation);
				cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_MONTH, day);
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.YEAR, year);
				data.setDate(cal);
				isChecked = mUserNearBy.isChecked();
				data.setConsiderNearbyTrains(mUserNearBy.isChecked());
				DayTrainsBtwStationsFragment dayTrainsBtwStations = new DayTrainsBtwStationsFragment();
				bundle = new Bundle();
				bundle.putSerializable("data", data);
				bundle.putBoolean("fetchData", true);
				bundle.putString("title",
						getString(R.string.trains_between_stations_text));
				dayTrainsBtwStations.setArguments(bundle);
				mFrontController
						.storeTrainsBtwStationHistory(src_station_name_actv
								.getText().toString()
								+ " to "
								+ dest_station_name_actv.getText().toString());
				trainsBtwStationHistory = mFrontController
						.getTrainsBtwStationHistory();
				ft.replace(R.id.fragment_container, dayTrainsBtwStations)
						.addToBackStack(null);
				ft.commit();
			}
			break;
		case R.id.button2:
			builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Unrecognized field");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

			if (src_station_name_actv.getText().toString().split(" - ").length < 2) {
				builder.setMessage("Please complete the Source station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else if (dest_station_name_actv.getText().toString().split(" - ").length < 2) {
				builder.setMessage("Please complete the Destination station selection from the dropdown.");
				AlertDialog dialog = builder.create();
				dialog.show();
				((TextView) dialog.findViewById(android.R.id.message))
						.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
			} else {
				ft = getFragmentManager().beginTransaction();
				data = new ScreenData(getActivity());
				src = src_station_name_actv.getText().toString();
				dest = dest_station_name_actv.getText().toString();
				srcStation = new Station(src_station_name_actv.getText()
						.toString().split(" - ")[1], src_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				destStation = new Station(dest_station_name_actv.getText()
						.toString().split(" - ")[1], dest_station_name_actv
						.getText().toString().split(" - ")[0], 0);
				data.setSource(srcStation);
				data.setDest(destStation);
				cal = Calendar.getInstance();
				cal.set(Calendar.DAY_OF_MONTH, day);
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.YEAR, year);
				data.setDate(cal);
				isChecked = mUserNearBy.isChecked();
				data.setConsiderNearbyTrains(mUserNearBy.isChecked());
				WeekTrainsBtwStationsFragment weekTrainsBtwStations = new WeekTrainsBtwStationsFragment();
				bundle = new Bundle();
				bundle.putSerializable("data", data);
				bundle.putString("title",
						getString(R.string.trains_between_stations_text));
				weekTrainsBtwStations.setArguments(bundle);
				mFrontController
						.storeTrainsBtwStationHistory(src_station_name_actv
								.getText().toString()
								+ " to "
								+ dest_station_name_actv.getText().toString());
				trainsBtwStationHistory = mFrontController
						.getTrainsBtwStationHistory();
				ft.replace(R.id.fragment_container, weekTrainsBtwStations)
						.addToBackStack(null);
				ft.commit();
			}
			break;

		case R.id.imageButton1stsrcsearch:
			src_station_name_actv.setText("");
			break;

		case R.id.imageButton1stdstsearch:
			dest_station_name_actv.setText("");
			break;

		case R.id.imageButton4:
			v.startAnimation(AnimationUtils.loadAnimation(getActivity(),
					R.anim.click_anim));
			src_station_name_actv.removeTextChangedListener(sourceWatcher);
			dest_station_name_actv.removeTextChangedListener(destWatcher);
			src_station_name_actv.setFocusable(false);
			src_station_name_actv.setFocusableInTouchMode(false);
			dest_station_name_actv.setFocusable(false);
			dest_station_name_actv.setFocusableInTouchMode(false);

			String temp = "";
			temp = src_station_name_actv.getText().toString();

			src_station_name_actv.setText(dest_station_name_actv.getText()
					.toString());
			dest_station_name_actv.setText(temp);
			src_station_name_actv.setFocusable(true);
			src_station_name_actv.setFocusableInTouchMode(true);
			dest_station_name_actv.setFocusable(true);
			dest_station_name_actv.setFocusableInTouchMode(true);
			src_station_name_actv.addTextChangedListener(sourceWatcher);
			dest_station_name_actv.addTextChangedListener(destWatcher);

			break;
		default:
			break;
		}

	}

	class AutoCompleteAdapter extends ArrayAdapter<String> {

		public AutoCompleteAdapter(Context context, int resource,
				int textViewResourceId, List<String> objects) {
			super(context, resource, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			((TextView) view.findViewById(R.id.textView1))
					.setTextColor(getActivity().getResources().getColor(
							android.R.color.black));
			return view;
		}
	}

	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		src_station_name_actv=null;
		dest_station_name_actv=null;
		actv_adapter=null;
		mDateTxt=null;
		mMonthTxt=null;
		mYearTxt=null;
		mCalendarImg=null;
		stationNames =null;
		mFrontController=null;
		mDayBtn=null;
		mWeekButton=null;
		mRecents=null;
		datePickerDialog=null;
		mClearSrc=null;
		mClearDest=null;
		mUserNearBy=null;
		mSwitchStations=null;
		trainsBtwStationHistory=null;
		rootView=null;
		src=null;
		dest=null;
		sourceWatcher=null;
		destWatcher=null;
	}
}