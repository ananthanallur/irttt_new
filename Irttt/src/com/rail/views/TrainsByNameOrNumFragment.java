package com.rail.views;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.rail.controller.FrontController;
import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.FinalDataReturned;
import com.rail.model.RowOf7Strings;
import com.rail.model.ScreenData;
import com.rail.utilities.RuntimeData;
import com.rail.utilities.Utils;

public class TrainsByNameOrNumFragment extends Fragment implements
		OnClickListener {
	private boolean isScrolled;
	private AutoCompleteTextView src_station_name_actv;
    private List<Object> trains = new ArrayList<Object>();
	private Button mDayBtn;
	private ImageButton mClearData;
	private FrontController mFrontController;
	private List<String> trainsHistory;
	private LinearLayout mTrainHistoryView;
	private LinearLayout mTrainsListView;
	private View trainList;
	private View recentView;
	private TextView header1;
	private TextView header2;
	private boolean isListVisible;
	private FinalDataReturned resultData;
	private LayoutInflater inflater;
	private View rootView;
	private ProgressBar progress;
	private String src;
    private ArrayAdapter<String> actv_adapter;

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.clear();
		menu.add("Clear all train name recents");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		mFrontController.clearRecentTrainsByNameNum();
		trainsHistory = mFrontController.getTrainNameOrNumberHistory();
		setHistoryAdapter(trainsHistory);
		if (trainsHistory.size() == 0
				&& header1.getText().toString()
						.equalsIgnoreCase("RECENTLY CHECKED KEYWORDS")) {
			header2.setVisibility(View.GONE);
			header1.setVisibility(View.GONE);
			rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
		} else {
			header2.setVisibility(View.VISIBLE);
			header1.setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.scroller).setVisibility(View.VISIBLE);
		}
		return super.onOptionsItemSelected(item);
	}

	public TrainsByNameOrNumFragment() {
	}
@Override
public void onResume() {
	super.onResume();
	isScrolled = false;
}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		this.inflater = inflater;
		setHasOptionsMenu(true);
		rootView = inflater.inflate(R.layout.fgmt_trainsbynameornum, container,
				false);
		mFrontController = FrontController.getInstance(getActivity());
		initUI();
		((UserActivity) getActivity())
				.setTitle(R.string.train_by_name_number_text);
		setRetainInstance(true);
		if (src != null) {
			src_station_name_actv.setText(src);
		}
		return rootView;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		isListVisible = false;
	}

	View view;
    TrainsTextChangedListener listener;

    private void initUI() {
        actv_adapter = new AutoCompleteAdapter(getActivity(),
                R.layout.lyt_auto_comp_item, R.id.textView1, trains);
        listener = new TrainsTextChangedListener();
        src_station_name_actv = (AutoCompleteTextView) rootView
				.findViewById(R.id.actv_src_station_name);
        src_station_name_actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FragmentTransaction ft = null;
                Bundle bundle = new Bundle();
                String txt = ((TextView)view.findViewById(R.id.textView1)).getText().toString();
                txt=txt.substring(0,txt.indexOf(" - "));
                ScreenData data = new ScreenData(getActivity());
                InputMethodManager imm = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(src_station_name_actv.getWindowToken(),
                        0);
                trainsHistory = mFrontController
                        .storeTrainNameOrNumHistory(txt);
                ft = getFragmentManager().beginTransaction();
                data.setTrainNo(txt);
                DayTrainByNoDetailsFragment dayTrainsBtwStations = new DayTrainByNoDetailsFragment();
                bundle.putSerializable("data", data);
                bundle.putString("type", "number");
                bundle.putString("title",
                        getString(R.string.train_by_name_number_text));

                dayTrainsBtwStations.setArguments(bundle);
                ft.replace(R.id.fragment_container, dayTrainsBtwStations)
                        .addToBackStack(null);
                src_station_name_actv.setText("");
                ft.commit();

            }


        });

        src_station_name_actv.setAdapter(actv_adapter);
        src_station_name_actv.addTextChangedListener(listener);
		progress = (ProgressBar) rootView.findViewById(R.id.progressBar1);

		mDayBtn = (Button) rootView.findViewById(R.id.button1);
		mDayBtn.setText(Html.fromHtml("FIND<b><big> TRAINS</big> <b/> "));
		mClearData = (ImageButton) rootView
				.findViewById(R.id.imageButton1stsrcsearch);
		mTrainHistoryView = (LinearLayout) rootView
				.findViewById(R.id.listView1);
		mClearData.setOnClickListener(this);
		mDayBtn.setOnClickListener(this);
		trainsHistory = mFrontController.getTrainNameOrNumberHistory();
		mFrontController.showAdv(getActivity(), rootView);
		 RuntimeData.getAdView().setAdListener(new AdListener() {
				@Override
				public void onAdLoaded() {
					super.onAdLoaded();
					/*if(!isScrolled){
					((ScrollView)rootView.findViewById(R.id.parent)).scrollTo(0, 0);
					isScrolled = true;
					}*/
				}
			});
		setHistoryAdapter(trainsHistory);
		mTrainHistoryView.setEnabled(true);
		mTrainHistoryView.setClickable(true);
		mTrainsListView = (LinearLayout) rootView
				.findViewById(R.id.trainlistView1);

		view = rootView.findViewById(R.id.linearLayout2);
		view.setBackgroundColor(getActivity().getResources().getColor(R.color.brown));
		((TextView) view.findViewById(R.id.textView3))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		((TextView) view.findViewById(R.id.textView4))
				.setTextColor(getActivity().getResources().getColor(
						android.R.color.white));
		header1 = ((TextView) rootView.findViewById(R.id.textView1));
		header2 = ((TextView) rootView.findViewById(R.id.textView2));
		if (trainsHistory.size() == 0
				&& header1.getText().toString()
						.equalsIgnoreCase("RECENTLY CHECKED KEYWORDS")) {
			header2.setVisibility(View.GONE);
			header1.setVisibility(View.GONE);
			rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
		}
		recentView = rootView.findViewById(R.id.recent);
		trainList = rootView.findViewById(R.id.trainList);
		if (isListVisible) {
			recentView.setVisibility(View.GONE);
			processResult(resultData);
		}
	}


	@Override
	public void onClick(View v) {
		FragmentTransaction ft = null;
		switch (v.getId()) {

		case R.id.button1:
			Bundle bundle = new Bundle();
			String regex = "\\d+";
			String txt = src_station_name_actv.getText().toString();
			ScreenData data = new ScreenData(getActivity());
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(src_station_name_actv.getWindowToken(),
					0);
			if (txt.matches(regex)) {
                if(mFrontController.isTrainByNumFound(txt)){
                    ft = getFragmentManager().beginTransaction();
                    data.setTrainNo(txt);
                    DayTrainByNoDetailsFragment dayTrainsBtwStations = new DayTrainByNoDetailsFragment();
                    bundle.putSerializable("data", data);
                    bundle.putString("type", "number");
                    bundle.putString("title",
                            getString(R.string.train_by_name_number_text));
                    src = src_station_name_actv.getText().toString();
                    dayTrainsBtwStations.setArguments(bundle);
                    ft.replace(R.id.fragment_container, dayTrainsBtwStations)
                            .addToBackStack(null);
                    ft.commit();
                }else{
                    data.setTrainNo(src_station_name_actv.getText().toString());
                    isListVisible = true;
                    new FetchTrainsByNoTask().execute(data);
                }

			} else {
				data.setTrainName(src_station_name_actv.getText().toString());
				isListVisible = true;

				new FetchTrainsTask().execute(data);
			}
			trainsHistory = mFrontController
					.storeTrainNameOrNumHistory(src_station_name_actv.getText()
							.toString());

			break;

		case R.id.imageButton1stsrcsearch:
			src_station_name_actv.setText("");
			header1.setText("RECENTLY CHECKED KEYWORDS");
			header2.setText("Recent train names / numbers checked for details");
			trainList.setVisibility(View.GONE);
			view.setVisibility(View.GONE);
			trainsHistory = mFrontController.getTrainNameOrNumberHistory();

			recentView.setVisibility(View.VISIBLE);
			if (trainsHistory.size() == 0) {
				header2.setVisibility(View.GONE);
				header1.setVisibility(View.GONE);
				rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
			}
			isListVisible = false;
			break;

		default:
			break;
		}

	}

	class FetchTrainsTask extends
			AsyncTask<ScreenData, Void, FinalDataReturned> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			header2.setVisibility(View.GONE);
			header1.setVisibility(View.GONE);
			rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
		}

		@Override
		protected FinalDataReturned doInBackground(ScreenData... params) {
			trainsHistory = mFrontController.getTrainNameOrNumberHistory();
			return mFrontController.getTrainsByName(params[0]);
		}

		@Override
		protected void onPostExecute(final FinalDataReturned result) {
			super.onPostExecute(result);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					resultData = result;
					processResult(result);
					setHistoryAdapter(trainsHistory);
					if (trainsHistory.size() == 0) {
						header2.setVisibility(View.GONE);
						header1.setVisibility(View.GONE);
						rootView.findViewById(R.id.scroller).setVisibility(
								View.GONE);
					} else {
						header2.setVisibility(View.VISIBLE);
						header1.setVisibility(View.VISIBLE);
						rootView.findViewById(R.id.scroller).setVisibility(
								View.VISIBLE);
					}
					progress.setVisibility(View.GONE);
				}
			}, 500);
		}
	}

	private void processResult(FinalDataReturned result) {
		if (result.getErrCode() == RailTimeConstants.TRAIN_NO_TRAINS_WITH_NAME || result.getErrCode() == RailTimeConstants.TRAIN_NO_TRAINS_WITH_NUM) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage("There are no trains matching this name.");
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							trainsHistory = mFrontController
									.getTrainNameOrNumberHistory();

							setHistoryAdapter(trainsHistory);
							recentView.setVisibility(View.VISIBLE);
							isListVisible = false;
						}
					});
			builder.create().show();
		} else {
			recentView.setVisibility(View.GONE);
			setTrainListAdapter(result.getRows());

		}
	}

	private void setTrainListAdapter(List<RowOf7Strings> rows) {
		mTrainsListView.removeAllViews();
		int i = 0;
		for (RowOf7Strings trainDetails : rows) {
			int position = i++;
			View convertView = inflater.inflate(
					R.layout.lyt_train_list_item_type_3, null, false);
			TextView trno = (TextView) convertView.findViewById(R.id.textView3);
			TextView trname = (TextView) convertView
					.findViewById(R.id.textView4);

			trno.setText(trainDetails.getCol1().trim());
			trname.setText(trainDetails.getCol2().trim());
			if (position % 2 == 0) {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(R.color.light_gray));
			} else {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(android.R.color.background_light));
			}
			convertView.setTag(trainDetails.getCol1().trim() + "#"
					+ trainDetails.getCol2().trim());
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					FragmentTransaction ft = getFragmentManager()
							.beginTransaction();
					ScreenData data = new ScreenData(getActivity());
					data.setTrainNo(view.getTag().toString().split("#")[0]);
					data.setTrainName(view.getTag().toString().split("#")[1]);
					TrainDetailsFragment trainDetails = new TrainDetailsFragment();
					src = src_station_name_actv.getText().toString();
					Bundle bundle = new Bundle();
					bundle.putSerializable("data", data);
					bundle.putString("title",
							getString(R.string.train_by_name_number_text));
					trainDetails.setArguments(bundle);

					ft.replace(R.id.fragment_container, trainDetails)
							.addToBackStack(null);
					ft.commit();
				}
			});
			mTrainsListView.addView(convertView);

		}

		header1.setText("MATCHING TRAINS");
		header2.setText("Trains matching Keyword: "
				+ src_station_name_actv.getText().toString());
		view.setVisibility(View.VISIBLE);
		trainList.setVisibility(View.VISIBLE);

	}

	private String station = "";

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		station = v.getTag().toString();
		menu.add("Delete");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		mFrontController.removeRecentTrainNames(station);
		trainsHistory = mFrontController.getTrainNameOrNumberHistory();

		setHistoryAdapter(trainsHistory);
		if (trainsHistory.size() == 0) {
			header1.setVisibility(View.GONE);
			header2.setVisibility(View.GONE);
			rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
		} else {
			header1.setVisibility(View.VISIBLE);
			header2.setVisibility(View.VISIBLE);
			rootView.findViewById(R.id.scroller).setVisibility(View.VISIBLE);
		}
		return super.onContextItemSelected(item);
	}

	private void setHistoryAdapter(List<String> trainsHistory2) {
		mTrainHistoryView.removeAllViews();
		int i = 0;

		for (String trainName : trainsHistory2) {
			View convertView = inflater.inflate(
					R.layout.lyt_train_list_item_type_3, null, false);
			int position = i++;
			TextView slNo = (TextView) convertView.findViewById(R.id.textView3);
			TextView station = (TextView) convertView
					.findViewById(R.id.textView4);
			slNo.setText(position + 1 + "");
			station.setText(trainName);
			slNo.setTypeface(null, Typeface.BOLD);
			station.setTypeface(null, Typeface.BOLD);
			if (position % 2 == 0) {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(R.color.light_gray));
			} else {
				convertView.setBackgroundColor(getActivity().getResources()
						.getColor(android.R.color.background_light));
			}
			convertView.setTag(trainName);
			// registerForContextMenu(convertView);
			convertView.findViewById(R.id.imageView1).setVisibility(
					View.VISIBLE);
			convertView.findViewById(R.id.imageView1).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							mFrontController.removeRecentTrainNames(((View) v
									.getParent()).getTag().toString());
							trainsHistory = mFrontController
									.getTrainNameOrNumberHistory();

							setHistoryAdapter(trainsHistory);
							if (trainsHistory.size() == 0) {
								header1.setVisibility(View.GONE);
								header2.setVisibility(View.GONE);
								rootView.findViewById(R.id.scroller)
										.setVisibility(View.GONE);
							} else {
								header1.setVisibility(View.VISIBLE);
								header2.setVisibility(View.VISIBLE);
								rootView.findViewById(R.id.scroller)
										.setVisibility(View.VISIBLE);
							}
						}
					});
			convertView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View view) {
					((ScrollView) getActivity().findViewById(R.id.parent))
							.scrollTo(0, 0);
					src_station_name_actv.setText(view.getTag().toString());
					header2.setText("Trains matching Keyword: "
							+ (String) view.getTag().toString());
					TrainsByNameOrNumFragment.this.onClick(mDayBtn);
				}
			});

			mTrainHistoryView.addView(convertView);

		}

	}

	class FetchTrainsByNoTask extends
			AsyncTask<ScreenData, Void, FinalDataReturned> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress.setVisibility(View.VISIBLE);
			header2.setVisibility(View.GONE);
			header1.setVisibility(View.GONE);
			rootView.findViewById(R.id.scroller).setVisibility(View.GONE);
		}

		@Override
		protected FinalDataReturned doInBackground(ScreenData... params) {
			trainsHistory = mFrontController.getTrainNameOrNumberHistory();
			return mFrontController.getTrainsByNum(params[0]);
		}

		@Override
		protected void onPostExecute(final FinalDataReturned result) {
			super.onPostExecute(result);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					resultData = result;
					processResult(result);
					setHistoryAdapter(trainsHistory);
					if (trainsHistory.size() == 0) {
						header2.setVisibility(View.GONE);
						header1.setVisibility(View.GONE);
						rootView.findViewById(R.id.scroller).setVisibility(
								View.GONE);
					} else {
						header2.setVisibility(View.VISIBLE);
						header1.setVisibility(View.VISIBLE);
						rootView.findViewById(R.id.scroller).setVisibility(
								View.VISIBLE);
					}
					progress.setVisibility(View.GONE);
				}
			}, 500);
		}
	}

    public class TrainsTextChangedListener implements TextWatcher {
        @Override
        public void afterTextChanged(Editable s) {
            if(src_station_name_actv.isPerformingCompletion()){
                return;
            }
           {
                if (src_station_name_actv != null
                        && src_station_name_actv.getText().toString().length() > 0) {
                    mDayBtn.setEnabled(true);
                    mClearData.setVisibility(View.VISIBLE);
                    trains = mFrontController.getTrainsFromDB(s.toString());
                    actv_adapter.notifyDataSetChanged();
                    actv_adapter = new AutoCompleteAdapter(getActivity(),
                            R.layout.lyt_auto_comp_item, R.id.textView1, trains);
                    src_station_name_actv.setAdapter(actv_adapter);

                } else {
                    mDayBtn.setEnabled(false);
                    mClearData.setVisibility(View.GONE);
                    header1.setText("RECENTLY CHECKED KEYWORDS");
                    header2.setText("Recent train names / numbers checked for details");
                    view.setVisibility(View.GONE);
                    trainList.setVisibility(View.GONE);
                    trainsHistory = mFrontController.getTrainNameOrNumberHistory();

                    setHistoryAdapter(trainsHistory);
                    recentView.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void onTextChanged(CharSequence userInput, int start,
                                  int before, int count) {

        }

    }
    class AutoCompleteAdapter extends ArrayAdapter {
        List<Object> trains = new ArrayList<Object>();


        public AutoCompleteAdapter(Context context, int resource, int textViewResourceId, List<Object> objects) {
            super(context, resource, textViewResourceId, objects);
            this.trains = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            ((TextView) view.findViewById(R.id.textView1))
                    .setTextColor(getActivity().getResources().getColor(
                            android.R.color.black));
            ((TextView) view.findViewById(R.id.textView1)).setText(trains.get(position).toString());
            return view;
        }
    }
}