package com.rail.views;

import java.util.ArrayList;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rail.controller.RailTimeConstants;
import com.rail.lite.R;
import com.rail.model.NavDrawerItem;
//import com.rail.utilities.OrientationUtils;
import com.rail.utilities.RuntimeData;

public class UserActivity extends ActionBarActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private CharSequence mTitle;
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private RelativeLayout relative_layout;

	protected void onResume() {
		RuntimeData.setRunning(true);
		super.onResume();

		if (RuntimeData.getAlert() != null && RuntimeData.isAlertRemoved()) {
			RuntimeData.getAlert().show();
			RuntimeData.setAlertRemoved(false);
		}
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.isProgressRemoved()) {
			RuntimeData.getProgress_dialog().show();
			RuntimeData.setProgressRemoved(false);
		}
		if (RailTimeConstants.SHOW_ADS) {

			if (RuntimeData.getAdView() != null) {
				RuntimeData.getAdView().resume();
			}
		}

	}

	@Override
	protected void onPause() {
		RuntimeData.setRunning(false);
		super.onPause();
		if (RuntimeData.getAlert() != null
				&& RuntimeData.getAlert().isShowing()) {
			RuntimeData.getAlert().dismiss();
			RuntimeData.setAlertRemoved(true);
		}
		if (RuntimeData.getProgress_dialog() != null
				&& RuntimeData.getProgress_dialog().isShowing()) {
			RuntimeData.getProgress_dialog().dismiss();
			RuntimeData.setProgressRemoved(true);
		}
		if (RailTimeConstants.SHOW_ADS) {
			if (RuntimeData.getAdView() != null) {
				RuntimeData.getAdView().pause();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setContentView(R.layout.activity_user);

		Typeface custom_font = Typeface
				.createFromAsset(getAssets(), "4365.ttf");
		((TextView) findViewById(R.id.textViewx)).setTypeface(custom_font,
				Typeface.BOLD);
		getSupportActionBar().setElevation(0);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setIcon(android.R.color.transparent);
		getSupportActionBar().setLogo(android.R.color.transparent);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.lyt_actionbar, null);
		TextView mTitleTextView = (TextView) mCustomView
				.findViewById(R.id.textView1);
		mTitleTextView.setText("My Own Title");
		mCustomView.findViewById(R.id.ll).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						mDrawerLayout.openDrawer(relative_layout);

					}
				});
		getSupportActionBar().setCustomView(R.layout.lyt_actionbar);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		initUI(savedInstanceState);
		View v = new View(this);
		v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
		mDrawerList.addFooterView(v);
	}

	private void initUI(Bundle savedInstanceState) {
		mTitle = getTitle();
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		relative_layout = (RelativeLayout) findViewById(R.id.relative_layout);
		mDrawerList = (ListView) relative_layout.findViewById(R.id.left_drawer);
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position > 4) {
					position++;
				}
				displayView(position);
			}
		});
		navDrawerItems = new ArrayList<NavDrawerItem>();
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
				.getResourceId(6, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons
				.getResourceId(7, -1)));
		navMenuIcons.recycle();
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.string.app_name, R.string.app_name) {
			public void onDrawerClosed(View view) {
				supportInvalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				supportInvalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			displayView(getIntent().getIntExtra("screen", 0));
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/***
	 * Called when invalidateOptionsMenu() is triggered
	 */
	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	private void displayView(int position) {
		Fragment fragment = null;
		switch (position) {
		case 1:
			fragment = new TrainsBtwFragment();
			break;
		case 2:
			fragment = new PassingByTrainsFragment();
			break;
		case 3:
			fragment = new PNRHomeFragment();
			break;
		case 4:
			fragment = new TrainsByNameOrNumFragment();
			break;
		case 7:
			fragment = new SettingsFragment();
			break;
		case 8:
			fragment = new HelpFragment();
			break;

		case 6:
			fragment = new SeatmapFragment();
			break;

		default:
			break;
		}

		if (fragment != null) {
			android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager.popBackStackImmediate();
			fragmentManager.beginTransaction()
					.replace(R.id.fragment_container, fragment).commit();

			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(relative_layout);
		} else if (position == 0) {
			finish();
		} else {
			Log.e("MainActivity", "Error in creating fragment");

		}
	}

	protected boolean isAppInstalled(String packageName) {
		Intent mIntent = getPackageManager().getLaunchIntentForPackage(
				packageName);
		if (mIntent != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		((TextView) getSupportActionBar().getCustomView().findViewById(
				R.id.textView1)).setText(mTitle);
		((TextView) getSupportActionBar().getCustomView().findViewById(
				R.id.textView1)).setCompoundDrawablesWithIntrinsicBounds(null,
				null, null, null);
		getSupportActionBar().getCustomView().findViewById(
				R.id.view1).setVisibility(View.VISIBLE);
	}

	public void setCompoundTitle(Drawable drawable) {
		if(drawable!=null){
			getSupportActionBar().getCustomView().findViewById(
					R.id.view1).setVisibility(View.GONE);
		}else{
			getSupportActionBar().getCustomView().findViewById(
					R.id.view1).setVisibility(View.VISIBLE);
		}
		((TextView) getSupportActionBar().getCustomView().findViewById(
				R.id.textView1)).setCompoundDrawablesWithIntrinsicBounds(
				drawable, null, null, null);
		
		((TextView) getSupportActionBar().getCustomView().findViewById(
				R.id.textView1)).setCompoundDrawablePadding(20);

	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}